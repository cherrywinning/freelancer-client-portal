//global vars
var sDashWidgets;
var gridster;
var data_series = {};
var mprn = localStorage.getItem("mprn");
var sitelist = "";
var site_name = "";
var controller_id;
var wdefaults = [];
var engParams = {
	sys_w: {name: "Watts", units: "kW", type: "Power"},
	sys_va: {name: "VA", units: "kVA", type: "Power"},
	sys_var: {name: "VAR", units: "kVAR", type: "Power"},
	phase_1_volts: {name: "Phase 1 Volts", units: "V", type: "Voltage"},
	phase_2_volts: {name: "Phase 2 Volts", units: "V", type: "Voltage"},
	phase_3_volts: {name: "Phase 3 Volts", units: "V", type: "Voltage"},
	phase_1_amps: {name: "Phase 1 Amps", units: "A", type: "Current"},
	phase_2_amps: {name: "Phase 2 Amps", units: "A", type: "Current"},
	phase_3_amps: {name: "Phase 3 Amps", units: "A", type: "Current"},
    neutral_amps: {name: "Neutral Amps", units: "A", type: "Current"},
	v1_thd: {name: "THD Voltage - Phase 1", units: "%", type: "THD Volts"},
	v2_thd: {name: "THD Voltage - Phase 2",	units: "%", type: "THD Volts"},
	v3_thd: {name: "THD Voltage - Phase 3",	units: "%", type: "THD Volts"},
	i1_thd: {name: "THD Amperage - Phase 1", units: "%", type: "THD Amps"},
	i2_thd: {name: "THD Amperage - Phase 2", units: "%", type: "THD Amps"},
	i3_thd: {name: "THD Amperage - Phase 3", units: "%", type: "THD Amps"},
	freq: {name: "Frequency", units: "Hz", type: "Other"},
	pf: {name: "Power Factor", units: "", type: "Other"}
};
var allowedParams = {};
var userSites = {};
var userMeters = {};
var userReports = {};
var reqmeters = [];
var usedMeters = [];
var allMeters = [];
var firstAnalysisBuild = true;
var showAjaxError = true;
var chart_units = {
	sys_w: ["W", "Watts"], sys_va: ["VA", "VA"], sys_var: ["VAR", "VAR"],
	phase_1_volts: ["Volts","Phase 1 Voltage"], phase_2_volts: ["Volts", "Phase 2 Voltage"], phase_3_volts: ["Volts", "Phase 3 Voltage"],
	phase_1_amps: ["Amps", "Phase 1 Current"], phase_2_amps: ["Amps", "Phase 2 Current"], phase_3_amps: ["Amps", "Phase 3 Current"],
  neutral_amps: ["Amps", "Neutral Amps"],
	v1_thd: ["THD %", "Total Harmonic Distortion - Phase 1 Volts"], v2_thd: ["THD %", "Total Harmonic Distortion - Phase 2 Volts"], v3_thd: ["THD %", "Total Harmonic Distortion - Phase 3 Volts"],
	i1_thd: ["THD %", "Total Harmonic Distortion - Phase 1 Current"], i2_thd: ["THD %", "Total Harmonic Distortion - Phase 2 Current"],i3_thd: ["THD %", "Total Harmonic Distortion - Phase 3 Current"],
	freq: ["Hz", "Frequency"], pf: ["Power Factor", "Power Factor"]
};
var power_list = ['sys_w', 'sys_va', 'sys_var'];
var percent_list = ['v1_thd', 'v2_thd', 'v3_thd', 'i1_thd', 'i2_thd', 'i3_thd'];
var axispos = true; //yAxis positioner; left == true, right == false;
var defaultErrMsg = "There was a problem retrieving data from the database. Please refresh this page and try again. If the problem persists, please check your Internet connection.";
var nodeMap = {};
var userData = {};
var userNotifications = {};
var fullParamNames = {
	power: "Power",
	current: "Current",
	voltage: "Voltage",
	frequency: "Frequency",
	thd: "THD",
};

var user_data_worker, data_series_worker, stored_widgets_worker, user_sites_worker;
var got_widgets = false;
var got_wdefaults = false;

function showMessage(type, message) {
	$("#messages").trigger("custmessage", [type, message]);
}

function convertSources(sources) {
	var splitsrc = sources.split(",,");
	var sourcelist = "";

	$.each(splitsrc, function(key, val) {
		var src = val.split(",");

		$.each(userMeters, function(index, value) {
			if (value.controller_id == src[0] && value.meter_id == src[1]) {
				sourcelist += value.site_description + ": " + value.metered_circuit + " / ";
			}
		});
	});

	sourcelist = sourcelist.substring(0, sourcelist.length - 3);
	return sourcelist;
}

function convertConMeter(conmeter) {
	var desc = "";

	if (conmeter.indexOf("-") >= 0) {
		conmeter = conmeter.split("-");
	} else if (conmeter.indexOf("_") >= 0) {
		conmeter = conmeter.split("_");
	}

	$.each(userMeters, function(key, val) {
		if (val.controller_id == conmeter[0] && val.meter_id == conmeter[1]) {
			desc = val.site_description + ": " + val.metered_circuit;
		}
	});

	return desc;
}

function convertParams(params) {
	var list = [];
	params = params.split(",");

	$.each(params, function(key, val) {
		list.push(engParams[val].name);
	});

	list = list.join(", ");
	return list;
}

function convertTime(start, end) {
	var startdate = new XDate(start, true);
	var startd = startdate.toString("ddd, dd MMM yyyy HH:mm");
	var enddate = new XDate(end, true);
	var endd = enddate.toString("ddd, dd MMM yyyy HH:mm");
	var diff = (Math.round(startdate.diffDays(enddate) * 10)) / 10;
	var difftype = "Days";

	if (diff >= 365) {
		diff = (Math.round(startdate.diffYears(enddate) * 10)) / 10;
		difftype = "Years";
	} else if (diff >= 28) {
		diff = (Math.round(startdate.diffMonths(enddate) * 10)) / 10;
		difftype = "Months";
	} else if (diff >= 7) {
		diff = (Math.round(startdate.diffWeeks(enddate) * 10)) / 10;
		difftype = "Weeks";
	}

	var tarray = new Array(startd, endd, diff, difftype);
	return tarray;
}

function convertInterval(interval) {
	var out = "";

	switch(interval) {
		case "raw":
		case "Raw":
			out = "Real-Time Data From Your Meters";
			break;
		case "minute":
		case "1 Minute":
			out = "Data Averaged Over 1 Minute Intervals";
			break;
		case "15_minute":
		case "15 Minute":
			out = "Data Averaged Over 15 Minute Intervals";
			break;
		case "hour":
		case "1 Hour":
			out = "Data Averaged Over Hourly Intervals";
			break;
		case "day":
		case "Day":
			out = "Data Averaged Over Daily Intervals";
			break;
		case "week":
		case "Week":
			out = "Data Averaged Over Weekly Intervals";
			break;
		case "month":
		case "Month":
			out = "Data Averaged Over Monthly Intervals";
			break;
		default:
			out = "Automatic Data Intervals";
			break;
	}

	return out;
}

function convertPeriod(phrase) {
	switch(phrase) {
        case 'prevHour':
            return "Previous Hour";
		case 'prevDay':
			return "Previous Day";
		case 'prevWeek':
			return "Previous Week";
		case 'prevMonth':
			return "Previous Month";
		case 'prev3Months':
			return "Previous 3 Months";
		case 'prev6Months':
			return "Previous 6 Months";
		case 'prevYear':
			return "Previous Year";
	}
}

function initialJS() {
	//only get data if we need to
	userSites = JSON.parse(localStorage.getItem("userSites"));
	userData = JSON.parse(localStorage.getItem("userData"));
	mprn = localStorage.getItem("mprn");

	if (document.location.pathname === "/portal/home") {
		data_series_worker = new Worker('/static/js/workers/data_series_worker.js');
		data_series_worker.postMessage({mprn: mprn});

		stored_widgets_worker = new Worker('/static/js/workers/stored_widgets_worker.js');
	}

	user_data_worker = new Worker('/static/js/workers/user_data_worker.js');
	user_data_worker.postMessage({cmd: 'getdata'});

	user_sites_worker = new Worker('/static/js/workers/user_sites_worker.js');
	user_sites_worker.postMessage({cmd: 'getdata'});

	if (document.location.pathname === "/portal/home") {
		data_series_worker.onmessage = function(e) {
			data_series = JSON.parse(e.data);

			chkForData(data_series); //function in widgets class
			wrefreshall();
			if (!got_widgets) {
				stored_widgets_worker.postMessage({mprn: mprn});
			}

			controller_id = data_series[0].controller_id;
			$("#wcon-id").val(controller_id);

			//update the sysw signs
			$.each(data_series, function(key, val) {
				if (val.sys_w < 0) {
					localStorage.setItem("csyswSign-" + val.controller_id + "-" + val.meter_id, "neg");
					localStorage.setItem("psyswSign-" + val.controller_id + "-" + val.meter_id, "neg");
				} else {
					localStorage.setItem("csyswSign-" + val.controller_id + "-" + val.meter_id, "pos");
					localStorage.setItem("psyswSign-" + val.controller_id + "-" + val.meter_id, "pos");
				}
			});

			//change the color of data shadow
			$("#datadwn").removeClass("hasnodata");
			$("#datadwn").toggleClass("hasdata");
		};

		stored_widgets_worker.onmessage = function(e) {
			wdefaults = JSON.parse(e.data);
			if (!got_widgets) {
				checkForWidgets();
				got_widgets = true;
			}
		};
	}

	user_data_worker.onmessage = function(e) {
		userData = JSON.parse(e.data);
		localStorage.setItem("userData", e.data);
		//store users firstname
		localStorage.setItem("ufname", userData[0].first_name);
		localStorage.setItem("usurname", userData[0].last_name);
		localStorage.setItem('uid', userData[0].user_id);
	};

	user_sites_worker.onmessage = function(e) {
		userSites = JSON.parse(e.data);
		localStorage.setItem("userSites", e.data);
		//check if we already have an mprn
		if (mprn === null) {
			mprn = userSites[0].pk;
			//save the mprn to localStorage
			localStorage.setItem("mprn", mprn);
		} else {
			mprn = localStorage.getItem("mprn");
		}
		//save the site name
		$.each(userSites, function(key, val) {
			if (val.pk == mprn) {
				site_name = val.fields.description;
				localStorage.setItem("curSiteDesc", site_name);
				$("#fullscreenTitle").html("Latest Data for " + localStorage.getItem("curSiteDesc"));
			}
		});
	}

	if (!userSites || !mprn) {
		//get initial data, performed sync as data is required to display widgets
		getUserSites();
	}

	if (!userData) {
		user_data_worker.postMessage({cmd: 'getdata'});
	}

	// getDataSeries();

	//iterate through the result to get list of sites
	$.each(userSites, function(key, val) {
		sitelist += '<li>';
		sitelist += '<a href="/portal/home" data-site="' + val.pk + '" class="site-link" title="Load This Site"><span class="lefticon">&#xf2ea;</span>';
		sitelist += val.fields.description + '</li>';
	});

	//build nodeMap
	if (Object.keys(nodeMap).length === 0) {
		buildNodeMap();
	}


}

function getUserData() {
	//get and save user name
	$.ajax({
		url: "/user/info",
		type: "GET",
		dataType: "JSON",
		async: false,
		success: function(result) {
			userData = result;
			localStorage.setItem("userData", JSON.stringify(userData));
			//store users firstname
			localStorage.setItem("ufname", result[0].first_name);
			localStorage.setItem("usurname", result[0].last_name);
			localStorage.setItem('uid', result[0].user_id);
		},
		error: function() {
			showMessage("err", defaultErrMsg);
			console.log("Unable to retrieve user info");
			//ensure that we have something to show as the username
			localStorage.setItem("ufname", "John");
			localStorage.setItem("usurname", "Smith");
			localStorage.setItem('uid', '');
		}
	});
}

// function getDataSeries() {
// 	//get data_series for the selected mprn
// 	$.ajax({
// 		url: "/api/site/now?mprn=" + mprn,
// 		type: "GET",
// 		dataType: "JSON",
//
// 		success: function(result) {
// 			data_series = result;
//
// 			chkForData(data_series); //function in widgets class
//
// 			//get the users stored widgets
// 			$.ajax({
// 				url: "/api/site/user/config?site=" + mprn,
// 				type: "GET",
// 				dataType: "JSON",
// 				success: function(result) {
// 					//store the result - stored widgets will be added when the page has fully loaded
// 					wdefaults = result;
// 				},
// 				error: function(result) {
// 					showMessage("err", defaultErrMsg);
// 					console.log("Unable to retrieve stored widgets");
// 				},
//
// 			});
// 		},
// 		error: function(result) {
// 			showMessage("err", defaultErrMsg);
// 			console.log("NO DATA RECEIVED");
// 		},
//
// 	});
// }

function getUserSites() {
	user_sites_worker.postMessage({cmd: 'getdata'});
	// $.ajax({
	// 	url: "/user/sites",
	// 	type: "GET",
	// 	dataType: "JSON",
	//
	// 	success: function(result) {
	// 		userSites = result;
	// 		localStorage.setItem("userSites", JSON.stringify(userSites));
	// 		//check if we already have an mprn
	// 		if (mprn === null) {
	// 			mprn = result[0].pk;
	// 			//save the mprn to localStorage
	// 			localStorage.setItem("mprn", mprn);
	// 		} else {
	// 			mprn = localStorage.getItem("mprn");
	// 		}
	// 		//save the site name
	// 		$.each(result, function(key, val) {
	// 			if (val.pk == mprn) {
	// 				site_name = val.fields.description;
	// 				localStorage.setItem("curSiteDesc", site_name);
	// 			}
	// 		});
	// 	}
	// });
}

function getWidgetOrder() {
	if (document.location.pathname === "/portal/home") {
		var savelist = [];
		var gsavelist = [];
		setTimeout(function() {
			gsavelist = gridster.serialize();
			saveWidgetOrder(gsavelist);
		}, 500);

	} else if (document.location.pathname === "/portal/history/reporting") {
		getReportOrder();
	}

}

function saveWidgetOrder(list) {
	//build the list of widgets that are on the dashboard
	var wlist = JSON.stringify(list);

	//get mprn - same for all controller + meter combinations
	var curmprn = localStorage.getItem("mprn");

	var data = "site=" + curmprn + "&layout=" + wlist;

	$.ajax({
		url: "/api/site/user/config",
		type: "POST",
		dataType: "text",
		data: data,
		async: false,
		success: function(result) {
			//update wdefaults
			wdefaults = list;
			console.log("Saved Widget Layout");
		},
		error: function(result) {
			var errmsg = "There was a problem saving data to the database. Please refresh this page and try again.";
			errmsg += " If the problem persists, please check your Internet connection.";
			showMessage("err", errmsg);
			console.log("ERROR: WIDGET LAYOUT NOT SAVED");
		},


	});
}

function loadSite(siteid) {

	//check if we need to save the widget order and clear the dashboard
	if (document.location.pathname === "/portal/home") {
		getWidgetOrder(); //save widgets

		gridster.remove_all_widgets();

	}

	//clear all settings for previous site
	var temp_ufname = localStorage.getItem("ufname");
	var temp_uid = localStorage.getItem("uid");
	var temp_userData = localStorage.getItem("userData");
	var temp_userSites = localStorage.getItem("userSites");
	var temp_usurname = localStorage.getItem("usurname");
	localStorage.clear();
	localStorage.setItem("ufname", temp_ufname);
	localStorage.setItem("uid", temp_uid);
	localStorage.setItem("userData", temp_userData);
	localStorage.setItem("userSites", temp_userSites);
	localStorage.setItem("usurname", temp_usurname);
	allgauges = {};
	widgettypes = {};
	wdefaults = [];
	got_widgets = false;


	//save the mprn
	localStorage.setItem("mprn", siteid);

	//process the link to the dashboard
	document.location.href = "/portal/home";

}

function getMeteredCircuits() {
	//clear selects and disable buttons
	if (data_series.length > 0) {
		var metered = "";
		$.each(data_series, function (key, val) {
			metered += '<option value="' + val.controller_id + ',' + val.meter_id + ',' + val.metered_circuit + '">' + val.site_description + " - " + val.metered_circuit + '</option>';
		});

		$("#add-widget-circuit").html(metered);
		$("#add-widget-circuit").trigger("chosen:updated");
	}
}

function enableWidgetSubmit() {
	$("#widgetformsubmit").removeAttr("disabled");
}

function titleCase(val){
    return val.charAt(0).toUpperCase()+val.substr(1).toLowerCase();
}

function showAddWidget() {
	getMeteredCircuits();
	if ($("#add-widget").offset().top < 0) {
		$("#dash-front").animate({
			top: "20px"
		}, {duration: 500, queue: false});
	} else {
		$("#dash-front").animate({
			top: "-730px"
		}, {duration: 500, queue: false});
	}
}

function goFullScreen() {
	$("#content").fullscreen();
}

function hideNav() {
	if ($("#sidebar").css("display") == "none") {
		$("#sidebar").animate({width: 'toggle'}, {duration: 1000, queue: false});
		$("#content").animate({marginLeft: "320px"}, {duration: 1000, queue: false});
		$("#navscrollup").animate({left: "10px"}, {duration: 1000, queue: false});
		$("#navscrolldown").animate({left: "10px"}, {duration: 1000, queue: false});
		$("#hidenav").html("&#xf150;");
	} else {
		$("#sidebar").animate({width: 'toggle'}, {duration: 1000, queue: false});
		$("#content").animate({marginLeft: "20px"}, {duration: 1000, queue: false});
		$("#navscrollup").animate({left: "-250px"}, {duration: 1000, queue: false});
		$("#navscrolldown").animate({left: "-250px"}, {duration: 1000, queue: false});
		$("#hidenav").html("&#xf152;");
	}

	$(document).trigger("divresize");
}

function checkForWidgets() {
	if (wdefaults.length === 0) {
		//if the user has not saved any widgets, then show all the widgets available to the user
		$.each(widgettypes, function(key, val) {
			$.each(val, function(subkey, subval) {

				var conmeter = key.split("-");
				var con = conmeter[0];
				var meter = conmeter[1];

				var desc = getData(con, meter, "metered_circuit");
				var types = subval.split(",");
				var grid = {
					size_x: 2,
					size_y: 2,
					col: null,
					row: null
				};

				for (var i=0; i < types.length; i++) {
					createWidgetObj(con, meter, desc, subkey, types[i], grid);
				}
			});

			getWidgetOrder();

		});

	} else {

		//check for old version of wdefaults and convert to new version
		var oldlayout = false;

		if (typeof(wdefaults[0]) === 'string') {
			var newwdefaults = [];
			oldlayout = true;

			$.each(wdefaults, function(key, val) {
				newwdefaults.push({
					id: val,
					col: null,
					row: null,
					size_x: 2,
					size_y: 2
				});
			});

			wdefaults = newwdefaults;
		}

		//parse wdefaults for stored widgets
		$.each(wdefaults, function(key, val) {
			var wname = val.id.split("-");
			var wdata = wname[0];
			var wtype = wname[1];
			var wcid = wname[2];
			var wmid = wname[3];
			var grid = {
				size_x: val.size_x,
				size_y: val.size_y,
				col: val.col,
				row: val.row
			};

			//get the description of the meter
			var wdesc = getData(wcid, wmid, "metered_circuit");

			//create the widget
			//check if the site supports the widget type
			var siteids = wcid + "-" + wmid;
			if (widgettypes[siteids][wdata]) {
				createWidgetObj(wcid, wmid, wdesc, wdata, wtype, grid);
			}

		});

		//save the new layout back to the database
		if (oldlayout) {
			getWidgetOrder();
		}
	}
}

//dashboard document ready
function dashboardDocReady() {

	//clear any leftover localStorage items
	var lslen = localStorage.length;

	for (var i=lslen-1; i>=0; i--) {
		var lskey = localStorage.key(i);

		if (lskey.indexOf('csyswSign') !== -1 || lskey.indexOf('psyswSign') !== -1) {
			localStorage.removeItem(lskey);
		}
	}

	//code for gridster - replacement for sDashboard
	gridster = $('.gridster > ul').gridster({
		widget_base_dimensions: [220,220],
		widget_margins: [20,20],
		autogenerate_stylesheet: true,
		avoid_overlapped_widgets: true,
		autogrow_cols: true,
		auto_init: true,
		resize: {
			enabled: false,
			stop: function() {
				getWidgetOrder();
			}
		},
		serialize_params: function($w, wgd) {
			return {
				id: $w[0].dataset.widget,
				col: wgd.col,
				row: wgd.row,
				size_x: wgd.size_x,
				size_y: wgd.size_y
			}
		},
		draggable: {
			stop: function() {
				//save the widget order
				getWidgetOrder();
			}
		}
	}).data('gridster');



	// startDataWorker();
	// startWidgetWorker();

	$("#add-widget-circuit").on("change", function(e, p) {
		if (p.selected) {
			var cmd = p.selected.split(",");
			var conId = cmd[0];
			var meterId = cmd[1];
			var desc = cmd[2];
			addWidgetSlides(conId, meterId, desc);
		} else {
			resetWidgetSlides();
		}
	});

	$(document).bind('fscreenchange', function(e, state, elem) {
		if ($.fullscreen.isFullScreen()) {
			$("#content").css("width", "100%");
			$("#content").css("height", "100%");
			$("#content").css("margin", "0");
			$("#content").css("overflow", "auto");
			$("#dash-front").css("margin", "20px auto");
			$("#dash-front").css("width", "90%");
			$("#dash-front").css("display", "block");
		} else {
			$("#content").css("margin", "20px 20px 20px 320px");
			$("#content").css("width", "auto");
			$("#dash-front").css("margin", "0");
			$("#dash-front").css("width", "100%");
		}
	});

	// $("#fullscreenTitle").html("Latest Data for " + localStorage.getItem("curSiteDesc"));

	var userFName = localStorage.getItem("ufname");
	$("#welcome").html('Welcome, <a href="/portal/account" title="Go to Your User Page">' + userFName + '</a>.');

	//check if there are default widgets to be displayed
	//wdefaults stored on initial page load
	// checkForWidgets();

    $("#add-widget-circuit").chosen({
		max_selected_options: 1,
		placeholder_text_multiple: "Please select a metered circuit",
        width: "400px"
	});

    $("#add-widget-category").chosen({
		placeholder_text_multiple: "Show All Categories",
        width: "400px"
	});

	$("#add-widget-category").on("change", function(e, p) {
		//filter the carousel by the type of widget
		if (p.selected) {
			var cat = p.selected;
			$("#add-widget-carousel").slickFilter(function(index) {
				var type = $(this).attr("data-type");

				if (type.indexOf(cat) !== -1) {
					return true;
				}

			});
		} else {
			$("#add-widget-carousel").slickUnfilter();
		}
	});


    $("#add-widget-carousel").slick({
		onAfterChange: function() {
			var index = arguments[1];
			var wtype = $(this.$slides[index]).attr("data-type");

			//change the description text based on the widget type
			$("#add-widget-all-desc > div").each(function() {
				if ($(this).attr("data-type") === wtype) {
					var title = $(this).children("h1").text();
					var content = $(this).html();

					$("#add-widget-desc").html(content);

					$(".add-widget-img-text").html(title + "<br>(Click Image to Add)");
				}
			});
		}
    });

	$("#add-widget-form").on("submit", function(e) {
		e.preventDefault();
		addAllWidgets();
	});

	$("#add-widget-form").on('reset', function(e) {
		e.preventDefault();
		$("#dash-front").animate({
			top: "-730px"
		}, {duration: 500, queue: false});

		//reset the carousel etc
		resetWidgetSlides();
	});

}

//function to add slides to carousel
function addWidgetSlides(conId, meterId, desc) {
	if (conId && meterId && desc) {
		var wid = conId + "-" + meterId;
		var widgets = widgettypes[wid];
		var imgHtml = "";
		var first = true;
		var imgTitle = "";
		var imgDesc = "";
		var cats = [];
		var alltypes = [];
		var wlist = [];

		$.each(wdefaults, function(key, val) {
			wlist.push(val.id);
		});

		$.each(widgets, function(cat, type) {
			var types = type.split(",");

			$("#add-widget-carousel").slickRemove(true);

			for (var i=0; i<types.length; i++) {
				var id = cat + "-" + types[i];
				var fullwid = id + '-' + wid;

				//check if the widget has already been added to the dashboard
				if ($.inArray(fullwid, wlist) === -1) {
					imgHtml += $('#add-widget-all-images > div[data-type="' + id + '"]').clone().wrap('<p>').parent().html();
					cats.push(cat);
					//check if we have added a title and desc yet
					if (first) {
						$("#add-widget-all-desc > div").each(function() {
							if ($(this).attr("data-type") === id) {
								imgTitle = $(this).children("h1").text();
								imgDesc = $(this).html();
							}
						});

						first = false;
					}
				}

				//build a list of widget types
				if ($.inArray(types[i], alltypes) === -1) {
					alltypes.push(types[i]);
				}
			}
		});

		$('#add-widget-carousel').unslick();
		$('#add-widget-carousel').html(imgHtml);
		$("#add-widget-desc").html(imgDesc);
		$(".add-widget-img-text").html(imgTitle + "<br>(Click Image to Add)");
		$('#add-widget-carousel').slick({
			onAfterChange: function() {
				var index = arguments[1];
				var wtype = $(this.$slides[index]).attr("data-type");

				//change the description text based on the widget type
				$("#add-widget-all-desc > div").each(function() {
					if ($(this).attr("data-type") === wtype) {
						var title = $(this).children("h1").text();
						var content = $(this).html();

						$("#add-widget-desc").html(content);

						$(".add-widget-img-text").html(title + "<br>(Click Image to Add)");
					}
				});
			}
		});

		//update the list of categories
		var catlist = '';
		$.each(cats, function(key, category) {
			if (catlist.indexOf(category) === -1) {
				catlist += '<option value="' + category + '">Show ' + fullParamNames[category] + '</option>';
			}
		});

		$("#add-widget-category").html(catlist);
		$("#add-widget-category").trigger("chosen:updated");

		//update the list of types
		var typelist = '';
		$.each(alltypes, function(key, type) {
			typelist += '<option value="' + type + '">Show ' + titleCase(type) + 's</option>';
		});


		//if there are no available widgets to add, show an image
		if ($(".slick-track").html() === "") {
			var noWidget = '<div>';
			noWidget += '<img src="/static/images/widgets/used.png" alt="No More Available Widgets" title="No More Available Widgets">';
            noWidget += '</div>';

			$('.add-widget-img-text').html('No More Available Widgets<br>For This Metered Circuit');
			$('#add-widget-desc').html('<h1>There Are No More Available Widgets For This Metered Circuit</h1>');
			$("#add-widget-carousel").slickAdd(noWidget);
		}
	}
}

//function to reset the carousel
function resetWidgetSlides() {
	//destroy the carousel and recreate it with the default slide
	$('#add-widget-carousel').unslick();
	var reset = '<div data-type="select-meter">';
	reset += '<img src="/static/images/widgets/select.png" alt="Select a Metered Circuit" title="Select a Metered Circuit">';
	reset += '</div>';

	$('.add-widget-img-text').html('Please Select a Metered Circuit');
	$('#add-widget-desc').html('<h1>Please Select a Metered Circuit</h1><p>To add a widget to your dashboard, please select a metered circuit.</p>');
	$('#add-widget-carousel').html(reset);
	$('#add-widget-carousel').slick();

	//remove the filters
	$("#add-widget-category").html("");
	$("#add-widget-category").trigger("chosen:updated");
}

//function to add widgets to the dashboard
function addWidget(type, ismulti) {
	//get the params reqd to build a widget
	var cmd = $("#add-widget-circuit").val()[0].split(",");
	var conId = cmd[0];
	var meterId = cmd[1];
	var desc = cmd[2];
	type = type.split("-");
	var ctype = type[0];
	var cchart = type[1];
	var grid = {
		size_x: 2,
		size_y: 2,
		col: null,
		row: null
	};

	createWidgetObj(conId, meterId, desc, ctype, cchart, grid);

	//save the new layout if single
	if (!ismulti) {
		getWidgetOrder();
		showMessage("info", "The Dashboard Has Been Updated.");
	}

	//remove the current slide from the carousel
	$("#add-widget-carousel").slickRemove($("#add-widget-carousel").slickCurrentSlide());

	//if there are no available widgets to add, show an image
	if ($(".slick-track").html() === "") {
		var noWidget = '<div>';
		noWidget += '<img src="/static/images/widgets/used.png" alt="No More Available Widgets" title="No More Available Widgets">';
		noWidget += '</div>';

		$('.add-widget-img-text').html('No More Available Widgets<br>For This Metered Circuit');
		$('#add-widget-desc').html('<h1>There Are No More Available Widgets For This Metered Circuit</h1>');
		$("#add-widget-carousel").slickAdd(noWidget);
	}

}

//add
function addAllWidgets() {
	//check that we have a metered circuit
	if ($("#add-widget-circuit").val()) {

		//get the available widgets
		var all = $("#add-widget-carousel div[data-type]:not(.slick-cloned)");
		$.each(all, function(key, el) {
			addWidget($(el).attr("data-type"), true);
		});

		//save the widget order
		getWidgetOrder();

	} else {
		showMessage("err", "Please select a metered circuit first.");
	}
}

//function used to refresh widgets
function startWidgetWorker() {
	var widgetWorker = new Worker("/static/js/timer2.js");

	widgetWorker.onmessage = function(e) {

		wrefreshall();
	};

	widgetWorker.postMessage({'cmd': 'start the widget worker'});
}

//function used to get data for sites
function startDataWorker() {
	var dataWorker = new Worker("/static/js/timer.js");

	dataWorker.onmessage = function(e) {
		//get the mprn from localStorage

		var cur_mprn = localStorage.getItem("mprn");

		$.ajax({
			url: "/api/site/now?mprn=" + cur_mprn,
			type: "GET",
			dataType: "json",
			success: function(result, sts) {
				data_series = result;
				controller_id = data_series[0].controller_id;
				$("#wcon-id").val(controller_id);

				//update the sysw signs
				$.each(data_series, function(key, val) {
					if (val.sys_w < 0) {
						localStorage.setItem("csyswSign-" + val.controller_id + "-" + val.meter_id, "neg");
						localStorage.setItem("psyswSign-" + val.controller_id + "-" + val.meter_id, "neg");
					} else {
						localStorage.setItem("csyswSign-" + val.controller_id + "-" + val.meter_id, "pos");
						localStorage.setItem("psyswSign-" + val.controller_id + "-" + val.meter_id, "pos");
					}
				});

				//change the color of data shadow
				$("#datadwn").removeClass("hasnodata");
				$("#datadwn").toggleClass("hasdata");
			},
			error: function(obj, sts, err) {
				$("#datadwn").removeClass("hasdata");
				$("#datadwn").toggleClass("hasnodata");
			}
		});
	};

	dataWorker.postMessage({'cmd': 'start the data worker'});
}

//basic parsing of URL parameters for history/analysis page
function getURLparams() {
	var urlparams = window.location.search.substring(1);
	if (urlparams.length > 0) {
		urlparams = JSON.parse(decodeURI(urlparams));
		return urlparams;
	} else {
		return false;
	}
}

//required for all pages
function allPages() {
	//reset the body
	$("body").css("overflow", "scroll"); //reset overflow

	checkNav();
	$("#sidebar").height($(document).height());

	//notify of ajax errors
	if (showAjaxError) {
		$(document).ajaxError(function() {
			console.groupCollapsed("%cThere was an ajax error. Click to see more.", "color: red;");
			console.log("%cURL: " + arguments[2].url, "color: red;");
			console.log("%cTYPE: " + arguments[2].type, "color: red;");
			console.log("%cDATATYPE: " + arguments[2].dataType, "color: red;");
			console.log("%cERROR MESSAGE: " + arguments[3].message, "color: red;");
			console.log("%cFULL ERROR RESPONSE: ", "color: red;", arguments);
			console.groupEnd();
		});
	}

	//trigger divresize event
	$(window).on("resize", function() {
		$(document).trigger("divresize");

		//check that all the navigation bar is visible
		//if not add buttons to scroll
		checkNav();
		$("#sidebar").height($(document).height());
	});

	//set the username
	var uname = localStorage.getItem("ufname");

	if (uname !== "") {
		$("#welcome").html('Welcome, <a href="/portal/account" title="Go to Your User Page">' + uname + '</a>.');
	} else {
		$("#welcome").html("Welcome!");
	}

	//set up the menu
	$("#nav").navgoco({
		accordion: true,
		onToggleAfter: function() {
			setTimeout(function() {
				checkNav();
			}, 500);
		}
	});

	//set the site name
	$("#siteheadname").html(localStorage.getItem("curSiteDesc"));


	//build the sites list
	$("#sitelist").prepend(sitelist);
	$("#sitelist > li").tsort();
	checkNav();

	//catch clicks on site links
	$(".site-link").on("click", function(e) {
		e.preventDefault();
		loadSite($(this).attr("data-site"));
	});

	//custom error messages
	$("#messages").on("custmessage", function(e, mess_type, mess_content) {
		$("#messages").removeClass();

		switch(mess_type) {
			case "info":
			default:
				$("#messages").addClass("mess-info");
				$("#messages").html(mess_content);
				break;
			case "err":
				$("#messages").addClass("mess-error");
				$("#messages").html(mess_content);
				break;
		}

		$("#messagewrap").animate({top: 0}, {duration: 1000, queue: false});
		$("html body").animate({scrollTop: 0}, {duration: 500, queue: false});

		setTimeout(function() {
			$("#messagewrap").animate({
				top: ($("#messagewrap").height() + 20) * -1
			}, {duration: 1000, queue: false});
		}, 10000);

	});

	$("#message-close").on("click", function() {
		$('#messagewrap').animate({
			top: (($('#messagewrap').height() + 10) * -1)
		}, 250);
	});

}

//check if the nav is visible or not
function checkNav() {
	var windowh = $(window).height();
	var navh = $("#nav").height();
	var welcomeh = $("#welcome").outerHeight(); //padding on this

	//check if the nav is going to be offscreen
	if (welcomeh + navh > windowh) {
		$("#navscrollup").fadeIn(250);
		$("#navscrolldown").fadeIn(250);
	} else {
		$("#navscrollup").fadeOut(250);
		$("#navscrolldown").fadeOut(250);

		//reset sidebar top
		$("#sidebar").animate({
			top: "0"
		}, 250);
	}

}

function scrollNavUp() {
	$("#sidebar").animate({
		top: "-=50"
	}, 250);
}

function scrollNavDown() {
	var sidebartop = $("#sidebar").position().top;

	if (sidebartop < 0) {
		$("#sidebar").animate({
			top: "+=50"
		}, 250);
	}
}

//sets new extremes using a button
function changeExtremes(dir) {
	//get the chart object
	var chart = $("#chart").highcharts('StockChart');

	//get the min and max and calculate the delta as half the current timespan
	var min = chart.xAxis[0].min;
	var max = chart.xAxis[0].max;
	var delta = (max - min) / 2;

	var newmin;
	var newmax;

	//moving forwards or backwards?
	if (dir === 'forward') {
		//show the loading
		chart.showLoading();

		//get the current time
		var latest = new XDate();

		//get the new max
		newmax = max + delta;

		//check if the new max is greater than the current data we have
		if (newmax > latest) {
			//recalulate the delta
			newmax = XDate.now();
			delta = newmax - max;
		}

		newmin = min + delta;

		//set the new extremes
		chart.xAxis[0].setExtremes(newmin, newmax);
		chart.redraw();
		chart.hideLoading();

	} else if (dir === 'back') {
		//show the loading
		chart.showLoading();

		newmin = min - delta;
		newmax = max - delta;

		chart.xAxis[0].setExtremes(newmin, newmax);
		chart.hideLoading();
		chart.redraw();
	}
}

//gets chart date for specific ranges
function shortDate(phrase) {
	var rightnow = new XDate();
	var start = new XDate();

	switch(phrase) {
        case 'prevHour':
            start = start.addHours(-1);
            break;
		case 'prevDay':
			start = start.addDays(-1);
			break;
		case 'prevWeek':
			start = start.addWeeks(-1);
			break;
		case 'prevMonth':
			start = start.addMonths(-1);
			break;
		case 'prev3Months':
			start = start.addMonths(-3);
			break;
		case 'prevYear':
			start = start.addYears(-1);
			break;
	}

	$("#dtshortcuts a").not("#" + phrase.toLowerCase()).removeClass("selected-button");
	$("#" + phrase.toLowerCase()).addClass("selected-button");

	start = start.toString("ddd, dd MMM yyyy HH:mm");
	rightnow = rightnow.toString("ddd, dd MMM yyyy HH:mm");

	$("#dpicker_from").datetimepicker({
		value: start,
		format: "D, d M Y H:i"
	});

	$("#dpicker_to").datetimepicker({
		value: rightnow,
		format: "D, d M Y H:i"
	});

	//check the allowable data resolutions
	checkResolution("upscale");
}

function checkResolution(selectid, type, period) {
	//set the limits
	var series_limit = 200;
	var maxres, minres, range_mod;

	if ($("#range").val() === "rangeson") {
		range_mod = 3;
	} else {
		range_mod = 1;
	}

	if (type === "save-report") {
		var times = createStartFromPeriod(period);
		var start = XDate.parse(times[0]);
		var end = XDate.parse(times[1]);
		var delta = (end - start) / 1000;
	} else {
		//get the time delta
		var start = XDate.parse($("#dpicker_from").val());
		var end = XDate.parse($("#dpicker_to").val());
		var delta = (end - start) / 1000;
	}

	//check which data resolutions the user will be allowed to use
	if (delta < series_limit * 3) {
		maxres = "raw";
	} else if (delta < series_limit * range_mod * 60) {
		maxres = "minute";
	} else if (delta < series_limit * range_mod * 900) {
		maxres = "15_minute";
	} else if (delta < series_limit * range_mod * 3600) {
		maxres = "hour";
	} else if (delta < series_limit * range_mod * 86400) {
		maxres = "day";
	} else if (delta < series_limit * range_mod * 604800) {
		maxres = "week";
	} else {
		maxres = "month";
	}

	//also need to limit other side of resolutions so that user
	//cannot pick a resolution that is less than the period they want to view
	if (delta < 120) {
		minres = "raw";
	} else if (delta < 1800) {
		minres = "minute";
	} else if (delta < 7200) {
		minres = "15_minute";
	} else if (delta < 172800) {
		minres = "hour";
	} else if (delta < 1209600) {
		minres = "day";
	} else  if (delta < 5356800) {
		minres = "week";
	} else {
		minres = "month";
	}

	//enable/disable resolutions as required
	var res_order = ["raw", "minute", "15_minute", "hour", "day", "week", "month"];

	//find where in the array maxes is and remove all items prior to that position
	var posmax = res_order.indexOf(maxres);
	var posmin = res_order.indexOf(minres);

	var user_res = res_order.slice(posmax, posmin + 1);

	//loop through select and disable options that user cannot have
	$("#" + selectid + " > option").each(function() {
		if ($.inArray(this.value, user_res) == -1) {
			$(this).attr("disabled", "disabled");
			//remove all selected options
			$(this).removeAttr("selected");
		} else {
			if ($(this).attr("disabled") == "disabled") {
				$(this).removeAttr("disabled");
			}
		}
	});

	//update the select menu
	$("#" + selectid).trigger("chosen:updated");

	//if the user can select raw, minute, 15_minute or hour, then show the flags section
	if ($.inArray("raw", user_res) !== -1 || $.inArray("minute", user_res) !== -1 || $.inArray("15_minute", user_res) !== -1 || $.inArray("hour", user_res) !== -1) {
		$("#dtflags").slideDown(500);
	} else {
		$("#dtflags").slideUp(500);
	}
}

function setRanges(phrase) {
	$("#dtranges a").not("#" + phrase.toLowerCase()).removeClass("selected-button");
	$("#" + phrase.toLowerCase()).addClass("selected-button");
	$("#range").val(phrase);

	//check the allowable data resolutions
	checkResolution("upscale");
}

function setFlags(phrase) {
	$("#dtflags a").not("#" + phrase.toLowerCase()).removeClass("selected-button");
	$("#" + phrase.toLowerCase()).addClass("selected-button");
	$("#flags").val(phrase);
}

//get the current resolution and set the axis values as needed
function get_current_resolution() {
	var resolution = localStorage.resolution;

	switch(resolution) {
		case "Raw":
			return "raw";
		case "1 Minute":
			return "minute";
		case "15 Minute":
			return "15_minute";
		case "1 Hour":
			return "hour";
		case "Day":
			return "day";
		case "Week":
			return "week";
		case "Month":
			return "month";
		default:
			return "";
	}
}

function createChart(data, compdata, params, meters, range, flagsdata) {
	var axes = {};
	var axes_list = [];
	meters = meters.split(",");
	params = params.split(",");

	//build list of meters
	var meterlist = meters.join(",,").replace(/\_/g, ",");
	var chartTitle = convertSources(meterlist);

	//build the options object for the chart
	var options = {
		chart: {
			renderTo: 'chart',
			borderWidth: 0,
			type: "spline",
			animation: false,
			zoomType: 'x',
			reflow: true
		},
		navigator : {
			adaptToUpdatedData: false,
		},
		scrollbar: {
			liveRedraw: false
		},
		credits: false,
		lang: {
			noData: "No Data Exists for the Date Selected. Please Select a Valid Date.",
			histRangeButtonTitle: "Show/ Hide Ranges",
		},
		legend: {
			enabled: true,
			backgroundColor: '#FFFFFF',
			itemDistance: 30,
			shadow: true,
		},
		noData: {
			style: {
				fontWeight: 'bold',
				fontSize: '15px',
				color: '#303030'
			}
		},
		subtitle: {
			text: "",
			align: 'center'
		},
		title: {
			align: 'center',
			text: "DSU - " + chartTitle
		},
		rangeSelector : {
			enabled: true,
			buttons: [{
				type: 'hour',
				count: 1,
				text: '1h'
			}, {
				type: 'day',
				count: 1,
				text: '1d'
			}, {
				type: 'month',
				count: 1,
				text: '1m'
			}, {
				type: 'year',
				count: 1,
				text: '1y'
			}, {
				type: 'all',
				text: 'All'
			}],
			inputEnabled: false, // it supports only days
			selected : 4 // all
		},
		xAxis: [{
			type: "datetime",
			startOnTick: false,
			maxPadding: 0.1,
			minPadding: 0.1,
			minRange:  60 * 1000,
			dateTimeLabelFormats: {
				day: '%H:%M',
				hour: '%H:%M'
			},
			lineColor: "#aaa",
			lineWidth: 3,
			tickColor: "#aaa",
			events : {
				afterSetExtremes : afterSetExtremes
			},
			labels: {
				step: 2,
				enabled: true,
				format: "{value:%d-%b-%Y<br>%H:%M}"
			},
			id: "main-xaxis"
		}],
		exporting: {
			buttons: {
			},
			sourceWidth: 1280,
			sourceHeight: 900,
			chartOptions: {
				rangeSelector: {
					enabled: false
				},
				events: {
					load: function() {}
				}
			}
		},
		global: {
			useUTC: true
		},
		plotOptions: {
			series: {
				events: {
					legendItemClick: function(event) {
						var sisterSeries = this.chart.get("range-" + this.userOptions.id);
						if (this.visible && sisterSeries) {
							sisterSeries.hide();
						} else if (!this.visible && sisterSeries) {
							sisterSeries.show();
						}
					}
				}
			},
			flags: {
				tooltip: {
					xDateFormat: '%A, %d %b %Y, %H:%M:%S'
				}
			}
		},
		tooltip: {
			crosshairs: true,
			shared: true,
			valueDecimals: 2,
		}
	};

	//only show range button if ranges are included in the chart
	if (range == "on") {
		options.exporting.buttons = {
			'histRangeButton': {
				x: -10,
				y: 40,
				_id: 'histRangeButton',
				onclick: function () {
					var btn_text = chart.options.exporting.buttons.histRangeButton.text;

					if (btn_text == "Hide Ranges" || btn_text == "Show/ Hide Ranges"){
						chart.options.exporting.buttons.histRangeButton.text="Show Ranges";
						range_toggle(true);
					} else {
						chart.options.exporting.buttons.histRangeButton.text="Hide Ranges";
						range_toggle(false);
					}
				},
				_titleKey: "histRangeButtonTitle",
				symbol: null,
				symbolFill: '#7fc900',
				hoverSymbolFill: '#7fc900',
				text: "Show/ Hide Ranges",
				_textKey: "histRangeButtonText"
			}
		};
	}

	//get the number of axes based on the number of parameters we have to display
	//compare to the chart_units array so that we can get the proper units.
	$.each(params, function(key, val){
		if (val in chart_units) {
			axes_list.push(chart_units[val][0]);
		}
	});

	//sort the axes and git rid of any duplicates
	axes_list.sort();
	axes_list = jQuery.unique(axes_list);
	var numaxes = axes_list.length;

	//clear any existing charts
	if (Highcharts.charts.length > 0) {
		Highcharts.charts.length = 0;
	}

	//create a blank chart and remove the default axis
	chart = new Highcharts.StockChart(options);
//	chart.yAxis[0].remove();
	Highcharts.setOptions({
		global: {
			useUTC: true
		}
	});

	//get the units for each axis.
	var paramunit;

	//loop through each data array and get the relevant params
	for (var i=0; i<data.length; i++) {
		var avgdata = [];
		//check if we have min and max data to parse
		if (range == "off") { //no ranges

			//check if we have avg or raw data
			if (typeof(data[i].avg) !== "undefined") {
				avgdata = data[i].avg;
			} else if (typeof(data[i].raw) !== "undefined") {
				avgdata = data[i].raw;
			}

			//loop through avgdata to get data for each individual meter
			//key is meter id, val is the data
			$.each(avgdata, function(key, val) {
				//get the data for a single meter
				var singledata = avgdata[key];

				//loop through the params and build the series to add to highcharts
				$.each(params, function(pkey, pval) {
					//get the units for this param
					paramunit = chart_units[pval];

					//add the axis to the chart.
					if (!chart.get(paramunit[0])) {
						chart.addAxis({
							id: paramunit[0],
							title: {
								text: paramunit[0]
							},
							opposite: get_axis(),
							minorGridLineColor: "transparent",
							minorGridLineWidth: 0,
							lineColor: "#aaa",
							lineWidth: 3,
							tickColor: "#aaa"
						});
					}

					//check if the divisor shoule be applied

					if ($.inArray(pval, power_list) >= 0) {
						divisor = 1000;
					} else {
						divisor = 1;
					}

					if ($.inArray(pval, percent_list) >= 0) {
						divisor = 0.01;
					} else {
						divisor = 1;
					}

					var alldata = [];
					var desc = "";
					var isseries = true; //check if series should be added to chart

					$.each(singledata, function(skey, sval) {
						var y = sval[pval];
						var x = sval.time_sent;

						if (typeof(y) !== "undefined") {
							alldata.push([x, y/divisor]);
						} else {
							isseries = false;
							return false;
						}

					});

					//sort the data
					alldata.sort();

					//get the site description
					$.each(userMeters, function(ukey, uval) {
						if (uval.controller_id + "_" + uval.meter_id == key) {
							desc = uval.site_description + " - " + uval.metered_circuit;
						}
					});

					//add the series to highcharts
					if (isseries) {
						chart.addSeries({
							id: key + "-" + pval,
							name: desc + ": " + paramunit[1],
							data: alldata,
							zindex: 1,
							yAxis: paramunit[0]
						});
					}

				});
			});
		} else if (range == "on") { //user requested range data
			//check if we have avg or raw data
			if (typeof(data[i].avg) !== "undefined") {
				var avgdata = data[i].avg;
			} else if (typeof(data[i].raw) !== "undefined") {
				var avgdata = data[i].raw;
			}

			//similarily check that we have range data
			if (typeof(data[i].min) !== "undefined") {
				var mindata = data[i].min;
				var maxdata = data[i].max;
			} else {
				var mindata = false;
				var maxdata = false;
			}


			//loop through avgdata to get data for each individual meter
			//key is meter id, val is the data
			$.each(avgdata, function(key, val) {
				//get the data for a single meter
				var singledata = avgdata[key];
				var minsingle = mindata[key];
				var maxsingle = maxdata[key];

				//loop through the params and build the series to add to highcharts
				$.each(params, function(pkey, pval) {
					//get the units for this param
					paramunit = chart_units[pval];

					//add the axis to the chart.
					if (!chart.get(paramunit[0])) {
						chart.addAxis({
							id: paramunit[0],
							title: {
								text: paramunit[0]
							},
							opposite: get_axis(),
							minorGridLineColor: "transparent",
							minorGridLineWidth: 0,
							lineColor: "#aaa",
							lineWidth: 3,
							tickColor: "#aaa"
						});
					}

					//check if the divisor shoule be applied
					if ($.inArray(pval, power_list) >= 0) {
						divisor = 1000;
					} else if ($.inArray(pval, percent_list) >= 0) {
						divisor = 0.01;
					} else {
						divisor = 1;
					}

					var alldata = [];
					var allmindata = [];
					var allmaxdata = [];
					var rangedata = [];
					var desc = "";
					var isseries = true; //check if series should be added to chart
					var isrange = true; //check if range should be added to chart

					$.each(singledata, function(skey, sval) {
						var y = sval[pval];
						var x = sval.time_sent;

						if (typeof(y) !== "undefined") {
							alldata.push([x, y/divisor]);
						} else {
							isseries = false;
							return false;
						}

					});

					//sort the data
					alldata.sort();

					//loop through the mindata
					$.each(minsingle, function(skey, sval) {
						var y = sval[pval];
						var x = sval.time_sent;

						if (typeof(y) !== "undefined") {
							allmindata.push([x, y/divisor]);
						} else {
							isrange = false;
							return false;
						}

					});

					allmindata.sort();

					//loop through the maxdata
					$.each(maxsingle, function(skey, sval) {
						var y = sval[pval];
						var x = sval.time_sent;

						if (typeof(y) !== "undefined") {
							allmaxdata.push([x, y/divisor]);
						} else {
							isrange = false;
							return false;
						}

					});

					allmaxdata.sort();

					//get the site description
					$.each(userMeters, function(ukey, uval) {
						if (uval.controller_id + "_" + uval.meter_id == key) {
							desc = uval.site_description + " - " + uval.metered_circuit;
						}
					});

					//add the series to highcharts
					if (isseries) {
						chart.addSeries({
							id: key + "-" + pval,
							name: desc + ": " + paramunit[1],
							data: alldata,
							zindex: 1,
							yAxis: paramunit[0]
						});
					}

					//add ranges to highcharts
					//check if there's a range of data to be shown
					if (isrange && mindata && maxdata) {
						for (var i=0; i<allmindata.length; i++) {
							rangedata.push([allmindata[i][0], allmindata[i][1], allmaxdata[i][1]]);
						}

						rangedata.sort();

						chart.addSeries({
							id: "range-" + key + "-" + pval,
							name: "Range: " + desc + ": " + paramunit[1],
							data: rangedata,
							type: "arearange",
							lineWidth: 0,
							linkedTo: ':previous',
							zindex: 0,
							fillOpacity: 0.2,
							yAxis: paramunit[0],
							enableMouseTracking: false
						});

					} else {
						//don't include the show/hide ranges button if there are no ranges to display
						chart.options.exporting.buttons = null;
						chart.options.exporting.buttons = null;
					}

				});
			});
		}


	}

	//add the flags data if reqd
	if (flagsdata) {
		var fordered = [];
		var switchondata = [];
		var switchoffdata = [];
		var sonordered = [];
		var soffordered = [];
		var relayondata = [];
		var relayoffdata = [];
		var ronordered = [];
		var roffordered = [];
		var conondata = [];
		var conoffdata = [];
		var conordered = [];
		var coffordered = [];

		//parse the data
		$.each(flagsdata, function(key, val) {
			var fparam = val.parameter;
			var fparamval = val.value_description;
			var fparamtime = val.time_received;
			var fparamnode = val.node_description;
			fparamtime = XDate.parse(fparamtime);

			if (fparam.indexOf("switch") !== -1) {
				if (fparamval === "on") {
					switchondata.push([fparamtime, fparamnode + ": Panel in Online Mode"]);
				} else {
					switchoffdata.push([fparamtime, fparamnode + ": Panel in Offline Mode"]);
				}
			} else if (fparam.indexOf("controller") !== -1) {
				if (fparamval === "on") {
					conondata.push([fparamtime, fparamnode + ": Relay Set On"]);
				} else {
					conoffdata.push([fparamtime, fparamnode + ": Relay Set Off"]);
				}
			} else if (fparam.indexOf("relay") !== -1) {
				if (fparamval === "on") {
					relayondata.push([fparamtime, fparamnode + ": Relay Confirmed On"]);
				} else {
					relayoffdata.push([fparamtime, fparamnode + ": Relay Confirmed Off"]);
				}
			}

		});

		switchondata.sort();
		switchoffdata.sort();
		conondata.sort();
		conoffdata.sort();
		relayondata.sort();
		relayoffdata.sort();

		//loop through the data to build the data in the form that highcharts wants
		$.each(switchondata, function(key, val) {
			sonordered.push({
				x: val[0],
				title: "S-On",
				text: val[1]
			});
		});

		$.each(switchoffdata, function(key, val) {
			soffordered.push({
				x: val[0],
				title: "S-Off",
				text: val[1]
			});
		});

		$.each(conondata, function(key, val) {
			conordered.push({
				x: val[0],
				title: "C-On",
				text: val[1]
			});
		});

		$.each(conoffdata, function(key, val) {
			coffordered.push({
				x: val[0],
				title: "C-Off",
				text: val[1]
			});
		});

		$.each(relayondata, function(key, val) {
			ronordered.push({
				x: val[0],
				title: "R-On",
				text: val[1]
			});
		});

		$.each(relayoffdata, function(key, val) {
			roffordered.push({
				x: val[0],
				title: "R-Off",
				text: val[1]
			});
		});

		var chart = $("#chart").highcharts();

		chart.addSeries({
			allowPointSelect: true,
			type: 'flags',
			name: 'Switch Events (S-On/ Off)',
			id: 'sonflags',
			shape: 'squarepin',
			visible: true,
			stackDistance: 20,
			y: -70,
			data: sonordered,
			zIndex: 6,
			states: {
				hover: {
					enabled: false
				}
			}
		});

		chart.addSeries({
			allowPointSelect: true,
			type: 'flags',
			name: 'Switch Off Events',
			id: 'soffflags',
			shape: 'squarepin',
			visible: true,
			stackDistance: 20,
			y: -30,
			data: soffordered,
			linkedTo: ":previous",
			zIndex: 5,
			states: {
				hover: {
					enabled: false
				}
			}
		});

		chart.addSeries({
			allowPointSelect: true,
			type: 'flags',
			name: 'Controller Events (C-On/ Off)',
			id: 'conflags',
			shape: 'squarepin',
			visible: true,
			stackDistance: 20,
			y: -110,
			data: conordered,
			zIndex: 4,
			states: {
				hover: {
					enabled: false
				}
			}
		});

		chart.addSeries({
			allowPointSelect: true,
			type: 'flags',
			name: 'Controller Off Events',
			id: 'coffflags',
			shape: 'squarepin',
			visible: true,
			stackDistance: 20,
			y: -150,
			data: coffordered,
			linkedTo: ":previous",
			zIndex: 3,
			states: {
				hover: {
					enabled: false
				}
			}
		});

		chart.addSeries({
			allowPointSelect: true,
			type: 'flags',
			name: 'Relay Events (R-On/ Off)',
			id: 'rflags',
			shape: 'squarepin',
			visible: true,
			stackDistance: 20,
			y: -190,
			data: ronordered,
			zIndex: 2,
			states: {
				hover: {
					enabled: false
				}
			}
		});

		chart.addSeries({
			allowPointSelect: true,
			type: 'flags',
			name: 'Relay Off Events',
			id: 'roffflags',
			shape: 'squarepin',
			visible: true,
			stackDistance: 20,
			y: -230,
			data: roffordered,
			linkedTo: ":previous",
			zIndex: 1,
			states: {
				hover: {
					enabled: false
				}
			}
		});
	}

	//check if the user wants to add the comparison data
	if (compdata) {
		//add a new x-axis for the comp data
		chart.addAxis({
			type: "datetime",
			opposite: true,
			startOnTick: false,
			endOnTick: false,
			maxPadding: 0,
			minPadding: 0,
			minRange:  60 * 1000,
			dateTimeLabelFormats: {
				day: '%H:%M',
				hour: '%H:%M'
			},
			lineColor: "transparent",
			lineWidth: 0,
			minorGridLineWidth: 0,
			minorTicklength: 0,
			minirTickColor: "transparent",
			tickColor: "transparent",
			gridLineWidth: 1,
			gridLineColor: "transparent",
			events : {
				afterSetExtremes : afterSetExtremes
			},
			labels: {
				step: 2,
				enabled: true,
				format: "{value:%d-%b-%Y<br>%H:%M}"
			},
			id: "comp-xaxis"
		}, true);

		for (var i=0; i<compdata.length; i++) {
			var avgdata = [];

			//check if we have avg or raw data
			if (typeof(compdata[i].avg) !== "undefined") {
				avgdata = compdata[i].avg;
			} else if (typeof(compdata[i].raw) !== "undefined") {
				avgdata = compdata[i].raw;
			}

			//loop through avgdata to get data for each individual meter
			//key is meter id, val is the data
			$.each(avgdata, function(key, val) {
				//get the data for a single meter
				var singledata = avgdata[key];

				//loop through the params and build the series to add to highcharts
				$.each(params, function(pkey, pval) {
					//get the units for this param
					paramunit = chart_units[pval];

					//add the axis to the chart.
					if (!chart.get(paramunit[0])) {
						chart.addAxis({
							id: paramunit[0],
							title: {
								text: paramunit[0]
							},
							opposite: get_axis(),
							minorGridLineColor: "transparent",
							minorGridLineWidth: 0,
							lineColor: "#aaa",
							lineWidth: 3,
							tickColor: "#aaa",
							xAxis: 2
						});
					}

					//check if the divisor shoule be applied

					if ($.inArray(pval, power_list) >= 0) {
						divisor = 1000;
					} else {
						divisor = 1;
					}

					if ($.inArray(pval, percent_list) >= 0) {
						divisor = 0.01;
					} else {
						divisor = 1;
					}

					var alldata = [];
					var desc = "";
					var isseries = true; //check if series should be added to chart

					$.each(singledata, function(skey, sval) {
						var y = sval[pval];
						var x = sval.time_sent;

						if (typeof(y) !== "undefined") {
							alldata.push([x, y/divisor]);
						} else {
							isseries = false;
							return false;
						}

					});

					//sort the data
					alldata.sort();

					//get the site description
					$.each(userMeters, function(ukey, uval) {
						if (uval.controller_id + "_" + uval.meter_id == key) {
							desc = uval.site_description + " - " + uval.metered_circuit;
						}
					});

					//add the series to highcharts
					if (isseries) {
						chart.addSeries({
							id: "compare-" + key + "-" + pval,
							name: "Compare: " + desc + ": " + paramunit[1],
							data: alldata,
							zindex: 1,
							yAxis: paramunit[0],
							xAxis: 2
						});
					}

				});
			});
		}
	}


	$("#optionstab").click();
	$("#chartcontainer").animate({opacity: 1}, {duration: 1500, queue: false}, function() {
		chart.reflow();
	});
	$("html body").animate({scrollTop: 0}, {duration: 500, queue: false});

}

function afterSetExtremes(e) {
	var curchart = $("#chart").highcharts('StockChart');
	var start = e.min; // + tzms; //user selected start
	var end = e.max; // + tzms; //user selected end
	var axisid = e.currentTarget.options.id;

	//check if start end = null
	if (start === null) {
		start = curchart.series[0].xData[0];
	}

	if (end === null) {
		var len = curchart.series[0].xData.length;
		end = curchart.series[0].xData[len - 1];
	}

	//check if the tz needs to be taken into account
	//get the timezone offset for the start time
	var startdate = new XDate(start, true);
/* 	var tz_offset_start = startdate.getTimezoneOffset(); */
	var enddate = new XDate(end, true);
/* 	var tz_offset_end = enddate.getTimezoneOffset(); */

/*
	stzms = tz_offset_start * 60000;
	etzms = tz_offset_end * 60000;
	start = start + stzms;
	end = end + etzms;
*/

	//convert to unix time
	start = parseInt(start / 1000);
	end = parseInt(end / 1000);
	var meterparams = JSON.parse(localStorage.getItem("histMeterParams"));
	var reqnum = localStorage.getItem("histReqNum");
	var reqtype = localStorage.getItem("histReqType");
	var range = localStorage.getItem("histShowRanges");
	var resolution = localStorage.getItem("histResolution");
	var iscomp = localStorage.getItem("histIsComp");
	var divisor = 1;
	var requests = [];
	var allresults = [];
	var comprequests = [];
	var allcompresults = [];
	var mindata = [];
	var maxdata = [];
	var chartdata = [];

	if (axisid === 'main-xaxis') {
		//get all the necessary data
		for (var i=0; i<reqnum; i++) {
			var url = localStorage.getItem("histURL-" + i);
			var index = url.indexOf("&start");
			url = url.substring(0, index);
			url += "&resolution=" + resolution + "&callback=";
			url += "&start=" + start + "&end=" + end;

			requests.push($.getJSON(url));
		}

		//wait for all the data to be returned then parse it
		$.when.apply($, requests).done(function() {

			if (reqnum > 1) {
				//store newest resolution
				resolution = arguments[0][0].resolution;
				localStorage.setItem("histResolution", resolution);

				//store the data
				$.each(arguments, function(key, val) {
					allresults.push(val[0]);
				});


			} else {
				//store newest resolution
				resolution = arguments[0].resolution;
				localStorage.setItem("histResolution", resolution);

				//store the data
				allresults.push(arguments[0]);
			}

			//loop through meter params to get the data for the series that we need
			//when type is meter, we have to split params
			var alldatalen = Object.keys(allresults).length;

			for (var i=0; i< alldatalen; i++) {
				//check if we have avg or raw data
				if (typeof(allresults[i].avg) !== "undefined") {
					chartdata = allresults[i].avg;
				} else if (typeof(allresults[i].raw) !== "undefined") {
					chartdata = allresults[i].raw;
				}

				//check if we have range data
				if (typeof(allresults[i].min) !== "undefined") {
					mindata = allresults[i].min;
					maxdata = allresults[i].max;
				} else {
					mindata = false;
					maxdata = false;
				}

				//loop through chart data and get the meter(s) the data applies to
				$.each(chartdata, function(meter, mdata) {
					//get the parameters associated with meter
					meterid = meter;//.replace("_", "-");
					var paramlist = meterparams[meterid];
					var paramarr = paramlist.split(",");

					//loop through the list of parameters for this meter and create the series
					$.each(paramarr, function(index, pname) {
						var seriesdata = [];
						//check if the divisor shoule be applied
						if ($.inArray(pname, power_list) >= 0) {
							divisor = 1000;
						} else if ($.inArray(pname, percent_list) >= 0) {
							divisor = 0.01;
						} else {
							divisor = 1;
						}

						$.each(mdata, function(index, point) {
							var x = point.time_sent;
							var y = point[pname] / divisor;
							seriesdata.push([x,y]);
						});

						seriesdata.sort();

						//build series id
						var sid = meter + "-" + pname;

						//update the series
						var series = curchart.get(sid);
						var test = series.setData(seriesdata);

						//reset seriesdata for next series
						seriesdata = [];

						//update min and max if we have them
						if (mindata && maxdata) {
							$.each(mindata, function(rindex, rval) {
								for (var i=0; i<rval.length; i++) {
									var min = rval[i][pname];
									var max = maxdata[meter][i][pname];
									var time = rval[i].time_sent;
									seriesdata.push([time, min / divisor, max / divisor]);
								}

								seriesdata.sort();

								//update the range data in the chart
								var rid = "range-" + meter + "-" + pname;
								var series = curchart.get(rid);
								var test = series.setData(seriesdata);
								series.setVisible(true);

								seriesdata = [];
							});

						} else {
							var rid = "range-" + meter + "-" + pname;
							var series = curchart.get(rid);
							if (series) {
								series.setVisible(false);
							}
						}

					});
				});
			}

			//update the chart details with the new resolution
			var cinterval = "<ul><li>" + convertInterval(resolution) + "</li></ul>";
			$("#cinterval-val").html(cinterval);

		});
	}

	if (iscomp === "true" && axisid === 'comp-xaxis') {

		for (var i=0; i<reqnum; i++) {
			var url = localStorage.getItem("histCompURL-" + i);
			var index = url.indexOf("&start");
			url = url.substring(0, index);
			url += "&resolution=" + resolution + "&callback=";
			url += "&start=" + start + "&end=" + end;

			comprequests.push($.getJSON(url));
		}

		$.when.apply($, comprequests).done(function() {

			if (reqnum > 1) {

				//store the data
				$.each(arguments, function(key, val) {
					allcompresults.push(val[0]);
				});


			} else {

				//store the data
				allcompresults.push(arguments[0]);
			}

			//loop through meter params to get the data for the series that we need
			//when type is meter, we have to split params
			var alldatalen = Object.keys(allcompresults).length;

			for (var i=0; i< alldatalen; i++) {
				//check if we have avg or raw data
				if (typeof(allcompresults[i].avg) !== "undefined") {
					chartdata = allcompresults[i].avg;
				} else if (typeof(allcompresults[i].raw) !== "undefined") {
					chartdata = allcompresults[i].raw;
				}


				//loop through chart data and get the meter(s) the data applies to
				$.each(chartdata, function(meter, mdata) {
					//get the parameters associated with meter
					meterid = meter;//.replace("_", "-");
					var paramlist = meterparams[meterid];
					var paramarr = paramlist.split(",");

					//loop through the list of parameters for this meter and create the series
					$.each(paramarr, function(index, pname) {
						var seriesdata = [];
						//check if the divisor shoule be applied
						if ($.inArray(pname, power_list) >= 0) {
							divisor = 1000;
						} else if ($.inArray(pname, percent_list) >= 0) {
							divisor = 0.01;
						} else {
							divisor = 1;
						}

						$.each(mdata, function(index, point) {
							var x = point.time_sent;
							var y = point[pname] / divisor;
							seriesdata.push([x,y]);
						});

						seriesdata.sort();

						//build series id
						var sid = "compare-" + meter + "-" + pname;

						//update the series
						var series = curchart.get(sid);
						var test = series.setData(seriesdata);
					});
				});
			}
		});
	}
}

//changes the flag of the axis positioner
function get_axis() {
	axispos = !axispos;
	return axispos;
}

function range_toggle(bool){
	var visible = bool;

	for (i=0;i<chart.series.length;i++) {
		var name=chart.series[i].name;

		if (name.substr(0,5) == "Range") {
			if (visible) {
				chart.series[i].hide();
			} else {
				chart.series[i].show();
			}
		}
	}
}

function analysisDocReady() {

	//clear local storage of any remnants of previous analysis charts
	localStorage.removeItem('histAllMeters');
	localStorage.removeItem('histAllParams');
	localStorage.removeItem('histCompDate');
	localStorage.removeItem('histCompEnd');
	localStorage.removeItem('histCompStart');
	localStorage.removeItem('histIsComp');
	localStorage.removeItem('histShowFlags');
	localStorage.removeItem('histShowRanges');
	localStorage.removeItem('histUpscale');
	localStorage.removeItem('histReqType');
	localStorage.removeItem('histResolution');
	localStorage.removeItem('histMeterParams');

	var urlcount = parseInt(localStorage.getItem('histReqNum'));

	for (var i=0; i<urlcount; i++) {
		localStorage.removeItem('histURL-' + i);
		localStorage.removeItem('histCompURL-' + i);
	}

	localStorage.removeItem('histReqNum');


	var reqparams = [];

	//get the list of available meter-parameters
	getAllowedParams();

	//delete any leftover analysis chart items from localStorage
	var reqnum = localStorage.getItem("histReqNum");
	for (var i=0; i<reqnum; i++) {
		localStorage.removeItem("histURL-" + i);
	}
	localStorage.removeItem("histReqNum");
	localStorage.removeItem("histReqType");

	//bind left and right arrow keys to chart move back/forward
	$(document).keyup(function(e) {
		if (e.keyCode == 37) { //left arrow = move back
			changeExtremes('back');
		}

		if (e.keyCode == 39) { //right arrow = move forward
			changeExtremes('forward');
		}

		if (e.keyCode == 27 && $("#savereportcontain").is(":visible")) {
			$("#savereport").hide("scale", {}, 250, function() {
				$("#savereportcontain").fadeOut(100);
			});
		}
	});

	$("#closewin").bind("click", function(e) {
		if (e.target.id == "closewin") {
			$("#savereport").hide("scale", {}, 250, function() {
				$("#savereportcontain").fadeOut(100);
			});
		}
	});

	$("#cancelreport").bind("click", function(e) {
		e.preventDefault();
		if (e.target.id == "cancelreport") {
			$("#savereport").hide("scale", {}, 250, function() {
				$("#savereportcontain").fadeOut(100);
			});
		}
	});

	$("#savereportcontain").bind("click", function(e) {
		if (e.target.id == "savereportcontain") {
			$("#savereport").hide("scale", {}, 250, function() {
				$("#savereportcontain").fadeOut(100);
			});
		}
	});

	$("#showadv").on("click", function() {
		$("#advoptions").slideToggle(500);
		$(this).html() == "View Advanced Options" ? $(this).html("Hide Advanced Options") : $(this).html("View Advanced Options");

	});

	$("#upscale").chosen({
		max_selected_options: 1,
		placeholder_text_multiple: "Please select a data resolution. Start typing to search.",
		width: "500px"
	});

	$("#upscale").on("change", function(e, p) {
		var res = "";
		if (p.selected) {
			res = p.selected;
			switch(res) {
				case "day":
				case "week":
				case "month":
					$("#dtflags").slideUp(500, function() {
						setFlags("flagsoff");
						$("#flagsoff").removeClass("selected-button");
						$("#flags").val("");
					});
					break;
				default:
					$("#dtflags").slideDown(500);
					break;
			}
		}
	});

	$("#save-period").chosen({
		max_selected_options: 1,
		placeholder_text_multiple: "Please select a time period. Start typing to search.",
		width: "500px"
	});

	$("#save-period").on("change", function(e, p) {
		if (p.selected) {
			$("#save-data").removeAttr("disabled");
			$("#save-data").trigger("chosen:updated");
			checkResolution("save-data", "save-report", p.selected);
		} else if (p.deselected) {
			$("#save-data").attr("disabled", "disabled");
			$("#save-data").trigger("chosen:updated");
		}
	});

	$("#save-data").chosen({
		max_selected_options: 1,
		placeholder_text_multiple: "Please select a data interval. Start typing to search.",
		width: "500px"
	});

	//set up datetime picker
	var rightnow = new XDate();
	rightnow = rightnow.toString("ddd, dd MMM yyyy HH:mm");

	var yesterday = new XDate();
	yesterday.addDays(-1);
	yesterday = yesterday.toString("ddd, dd MMM yyyy HH:mm");

	var thisyear = new XDate();
	thisyear = thisyear.toString("yyyy");

	var compday = new XDate();
	compday.addDays(-1);
	compday = compday.toString("ddd, dd MMM yyyy");

	$("#dpicker_from").datetimepicker({
		value: yesterday,
		step: 30,
		maxDate: rightnow,
		yearStart: 2013,
		yearEnd: thisyear,
		format: "D, d M Y H:i",
		onChangeDateTime: function(dp, $input) {
			checkResolution("upscale");

			$("#dtshortcuts > a").each(function() {
				$(this).removeClass("selected-button");
			});
		}
	});

	$("#dpicker_to").datetimepicker({
		value: rightnow,
		step: 30,
		maxDate: rightnow,
		yearStart: 2013,
		yearEnd: thisyear,
		format: "D, d M Y H:i",
		onChangeDateTime: function(dp, $input) {
			checkResolution("upscale");
			$("#dtshortcuts > a").each(function() {
				$(this).removeClass("selected-button");
			});
		}
	});

	$("#dpicker_compare_from").datetimepicker({
		value: compday,
		maxDate: rightnow,
		yearStart: 2013,
		yearEnd: thisyear,
		format: "D, d M Y",
		timepicker: false,
		closeOnDateSelect: true
	});

	//build the first meter and params dropdowns for the form
	buildMeterParams();

	$(document).on("divresize", function() {
		var chart = $("#chart").highcharts('StockChart');
		if (chart) {
			setTimeout(function() {chart.reflow();}, 1000);
		}
	});

	$('input[name="dtcompare-on"]').on("change", function(e) {
		if ($('input[name="dtcompare-on"]:checked').val() == 0) {
			$("#dtcompare-date").fadeOut(500);
		} else {
			$("#dtcompare-date").fadeIn(500);
		}
	});

	//select the first time period
	shortDate('prevHour');

	$("#histachart").submit(function (e) {
		e.preventDefault();

		//hide the list of error messages and hide the chart options
		$("#message-close").click();

		//get all the meters and their related parameters
		var allmeterparams = [];
		var allmeters = [];
		var allparams = [];
		var meterlist = "";
		var pobj = {};
		var paramlist = "";
		var mobj = {};
		var rtype = "";
		var rnum = 0;
		var errmsg = "There was an error processing your request. Please check the following issue(s).";
		errmsg += " Once the issues are rectified, please try again.";
		errmsg += '<ul>';
		var haserror = false;
		var cobj = {};
		var range;
		var flags;

		//ensure that the first build flag is true
		firstAnalysisBuild = true;

		//get the user provided data from the options form
		var start = new XDate($("#dpicker_from").val());
		var sy = start.clone().getFullYear();
		var smh = start.clone().getMonth();
		var sd = start.clone().getDate();
		var sh = start.clone().getHours();
		var sm = start.clone().getMinutes();
		var ss = start.clone().getSeconds();
		start = parseInt(start.setUTCFullYear(sy).setUTCMonth(smh).setUTCDate(sd).setUTCHours(sh).setUTCMinutes(sm).setUTCSeconds(ss).getTime() / 1000);
		var end = new XDate($("#dpicker_to").val());
		var ey = end.clone().getFullYear();
		var emh = end.clone().getMonth();
		var ed = end.clone().getDate();
		var sh = end.clone().getHours();
		var sm = end.clone().getMinutes();
		var ss = end.clone().getSeconds();
		end = parseInt(end.setUTCFullYear(ey).setUTCMonth(emh).setUTCDate(ed).setUTCHours(sh).setUTCMinutes(sm).setUTCSeconds(ss).getTime() / 1000);

		var duration = end - start;
		var upscale = $("#upscale").val();
		var iscomp = parseInt($('input[name="dtcompare-on"]:checked').val());

		if (upscale !== null) {
			upscale = upscale[0];
		}

		//check if user wants ranges
		$("#range").val() == "rangeson" ? range = "on" : range = "off";
		localStorage.setItem("histShowRanges", range);

		//check if user wants flags
		$("#flags").val() == "flagson" ? flags = "on" : flags = "off";
		localStorage.setItem("histShowFlags", flags);

		//check that user has provide all the necessary info to build a chart
		if (start >= end) {
			errmsg += "<li>The specified starting data is later than the end date. Please check your dates.</li>";
			haserror = true;
		}

		//loop through each meter parameter pair
		$("#dtmeters .meterparams").each(function(key, val) {
			//get the id of the container
			var id = $(val).attr("id").split("-");
			id = id[1];

			var paramval = $("#param-" + id).val();
			var meterval = $("#meter-" + id).val();

			if (paramval !== null && meterval !== null) {
				paramval.sort();

				$.each(paramval, function(pkey, pval) {
					allmeterparams.push([meterval, pval]);
				});

			} else if (paramval === null && meterval !== null) {
				errmsg += "<li>Please ensure that you have selected at least one parameter for all your metered circuits.</li>";
				haserror = true;
			} else if (paramval === null && meterval === null) {
				errmsg += "<li>Please select a metered circuit and parameter.";
				errmsg += " If you do not want to analyse any further metered circuits please click the ";
				errmsg += '<span style="font-family: ionicons;">&#xf2bb;</span> icon in the relevant chart options.';
				haserror = true;
			}

		});

		errmsg += "</ul>";

		if (!haserror) {

			//get number of meters and parameters
			for (var i=0; i<allmeterparams.length; i++) {
				var meter = allmeterparams[i][0];
				var param = allmeterparams[i][1];

				if (allmeters.indexOf(meter) == -1) {
					allmeters.push(meter);
				}

				if (allparams.indexOf(param) == -1) {
					allparams.push(param);
				}
			}

			//check if compare data is reqd
			if (iscomp) {
				//get the hours and minutes and seconds for the start time
				var cstart = new XDate(start * 1000);
				var cshours = cstart.getHours();
				var csmins = cstart.getMinutes();
				var cssecs = cstart.getSeconds();
				var compdate = new XDate($("#dpicker_compare_from").val()).setHours(12); //set the time to 12pm so that the timezone doesn't cause problems

				//set the time
				var compstart = compdate.clone().setHours(cshours).setMinutes(csmins).setSeconds(cssecs);
				var compend = compstart.clone().addSeconds(duration);

				compstart = parseInt(compstart.getTime() / 1000);
				compend = parseInt(compend.getTime() / 1000);

				localStorage.setItem("histIsComp", true);
				localStorage.setItem("histCompStart", compstart);
				localStorage.setItem("histCompEnd", compend);
			} else {
				localStorage.setItem("histIsComp", false);
			}

			//save the list of meters and params to localStorage
			localStorage.setItem("histAllMeters", allmeters);
			localStorage.setItem("histAllParams", allparams);
			localStorage.setItem("histUpscale", upscale);

			//loop through the params that we need and find the list of corresponding meters
			//first by parameter
			for (var i=0; i<allparams.length; i++) {
				var cur_param = allparams[i];

				for (var j=0; j<allmeterparams.length; j++) {
					if (cur_param == allmeterparams[j][1]) {
						var conmeter = allmeterparams[j][0];
						conmeter = conmeter[0].replace("_", ",");
						meterlist += conmeter + ",,";
					}
				}

				meterlist = meterlist.slice(0, -2);
				pobj[cur_param] = meterlist;
				meterlist = "";
			}

			//secondly by meter
			for (var i=0; i<allmeters.length; i++) {
				var cur_meter = allmeters[i];

				for (var j=0; j<allmeterparams.length; j++) {
					if (cur_meter == allmeterparams[j][0]) {
						paramlist += allmeterparams[j][1] + ",";
					}
				}

				paramlist = paramlist.slice(0, -1);
				mobj[cur_meter] = paramlist;
				paramlist = "";
			}

			//calc number of required requests
			var pnum = Object.keys(pobj).length;
			var mnum = Object.keys(mobj).length;

			//check if a parameter-based or meter-based call will result in the fewest calls
			if (mnum <= pnum) {
				rtype = "meter";
				cobj = mobj;
				//build the required number of urls and store them in localStorage for calling later
				$.each(mobj, function(key, val) {

					var url = "/api/historical?sources=" + key.replace("_", ",") + "&params=" + val + "&range=" + range + "&start=" + start + "&end=" + end;

					if (iscomp) {
						var compurl = "/api/historical?sources=" + key.replace("_", ",") + "&params=" + val + "&range=off" + "&start=" + compstart + "&end=" + compend;
					}

					if (upscale !== null) {
						url += "&upscale=" + upscale;
						compurl += "&upscale=" + upscale;
					}

					localStorage.setItem("histURL-" + rnum, url);
					localStorage.setItem("histCompURL-" + rnum, compurl);
					rnum++;
				});

				//store the flags url
				if (flags == "on") {
					flagurl = "/api/historical/flags?start=" + start + "&end=" + end;
					localStorage.setItem("histFlagsURL", flagurl);
				}

				//store the number and type of requests that we need to make
				localStorage.setItem("histReqNum", rnum);
				localStorage.setItem("histReqType", rtype);

				//store the mobj - useful for parsing data later
				localStorage.setItem("histMeterParams", JSON.stringify(mobj));

			} else {
				rtype = "param";
				cobj = pobj;
				//build the required number of urls and store them in localStorage for calling later
				$.each(pobj, function(key, val) {

					var url = "/api/historical?sources=" + val + "&params=" + key + "&range=" + range + "&start=" + start + "&end=" + end;

					if (iscomp) {
						var compurl = "/api/historical?sources=" + val + "&params=" + key + "&range=" + range + "&start=" + compstart + "&end=" + compend;
					}

					if (upscale !== null) {
						url += "&upscale=" + upscale;
						compurl += "&upscale=" + upscale;
					}

					localStorage.setItem("histURL-" + rnum, url);
					localStorage.setItem("histCompURL-" + rnum, compurl);
					rnum++;
				});

				//store the flags url
				if (flags == "on") {
					flagurl = "/api/historical/flags?start=" + start + "&end=" + end;
					localStorage.setItem("histFlagsURL", flagurl);
				}

				//store the number and type of requests that we need to make
				localStorage.setItem("histReqNum", rnum);
				localStorage.setItem("histReqType", rtype);

				//store the mobj - useful for parsing data later
				localStorage.setItem("histMeterParams", JSON.stringify(mobj));
			}

			//get the data
			getAnalysisData();

			//populate the chart details
			buildChartDetails(cobj, start, end, upscale, rtype);
		} else {
			showMessage("err", errmsg);
		}

	});

	$("#optionstab").on("click", function() {

		if ($("#optionstabtitle").html() == "Hide Chart Options") {

			$("#options").animate({top: ($("#options").height() - 60) * -1}, {duration: 500, queue: false});
			$("#optionstabtitle").html("Show Chart Options");
			$("#optionstabup").fadeOut({duration: 50, queue: false});
			$("#optionstabdown").fadeIn({duration: 50, queue: false});

		} else {

			$("#options").animate({top: "20px"}, {duration: 500, queue: false});
			$("#optionstabtitle").html("Hide Chart Options");
			$("#optionstabdown").fadeOut({duration: 50, queue: false});
			$("#optionstabup").fadeIn({duration: 50, queue: false});

		}
	});

	//check if there are url parameters being passed to the page, and update the drop downs etc
	var urlparameters = getURLparams();

	if (urlparameters) {
		//check if we're coming from the dispatches page
		var referrer = urlparameters.type;

		if (referrer !== "dispatch") {
			var meterparams = urlparameters.meterparams;
			var period = urlparameters.time_period;
			var resolution = urlparameters.resolution;
			var type = urlparameters.type;

			if (meterparams && period && resolution) {
				//build array of requested sources
				//loop through meterparams and build the list of required meters and paramters
				//for meter type, params need to be split
				if (type === "param") {

					meterparams = convertPtoM(meterparams);
				}

				var counter = 0;

				$.each(meterparams, function(meter, params) {
					var meterid = meter;
					var plist = params.split(",");

					if (!$("#meter-" + counter).length) {
						buildMeterParams();
					}

					//check if the required meter/ param drop downs exist
					$("#meter-" + counter + " option").each(function(index, el) {
						if ($(el).val() === meterid) {
							$(el).attr("selected", "selected");
							$("#meter-" + counter).trigger("chosen:updated");
							//update the params list - empty until user picks a meter
							//change event is not fired when meter is set programmably,
							//so the function to create the list is manually called
							updateAllowedParams(meterid, counter, "add");
						} else {
							$(el).removeAttr("selected");
						}
					});

					$("#param-" + counter + " option").each(function(index, el) {
						$.each(plist, function(index, pname) {

							if ($(el).val() === pname) {
								$(el).attr("selected", "selected");
							}
						});
					});

					$("#param-" + counter).trigger("chosen:updated");

					counter++;
				});

				//update the time period
				shortDate(period);

				//update the resolution
				$("#upscale option").each(function(index, el) {
					if ($(el).val() === resolution) {
						$(el).attr("selected", "selected");
						$("#upscale").trigger("chosen:updated");
					} else {
						$(el).removeAttr("selected");
						$("#upscale").trigger("chosen:updated");
					}
				});

				//submit the chart
				$("#histachart").submit();
			}
		} else if (referrer === "dispatch") {
			var start = new XDate(urlparameters.start).toString("ddd, dd MMM yyyy HH:mm");;
			var end = new XDate(urlparameters.end).toString("ddd, dd MMM yyyy HH:mm");;
			var sources = urlparameters.sources;

			//break sources into array
			var allsources = sources.split(",,");

			var counter = 0;
			//loop through each source and select the corresponding dropdown
			$.each(allsources, function(key, conmeter) {
				var meterid = conmeter.replace(",", "_");

				if (!$("#meter-" + counter).length) {
					buildMeterParams();
				}

				$("#meter-" + counter + " option").each(function(index, el) {
					if ($(el).val() === meterid) {
						$(el).attr("selected", "selected");
						$("#meter-" + counter).trigger("chosen:updated");
						//update the params list - empty until user picks a meter
						//change event is not fired when meter is set programmably,
						//so the function to create the list is manually called
						updateAllowedParams(meterid, counter, "add");
					} else {
						$(el).removeAttr("selected");
					}
				});

				//only add the sys_w parameter
				$("#param-" + counter + " option").each(function(index, el) {
					if ($(el).val() === "sys_w") {
						$(el).attr("selected", "selected");
					}
				});

				$("#param-" + counter).trigger("chosen:updated");

				counter++;

			});

			//set up the date time
			$("#dpicker_from").datetimepicker({
				value: start,
				format: "D, d M Y H:i"
			});

			$("#dpicker_to").datetimepicker({
				value: end,
				format: "D, d M Y H:i"
			});

			//check the allowable data resolutions
			checkResolution("upscale");

			//submit the chart
			$("#histachart").submit();
		}
	}

	$("#createreport").submit(function(e) {
		e.preventDefault();
		var meterparams = $("#savereport-data").val();
		var reqtype = $("#savereport-reqtype").val();

		$("#save-title").val("");
		$("#save-period > option").each(function() {
			$(this).removeAttr("selected");
		});
		$("#save-period").trigger("chosen:updated");
		$("#save-data > option").each(function() {
			$(this).removeAttr("selected");
		});
		$("#save-data").trigger("chosen:updated");
		$("#save-meterparams").val(meterparams);
		$("#save-reqtype").val(reqtype);
		$("#savereportcontain").height($(document).height());
		$("#savereportcontain").fadeIn(100, function() {
			$("#savereport").show("scale", {}, 250, function() {
				$("html body").animate({scrollTop: $("#savereport").offset().top - 50}, 500);
			});
		});

	});

	$("#savereport").submit(function(e) {
		e.preventDefault();

		var meterparams = JSON.parse($("#save-meterparams").val());
		var title = $("#save-title").val();
		var period = $("#save-period").val();
		var resolution = $("#save-data").val();
		var redirect = $("#save-redirect").prop("checked");
		var reqtype = $("#save-reqtype").val();
		var iserr = false;
		var errmsg = "There was a problem saving your report. Please check the following issues and try again.<ul>";


		//check if the user has supplied a time period
		if (period === null) {
			errmsg += "<li>Please select a time period for your report.</li>";
			iserr = true;
		} else {
			period = period[0];
		}

		//check if user has supplied a title
		if (title === "") {
			errmsg += "<li>Please select a title for your report.</li>";
			iserr = true;
		}

		//check if user has supplied resolution
		if (resolution === null) {
			errmsg += "<li>Please select a data interval for your report.</li>";
			iserr = true;
		} else {
			resolution = resolution[0];
		}

		if (iserr) {
			errmsg += "</ul>";
			showMessage("err", errmsg);
		}

		//build the object to hold the report options
		//new reports will be placed at top of the page
		var ropts = {
			meterparams: meterparams,
			time_period: period,
			resolution: resolution,
			title: title,
			type: reqtype,
			position: 0
		};

		//check if user wants to go to reports page
		if (redirect && !iserr) { //user wants to save report and view on reporting page
			var saved = addReportLayout(ropts);
			if (saved) {
				showMessage("info", "Your report was successfully saved. Redirecting you to the Reporting page.");
				//send user to reporting page
				document.location.href = "/portal/history/reporting";
			} else {
				showMessage("err", defaultErrMsg);
			}

		} else if (!redirect && !iserr) { //user just wants to save report

			var saved = addReportLayout(ropts);
			if (saved) {
				showMessage("info", "Your report was successfully saved.");
				//close the save reports screen
				$("#closewin").trigger("click");
			} else {

				showMessage("err", defaultErrMsg);
			}
		}

	});

	$("#addmeter").on("click", function(e) {
		buildMeterParams();
	});


}

function loadHist(val) {
	window.location = "/portal/history/analysis?" + val;
}

function reportingDocReady() {
	//catch div resizes
	$(document).on("divresize", function() {
		var allcharts = $(".reportchart");
		$.each(allcharts, function(key, val) {
			var charts = val.id.split("-");
			setTimeout(function() {Highcharts.charts[parseInt(charts[1])].reflow();}, 1000);
		});
	});

	//show loading
    Pace.start();

	var sDashReports = [];

	$("#whistory").sDashboard({
		dashboardData: sDashReports
	});

	var setup = $.when(
		//get the meters that user has access to
		$.ajax({
			url: "/user/meter",
			type: "GET",
			dataType: "JSON",
			async: false,
			success: function(result) {
				userMeters = result;
			}
		}),

		//get the users reports
		$.ajax({
			url: '/user/config/report',
			type: "GET",
			dataType: "JSON"
		})
	).done(function() {
		if (arguments[0][1] === "success") {
			userMeters = arguments[0][0];
		} else {
			//display error message
			showMessage("err", defaultErrMsg);
		}

		//store user reports
		if (arguments[1][1] === "success") {
			userReports = arguments[1][0];
		} else {
			showMessage("err", defaultErrMsg);
		}

		return arguments;
	}).fail(function() {
		if (arguments[0].status == 404) {
			userReports = {};
            Pace.stop();
		}
	});

	//only start the process of building reports once we have all the necessary data
	$.when(setup).done(function() {
		//loop through each report and get the data reqd to build a report
		var numreports = Object.keys(userReports).length;
		if (numreports > 0) {
			console.time("Building charts took:");
			//add reports to dashboard in reverse order
			for (var i=(numreports - 1); i>=0; i--) {
				buildHistReport(userReports[i]);
			}
			console.timeEnd("Building charts took:");
			//hide splash
		}
            Pace.stop();
	});

}

function buildHistReport(parameters) {
	var meterparams = parameters.meterparams;
	var numrequests = meterparams.length;
	var period = parameters.time_period;
	var resolution = parameters.resolution;
	var title = parameters.title;
	var pos = parameters.position;
	var type = parameters.type;
	var dataurls = [];
	var requests = [];
	var charts = {};
	var mplist = "";
	axispos = true;

	//get the reqd start and end times
	//get the start end end dates
	var times = createStartFromPeriod(period);
	var start = XDate.parse(times[0]) / 1000; //convert from ms to s
	var end = XDate.parse(times[1]) / 1000;

	//build the data urls to retrieve the data from the api
	//depends on whether meter or param type
	if (type === "meter") {
		$.each(meterparams, function(source, params) {
			source = source.replace("_", ",");
			dataurls.push(
				['/api/historical?range=off&resolution=' + resolution + '&upscale=' + resolution + '&sources=' + source + '&params=' + params + '&start=' + start + '&end=' + end, source, params]
			);

			mplist += '<li>' + convertSources(source.replace("_", ",")) + " - " + convertParams(params) + "</li>";
		});
	} else if (type === "param") {
		$.each(meterparams, function(param, sources) {
			dataurls.push(
				['/api/historical?range=off&resolution=' + resolution + '&upscale=' + resolution + '&sources=' + sources + '&params=' + param + '&start=' + start + '&end=' + end, sources, param]
			);

			mplist += '<li>' + convertSources(sources) + " - " + convertParams(param) + '</li>';
		});
	}

	//build the chart object to hold the data
	var chartinfo = '<div id="charts-' + pos + '" class="reportchart"></div>';
	chartinfo += '<div class="reportInfo">';
	chartinfo += '<button onclick="analyseReport(' + pos + ')">Perform Detailed Analysis</button><h3>Report Information</h3>';
	chartinfo += '<div class="reportLabel">Metered Circuits &amp; Parameters:<ul>' + mplist + '</ul></div>';
	chartinfo += '<div class="reportLabel">Time Period:<ul><li>' + convertPeriod(period) + '</li></ul></div>';
	chartinfo += '<div class="reportLabel">Data Resolution:<ul><li>' + convertInterval(resolution) + '</li></ul></div>';
	chartinfo += '<input type="hidden" id="report-parameters-' + pos + '" value="' + encodeURI(JSON.stringify(parameters)) + '">'  + '</div>';

	$("#whistory").sDashboard('addWidget', {
		widgetTitle: title,
		widgetId: "report-" + pos,
		widgetContent: chartinfo
	});

	//setup the chart
	var options = {
		chart: {renderTo: 'charts-' + pos, type: "spline", animation: true,	borderWidth: 0, backgroundColor: "#fff",
			plotBackgroundColor: "#fff", plotShadow: false,	reflow: true, shadow: false},
		navigator : {enabled: false},
		scrollbar: {liveRedraw: false},
		credits: false,
		lang: {noData: " "},
		legend: {enabled: true,	backgroundColor: '#FFFFFF',	itemDistance: 30, shadow: false},
		noData: {style: {fontWeight: 'bold',fontSize: '15px',color: '#303030'}},
		title: {align: 'center',text: "DSU - " + title},
		xAxis: [{type: "datetime",startOnTick: false,maxPadding: 0.1,minPadding: 0.1,minRange:  60 * 1000,
			dateTimeLabelFormats: {day: '%H:%M',hour: '%H:%M'},lineColor: "#aaa",lineWidth: 3,tickColor: "#aaa",
			labels: {step: 2,enabled: true,format: "{value:%d-%b-%Y<br>%H:%M}"}}],
		yAxis: {opposite: false,minorGridLineColor: "transparent",minorGridLineWidth: 0,lineColor: "#aaa",
			lineWidth: 3,tickColor: "#aaa"},
		global: {useUTC: true},
		scrollbar: {enabled: false},
		plotOptions: {series: {marker: {enabled: false}}},
		tooltip: {crosshairs: true,	shared: true,valueDecimals: 2,followPointer: false},
		exporting: {sourceWidth: 850,sourceHeight: 600,	chartOptions: {rangeSelector: {enabled: false}}},
		rangeSelector: {enabled: false}
	};
	var axes_list = [];
	var paramunit;

	charts[pos] = new Highcharts.StockChart(options);
	charts[pos].yAxis[0].remove();
	charts[pos].showLoading("Retrieving your data from the database");

	//get each series singly
	$.each(dataurls, function(index, url) {

		//url[0] is the api url
		//url[1] is the list of source(s)
		//url[2] is the list of param(s)
		$.when($.getJSON(url[0])).done(function() {
			//store data - can be raw or avg
			var alldata = [];

			if (typeof(arguments[0].avg) !== "undefined") {
				alldata = arguments[0].avg;
			} else if (typeof(arguments[0].raw) !== "undefined") {
				alldata = arguments[0].raw;
			} else {
				showMessage("err", defaultErrMsg);
			}

			//if we have a param type, then we have to split meters
			//param will be url[2]
			if (type === "param") {
				var meters = url[1].split(",,");
				var param = url[2];

				//add axis and units
				var paramunit = chart_units[param];

				//add the axis for the parameter
				if (param in chart_units) {
					axes_list.push(chart_units[param][0]);
				}

				//add the axis to the chart.
				if (!charts[pos].get(paramunit[0])) {
					charts[pos].addAxis({
						id: paramunit[0],
						title: {
							text: paramunit[0]
						},
						opposite: false,
						minorGridLineColor: "transparent",
						minorGridLineWidth: 0,
						lineColor: "#aaa",
						lineWidth: 3,
						tickColor: "#aaa"
					}, false);
				}

				if ($.inArray(param, power_list) >= 0) {
					divisor = 1000;
				} if ($.inArray(param, percent_list) >= 0) {
					divisor = 0.01;
				} else {
					divisor = 1;
				}

				$.each(meters, function(key, meterid) {
					var series = [];
					meterid = meterid.replace(",","_");

					var singledata = alldata[meterid];

					$.each(singledata, function(index, rawdata) {
						series.push([rawdata.time_sent, rawdata[param] / divisor]);
					});

					series.sort();

					charts[pos].addSeries({
						id: "chartdata-" + pos,
						name: convertConMeter(meterid) + ": " + convertParams(param),
						data: series,
						yAxis: paramunit[0]
					});

					series = [];
				});
			} else if (type === "meter") {
				//if we have a meter type, then params will have to be split
				var params = url[2].split(",");
				var meterid = url[1].replace(",", "_");

				//add the axes and units for each param
				$.each(params, function(index, param) {
					//add axis and units
					var paramunit = chart_units[param];

					//add the axis for the parameter
					if (param in chart_units) {
						axes_list.push(chart_units[param][0]);
					}

					//add the axis to the chart.
					if (!charts[pos].get(paramunit[0])) {
						charts[pos].addAxis({
							id: paramunit[0],
							title: {
								text: paramunit[0]
							},
							opposite: false,
							minorGridLineColor: "transparent",
							minorGridLineWidth: 0,
							lineColor: "#aaa",
							lineWidth: 3,
							tickColor: "#aaa"
						});
					}

					if ($.inArray(param, power_list) >= 0) {
						divisor = 1000;
					} if ($.inArray(param, percent_list) >= 0) {
						divisor = 0.01;
					} else {
						divisor = 1;
					}

					var singledata = alldata[meterid];
					var series = [];
					$.each(singledata, function(index, rawdata) {
						series.push([rawdata.time_sent, rawdata[param] / divisor]);
					});

					series.sort();

					charts[pos].addSeries({
						id: "chartdata-" + pos,
						name: convertConMeter(meterid) + ": " + convertParams(param),
						data: series,
						yAxis: paramunit[0]
					});
				});
			}

			charts[pos].hideLoading();
		});

	});
}

function analyseReport(id) {
	var reportData = encodeURI(JSON.stringify(userReports[id]));
	document.location.href = "/portal/history/analysis?" + reportData;
}

function getMeterList(select_id) {
	//only get the meter list if we haven't already gotten it
	if ($.isEmptyObject(userMeters)) {
		var prev_id = select_id - 1;

		$.ajax({
			url: "/user/meter",
			type: "GET",
			dataType: "JSON",
			async: false,
			success: function(result) {
				userMeters = result;

				$.each(userMeters, function(key, val) {
					var id = val.controller_id + '_' + val.meter_id;

					if ($.inArray(id, usedMeters) == -1) {
						var mhtml = '<option';

						if ($.inArray(id, reqmeters) >= 0) {
							mhtml += ' selected="selected"';
						}

						mhtml += ' value="' + id + '">' + val.site_description + " - " + val.metered_circuit + '</option>';

						$("#meter-" + select_id).append(mhtml);
					}
				});
			},
			error: function(obj, sts, err) {
				showMessage("err", defaultErrMsg);
			}
		});
	} else if (!$.isEmptyObject(userMeters) && select_id >= 0) {
		$.each(userMeters, function(key, val) {
			var id = val.controller_id + '_' + val.meter_id;

			if ($.inArray(id, usedMeters) == -1) {
				var mhtml = '<option';

				if ($.inArray(id, reqmeters) >= 0) {
					mhtml += ' selected="selected"';
				}

				mhtml += ' value="' + id + '">' + val.site_description + " - " + val.metered_circuit + '</option>';

				$("#meter-" + select_id).append(mhtml);
			}
		});
	}

	$("#meter-" + select_id + " > option").tsort();
	$("#meter-" + select_id).trigger("chosen:updated");
}

function buildMeterParams() {
	var numAvailMeters = userMeters.length;
	var numUsedMeters = usedMeters.length;

	if (typeof(numAvailMeters) === "undefined") {
		numAvailMeters = 2; //set a basic default if userMeters is not populated yet
	}

	if (numUsedMeters < (numAvailMeters)) {
		//get the last id of the meter/params and next id
		var lastid = $("#dtmeters select").last().attr("id");
		var nextid;

		//check if this is the first or subsequent set of elements to build
		if (typeof lastid !== "undefined") {
			lastid = lastid.split("-");
			lastid = parseInt(lastid[1]);
			nextid = lastid + 1;
		} else {
			nextid = 0;
		}

		//check if there's a value for the last meter & parameter
		var lastmeterval = $("#meter-" + lastid).val();
		var lastparamval = $("#param-" + lastid).val();

		if (nextid === 0 || (lastmeterval && lastparamval)) {
			var mphtml = '<div id="mp-' + nextid + '" class="meterparams">';
			mphtml += '<label for="meter-' + nextid + '" class="textlabel">Metered Circuit: </label>';
			mphtml += '<label for="param-' + nextid + '" class="textlabel">Parameter(s): </label>';
			mphtml += '<select id="meter-' + nextid + '" name="meter-' + nextid + '" multiple="multiple"></select>';
			mphtml += '<select id="param-' + nextid + '" name="param-' + nextid + '" multiple="multiple" data-placeholder="Please select a metered circuit first">';
			mphtml += '<option disabled="disabled">Please select a metered circuit first</option>';
			mphtml += '</select>';

			if (nextid !== 0) {
				mphtml += '<div class="removemeter"><a href="javascript: removeMeter(' + nextid + ');" title="Remove this meter and parameter">&#xf2bb;</a></div>';
			}

			mphtml += '</div>';

			$("#addmeter").before(mphtml);
			$("#mp-" + nextid).slideDown({duration: 500, queue: false});

			getMeterList(nextid);

			$("#meter-" + nextid).chosen({
				max_selected_options: 1,
				placeholder_text_multiple: "Please select a metered circuit.",
				width: "250px"
			});

			$("#meter-" + nextid).on("change", function(e, p) {
				var id = $(this).attr('id').split("-").pop();

				if (p.selected) {
					updateAllowedParams(p.selected, id, "add");
					usedMeters.push(p.selected);
				} else if (p.deselected) {
					updateAllowedParams(p.deselected, id, "remove");
					for (var i = 0; i < usedMeters.length; i++) {
						if (usedMeters[i] == p.deselected) {
							usedMeters.splice(i, 1);
						}
					}
				}
			});

			$("#param-" + nextid).chosen({
				placeholder_text_multiple: "Please select a metered circuit first.",
				width: "250px"
			});

		} else if (!(lastmeterval || lastparamval)) {

			if (nextid !== 1) {
				var errmsg = 'Please select a metered circuit and/or parameter.';
				errmsg += 'If you do not want to analyse any furthermetered circuits, please click the ';
				errmsg += '<span style="font-family: ionicons;">&#xf2bb;</span> icon in the relevant chart options';
			} else if (nextid == 1) {
				var errmsg = 'You must select at least one metered circuit and one parameter to generate an analysis chart.';
				errmsg += ' Please check your selection below and try again.';
			}

			showMessage("err", errmsg);
		}
	}
}

function removeMeter(id) {
		$("#mp-" + id).slideUp(500, function() {
			$("#mp-" + id).remove();
		});
}

//get the data to build the analysis chart
function getAnalysisData() {
	//show the splash screen
	Pace.start();
	//get the required request data from localStorage
	var reqnum = localStorage.getItem("histReqNum");
	var meters = localStorage.getItem("histAllMeters");
	var params = localStorage.getItem("histAllParams");
	var range = localStorage.getItem("histShowRanges");
	var flags = localStorage.getItem("histShowFlags");
	var iscomp = localStorage.getItem("histIsComp");

	var requests = [];
	var allresults = [];
	var comprequests = [];
	var allcompresults = [];

	//get all the necessary data
	for (var i=0; i<reqnum; i++) {
		var url = localStorage.getItem("histURL-" + i);
		requests.push($.getJSON(url));
	}

	if (iscomp === "true") {
		for (var i=0; i<reqnum; i++) {
			var url = localStorage.getItem("histCompURL-" + i);
			comprequests.push($.getJSON(url));
		}
	}

	//get the flags data if reqd
	var flags = localStorage.getItem("histShowFlags");

	if (flags === "on") {
		var flagsurl = localStorage.getItem("histFlagsURL");
		if (flagsurl) {
			var flagsdata;

			$.ajax({
				url: flagsurl,
				dataType: "JSON",
				type: "GET",

				success: function(result) {
					flagsdata = result;
				},
				error: function(o, s, e) {
					flagsdata = false;
				}
			});
		}
	}

	//wait for all the data to be returned then parse it
	$.when.apply($, requests).done(function() {

		if (reqnum > 1) {
			localStorage.setItem("histResolution", arguments[0][0].resolution);
			$.each(arguments, function(key, val) {
				allresults.push(val[0]);
			});
		} else {
			localStorage.setItem("histResolution", arguments[0].resolution);
			allresults.push(arguments[0]);
		}

		if (iscomp === "true") {
			$.when.apply($, comprequests).done(function() {

				if (reqnum > 1) {
					$.each(arguments, function(key, val) {
						allcompresults.push(val[0]);
					});
				} else {
					allcompresults.push(arguments[0]);
				}


				createChart(allresults, allcompresults, params, meters, range, flagsdata);
			});
		} else {
			createChart(allresults, false, params, meters, range, flagsdata);
		}


        Pace.stop();
	});

}

//populate chart details with user provided chart parameters
function buildChartDetails(cobj, start, end, upscale, type) {
	var cmeter = "<ul>";

	if (type === "meter") {
		//build list of meter-parameter pairs
		$.each(cobj, function(key, val) {
			cmeter += "<li>" + convertConMeter(key) + ": " + convertParams(val) + "</li>";
		});
	} else if (type === "param") {
		$.each(cobj, function(param, meters) {
			cmeter += "<li>" + convertSources(meters) + ": " + convertParams(param) + "</li>";

		});
	}

	cmeter +="</ul>";
	$("#cmeters-val").html(cmeter);

	//build time interval
	var ftimes = convertTime(start * 1000, end * 1000);
	var ctime = "<ul><li>From: " + ftimes[0] + "</li>";
	ctime += "<li>To: " + ftimes[1] + "</li>";
	ctime += "<li>Period: " + ftimes[2] + " " + ftimes[3] + "</li></ul>";

	$("#ctime-val").html(ctime);

	//build data interval
	var cinterval = "<ul><li>" + convertInterval(upscale) + "</li></ul>";
	$("#cinterval-val").html(cinterval);
	$("#savereport-data").val(JSON.stringify(cobj));
	$("#savereport-reqtype").val(localStorage.getItem("histReqType"));

}

function getAllowedParams() {
	$.ajax({
		url: "/api/historical/params",
		type: "GET",
		dataType: "JSON",

		success: function(result) {
			allowedParams = result;
		},
		error: function(obj, sts, err) {
			showMessage("err", defaultErrMsg);
			console.log("Unable to retrieve stored parameters. Using defaults.");

			//set up a default param list for user, if retrieval fails
			allowedParams = {
				default: {
					0: sys_w,
					1: sys_va,
					2: sys_var,
					3: phase_1_volts,
					4: phase_2_volts,
					5: phase_3_volts,
					6: phase_1_amps,
					7: phase_2_amps,
					8: phase_3_amps,
			  		9: neutral_amps,
					10: v1_thd,
					11: v2_thd,
					12: v3_thd,
					13: i1_thd,
					14: i2_thd,
					15: i3_thd,
					16: freq,
					17: pf
				}
			};
		}
	});
}

function updateAllowedParams(meter, id, type) {
	var output = "";
	var outPower = new Array('<optgroup id="power" label="Power">');
	var outVoltage = new Array('<optgroup id="voltage" label="Voltage">');
	var outCurrent = new Array('<optgroup id="current" label="Current">');
	var outTHDv = new Array('<optgroup id="thd-volts" label="THD Volts">');
	var outTHDa = new Array('<optgroup id="thd-amps" label="THD Amps">');
	var outOther = new Array('<optgroup id="other" label="Other">');

	if (type == "add") {

		var allowed = allowedParams[meter];

		if (typeof(allowed) === "undefined") {
			allowed = allowedParams.default;
		}

		$.each(allowed, function(key, val) {
			if (typeof(engParams[key]) !== "undefined" && val === 1) {
				output = '<option value="' + key + '">' + engParams[key].name + '</option>';

				switch(engParams[key].type) {
					case "Power":
						outPower.push(output);
						break;
					case "Voltage":
						outVoltage.push(output);
						break;
					case "Current":
						outCurrent.push(output);
						break;
					case "THD Volts":
						outTHDv.push(output);
						break;
					case "THD Amps":
						outTHDa.push(output);
						break;
					case "Other":
						outOther.push(output);
						break;
				}

			}
		});

		$("#param-" + id).attr("data-placeholder", "Please select a parameter");
		output = outPower.sort() + outVoltage.sort() + outCurrent.sort() + outTHDv.sort() + outTHDa.sort() + outOther.sort();

	} else if (type == "remove") {
		$("#param-" + id).attr("data-placeholder", "Please select a metered circuit first");
		output += '<option disabled="disabled">Please select a metered circuit first</option>';
	}

	$("#param-" + id).html(output);
	$("#param-" + id).trigger("chosen:updated");

}

function createStartFromPeriod(phrase) {
	phrase = phrase.toLowerCase();
	var start = new XDate();
	var end = new XDate();

	switch(phrase) {
        case "prevHour":
            start = start.addHours(-1);
            break;
		case "prevday":
			start.addDays(-1);
			break;
		case "prevweek":
			start.addWeeks(-1);
			break;
		case "prevmonth":
			start.addMonths(-1);
			break;
		case "prev3months":
			start.addMonths(-3);
			break;
		case "prev6months":
			start.addMonths(-6);
			break;
		case "prevyear":
			start.addYears(-1);
			break;
	}

	start = start.toString("ddd, dd MMM yyyy HH:mm");
	end = end.toString("ddd, dd MMM yyyy HH:mm");

	var output = [start, end];
	return output;
}

function convertPtoM(meterparams) {
	var temp = {};
	$.each(meterparams, function(param, meters) {
		var meters = meters.split(",,");

		//build an array of all our meters
		$.each(meters, function(index, meter) {
			temp[meter.replace(",","_")] = "";
		});

		//associate parameters with meters
		$.each(temp, function(meter, emp) {
			var plist = "";
			$.each(meterparams, function(param, meterlist) {
				if (meterlist.indexOf(meter.replace("_",",")) >= 0) {
					plist += param + ",";
				}
			});

			plist = plist.slice(0, -1);
			temp[meter] = plist;
		});
	});

	return temp;
}

function getReportOrder() {
	var savelist = [];

	setTimeout(function() {
		$.each($("#whistory").sDashboard("getDashboardData"), function(key, val) {
			savelist.push(val.widgetId);
		});
		saveReportOrder(savelist);
	}, 500);


}

function saveReportOrder(list) {
	//get the users current saved reports and save to the api
	//rearrange the user meters to reflect user positioning
	var oldpos;
	var temp = {};

	$.each(list, function(newpos, reportid) {
		oldpos = reportid.replace("report-", "");

		//update the relevant userMeter objects
		temp[newpos] = userReports[oldpos];
	});

	//update the temp object to change the position attribute for each report
	$.each(temp, function(newpos, obj) {
		obj.position = newpos;
	});


	var data = "layout=" + encodeURIComponent(JSON.stringify(temp));
	$.ajax({
		url: "/user/config/report",
		type: "POST",
		dataType: "text",
		data: data,
		success: function(result) {
			console.log("Saved Report Layout");
		},
		error: function(obj, sts, err) {
			showMessage("err", defaultErrMsg);
		}
	});
}

function addReportLayout(params) {
//	params.meterparams = JSON.parse(params.meterparams);
	//get the users saved reports
	$.ajax({
		url: '/user/config/report',
		type: "GET",
		dataType: "JSON",

		success: function(result) {
			userReports = result;
		},
		error: function(obj, sts, err) {
			return false;
		}
	});

	//update the userReports object adding the new report to the start
	var numreports = Object.keys(userReports).length;

	//loop backwards - don't want to overwrite existing reports
	for (var i=numreports; i>0; i--) {
		var j = i - 1;
		userReports[j].position = i;
		userReports[i] = userReports[j];
	}

	//add the new report
	userReports[0] = params;
	//save the report layout
	var data = "layout=" + encodeURIComponent(JSON.stringify(userReports));
	var saved;
	$.ajax({
		url: "/user/config/report",
		type: "POST",
		dataType: "text",
		data: data,

		success: function(result) {
			console.log("Saved Report Layout");
			saved =  true;
		},
		error: function(obj, sts, err) {
			saved = false;
		}
	});

	return saved;
}

function dispatchesDocReady() {

	var requests = [];
	requests.push($.getJSON('/user/meter'));
	requests.push($.getJSON('/user/node'));

	$.when.apply($, requests).done(function(m, n) {
		userMeters = m[0];

		$.each(n[0], function(key, meter) {
			var node = meter.node_id;
			var conMeter = meter.controller_id + "," + meter.meter_id;
			var desc = meter.node_description;

			if (node) {
				if (!nodeMap[node]) {
					nodeMap[node] = {};
				}

				nodeMap[node][conMeter] = node;
				nodeMap[node].desc = desc;

				var conID = "";
				var list = "";

				$.each(userMeters, function(key, val) {
					if (node === val.limiting_node) {
						var mprn = val.mprn;

						$.each(userMeters, function(k, v) {
							if (mprn === v.limiting_site) {
								conID = v.controller_id + "," + v.meter_id;

								if (list.indexOf(conID) === -1) {
									list += conID  + ",,";
								}
							}
						});
					}
				});

				list = list.slice(0, -2);
				nodeMap[node].incomers = list;
			}
		});

		var optlist = "";

		//build dropdown list from nodeMap
		$.each(nodeMap, function(node, conMeter) {
			optlist += '<option value="' + node + ":";

			$.each(conMeter, function(pair, desc) {
				if (conMeter[pair]) {
					optlist += pair + ",,";
				}
			});

			optlist = optlist.slice(0, -2);

			optlist += '">' + conMeter.desc + '</option>';
		});

		$("#userdisp-meters").html(optlist);

		$("#userdisp-meters > option").tsort();

		$("#userdisp-meters").chosen({
			max_selected_options: 1,
			placeholder_text_multiple: "Please select a dispatchable unit. Start typing to search.",
			width: "650px"
		});

		$("#dispdate").datetimepicker({
			value: new XDate().toString("ddd, dd MMM yyyy"),
			closeOnDateSelect: true,
			timepicker: false,
			format: "D, d M Y",
		});
	});
	//form submission
	$("#userdispform").submit(function(e) {
		e.preventDefault();

		//get form data
		var usernodes = $("#userdisp-meters").val();
		var begin = new XDate($("#dispdate").val()).getTime();
		var past = true; //$("#dispshowpast").is(":checked");
		var view = $("input:radio[name=dispviewtype]:checked").val();

		//check that we have nodes to request
		if (usernodes) {
			//calculate start and end times
			//if user chooses past, show two weeks before and two weeks after user selected date
			var userdate = new XDate(begin);

			if (past) {
				var startat = userdate.clone().addWeeks(-2);
				var endat = userdate.clone().addWeeks(3); //add 3 weeks, and subtract 1 day to get the sunday of 2 weeks from a date
			} else {
				var startat = userdate.clone();
				var endat = userdate.clone().addWeeks(5);
			}

			//get the week that the start and end dates fall in and get the Monday of the start date and the Sunday of the end date
			var startweek = startat.getWeek();
			var endweek = endat.getWeek();

			startat = startat.setWeek(startweek).setHours(0).setMinutes(0); //set to midnight
			endat = endat.setWeek(endweek).addDays(-1).setHours(23).setMinutes(59); //go back one day and set the time to 23:59

			//convert to unix time for url
			var urlstart = startat.getTime() / 1000;
			var urlend = endat.getTime() / 1000;

			//array to hold resulting dispatches
			var alldispatches = getDispatches(usernodes, urlstart, urlend);

			//check if the user wants a calendar or a list
			if (view === "cal") {
				$("#dispwrap").html(""); //drop any existing dispatches
				$("#dispgrid").html(""); //drop any existing dispatches
				//build the calendar
				var numdays = startat.diffDays(endat) + 1; //how many days are we showing? *Should* be 35, but just in case, calculate it.

				//date used to iterate through calenadar
				var startcal = startat.clone();

				var ghtml = "<table>";
				ghtml += '<thead><tr><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th><th>Sun</th></tr></thead>';
				ghtml += '<tfoot><tr><th>Mon</th><th>Tue</th><th>Wed</th><th>Thu</th><th>Fri</th><th>Sat</th><th>Sun</th></tr></tfoot>';
				ghtml += '<tbody><tr>';

				for (var i=1; i<=numdays; i++) {
					ghtml += '<td';

					//is this the day that user selected?
					if (startcal.toString("yyyy-MM-dd") == userdate.toString("yyyy-MM-dd")) {
						ghtml += ' class="dispgridtoday"';
					}

					ghtml += ' id="disp-' + startcal.toString("yyyyMMdd") + '"><div class="dispday">';

					//shows the date in the table cell
					var d = startcal.getDate();
					ghtml += startcal.toString("dd");

					//show the month number in the first cell, and when the month changes
					if (i == 1 || d == 1) {
						ghtml += startcal.toString("/ MM");
					}

					ghtml += '</div></td>';

					//start a new row at the end of each week
					if (i % 7 == 0) {
						ghtml += '</tr><tr>';
					}

					//increment the current date
					startcal.addDays(1);
				}

				//remove the extra </td> when we reach the end of the calendar
				ghtml = ghtml.slice(0, -4);
				ghtml += '</tbody></table>';

				//add the calendar to the page
				$("#dispgrid").html(ghtml);

				//add the dispatches to the calendar
				for (var nid in alldispatches) {
					var dispatches = alldispatches[nid];

					$.each(dispatches, function(index, dispatch) {
						//parse dispatch info
						var timeon = new XDate(dispatch.fields.time_on);
						var timeoff = new XDate(dispatch.fields.time_off);
						var timecanx;
						dispatch.fields.cancelled_time !== null ? timecanx = new XDate(dispatch.fields.cancelled_time) : timecanx = false;
						var timecreated = new XDate(dispatch.fields.created);
						var timemod = new XDate(dispatch.fields.modified);
						var timeack;
						dispatch.fields.controller_ack !== null ? timeack = new XDate(dispatch.fields.controller_ack) : timeack = false;
						var notes = dispatch.fields.notes;
						var conid = dispatch.fields.controller;
						var desc = nodeMap[nid].desc;
						var dispid = dispatch.pk;

						//id of containing table cell
						var cellid = timeon.clone().toString("yyyyMMdd");

						//get starting hour and minutes
						//dispatches displayed in 30 minute blocks
						var starth = timeon.clone().getHours(); //starting hour & min
						var startm = timeon.clone().getMinutes();
						startm >= 30 ? startm = 0.5 : startm = 0;

						//check if dispatch has been cancelled
						if (timecanx) {
							var canxh = timecanx.clone().getHours();
							var canxm = timecanx.clone().getMinutes();
							canxm >= 30 ? canxm = 0.5 : canxm = 0;
						} else {
							var canxh = false;
							var canxm = false;
						}

						//if the dispatch was cancelled before it started, then we don't need to show it
						if (!dispCanxBefore(timeon, timecanx)) {
							//build the tooltip
							var tt_title = '<div class="tt-head">' + desc + '</div>';
							var tt_text = '<div class="tt-maindate">' + dispMonthYear(timeon) + '</div>';
							tt_text += '<div class="tt-type">' + notes + '</div>';
							tt_text += '<div class="tt-start">Starting: ' + convertDispDate(timeon) + '</div>';
							tt_text += '<div class="tt-end">Finishing: ' + convertDispDate(timeoff) + '</div>';
							tt_text += '<div class="tt-canx">Cancelled: ' + convertDispDate(timecanx) + '</div>';
							tt_text += '<div class="tt-created">Created: ' + convertDispDate(timecreated) + '</div>';
							tt_text += '<div class="tt-mod">Modified: ' + convertDispDate(timemod) + '</div>';
							tt_text += '<div class="tt-ack">Acknowledged: ' + convertDispDate(timeack) + '</div>';

							//only show analyse button if the dispatch has started
							var thetime = new XDate().getTime();
							if (timeon < thetime) {
								//build list of sources
								var nodedata = nodeMap[nid];
								var sources = [];
								var sourcelist = "";

								$.each(nodedata, function(key, val) {
									if (typeof(val) === "number") {
										if (sources.indexOf(key) === -1) {
											sources.push(key);
										}
									}
								});

								$.each(sources, function(key, val) {
									sourcelist += val + ",,";
								});

								sourcelist += nodeMap[nid].incomers;

								tt_text += '<button class="tt-button" onclick="analyseDispatch(\'' + timeon + '\',\'' + timeoff + '\',\'' + sourcelist + '\');">Analyse</button>';
							}

							//calculate number of half hour periods between the start and end of the dispatch
							var halves = dispHalves(timeon, timeoff);

							//calculate position of span
							var mtop = ((starth + startm) / 24) * 120; //cells height = 120px
							var h = (halves * 2.5) + mtop > 120 ? 120 - mtop : halves * 2.5; //120px / 24 hours = 5px per hour

							//does dispatch straddle midnight?
							var startdate = timeon.clone().getDate();
							var enddate = timeoff.clone().getDate();

							//dispatch straddles midnight
							if (enddate !== startdate) {
								var dspan = "";

								var startcell = timeon.clone().toString("yyyyMMdd");
								var scellexists = $("#disp-" + startcell).length;

								var endcell = timeoff.clone().toString("yyyyMMdd");
								var ecellexists = $("#disp-" + endcell).length;

								//get half hours between start and midnight
								var p1end = timeon.clone().setHours(23).setMinutes(59);
								var p1halves = dispHalves(timeon, p1end);

								var p2start = timeoff.clone().setHours(0).setMinutes(0);
								var p2halves = dispHalves(p2start, timeoff);

								if (scellexists > 0) {
									var p1h = (p1halves * 2.5) + mtop > 120 ? 120 - mtop: p1halves * 2.5;

									//start cell exists
									var dspan = '<span class="dispgridline id-' + dispid;

									switch(notes.toLowerCase()) {
										case "client dispatch run":
											dspan += ' cdr';
											break;
										case "eirgrid test run":
										case "eirgrid dispatch run":
											dspan += ' edr';
											break;
										case "elex test run":
										case "elex test simulate":
											dspan += ' elex';
											break;
									}

									dspan += '" style="top: ' + mtop + 'px; height: ' + p1h + 'px;">';
									dspan += '<span class="dispgridmarker';

									if (canxh) {
										dspan += ' disphourcanx';
									}

									dspan += '">&nbsp;</span></span>';

									$("#disp-" + cellid).append(dspan);
									$(".id-" + dispid).qtip({
										content: {
											title: tt_title,
											text: tt_text
										},
										style: {
											def: false,
											classes: "disp-tt"
										},
										position: {
											my: "top center",
											at: "bottom center",
											target: $(".id-" + dispid),
											adjust: {
												method: "flip"
											},
											viewport: $(window)
										},
										hide: {
											fixed: true,
											delay: 250
										}
									});

								}

								if (ecellexists > 0) {
									var p2h = (p2halves * 2.5) + mtop > 120 ? 120 - mtop: p2halves * 2.5;

									//start cell exists
									var dspan = '<span class="dispgridline id-' + dispid;

									switch(notes.toLowerCase()) {
										case "client dispatch run":
											dspan += ' cdr';
											break;
										case "eirgrid test run":
										case "eirgrid dispatch run":
											dspan += ' edr';
											break;
										case "elex test run":
										case "elex test simulate":
											dspan += ' elex';
											break;
									}

									dspan += '" style="top: 0px; height: ' + p2h + 'px;">';
									dspan += '<span class="dispgridmarker';
									if (canxh) {
										dspan += ' disphourcanx';
									}

									dspan += '">&nbsp;</span>&nbsp;</span>';

									var nextcellid = parseInt(cellid) + 1;
									$("#disp-" + nextcellid).append(dspan);
									$(".id-" + dispid).qtip({
										content: {
											title: tt_title,
											text: tt_text
										},
										style: {
											def: false,
											classes: "disp-tt"
										},
										position: {
											my: "top center",
											at: "bottom center",
											target: $(".id-" + dispid),
											adjust: {
												method: "flip"
											},
											viewport: $(window)
										},
										hide: {
											fixed: true,
											delay: 250
										}
									});
								}

							} else {
								var dspan = '<span class="dispgridline id-' + dispid;

								switch(notes.toLowerCase()) {
									case "client dispatch run":
										dspan += ' cdr';
										break;
									case "eirgrid test run":
									case "eirgrid dispatch run":
										dspan += ' edr';
										break;
									case "elex test run":
									case "elex test simulate":
										dspan += ' elex';
										break;
								}

								dspan += '" style="top: ' + mtop + 'px; height: ' + h + 'px;">';
								dspan += '<span class="dispgridmarker';
								if (canxh) {
									dspan += ' disphourcanx';
								}

								dspan += '">&nbsp;</span>&nbsp;</span>';

								$("#disp-" + cellid).append(dspan);
								$(".id-" + dispid).qtip({
								content: {
									title: tt_title,
									text: tt_text
								},
								style: {
									def: false,
									classes: "disp-tt"
								},
								position: {
									my: "top center",
									at: "bottom center",
									target: $(".id-" + dispid),
									adjust: {
										method: "flip"
									},
									viewport: $(window)
								},
								hide: {
									fixed: true,
									delay: 250
								}
							});
							}
						}

					});
				}

				$("html body").animate({
					scrollTop: $("#dispgrid").offset().top
				}, {duration: 500, queue: false});

				$("#displegend").fadeIn({duration: 500, queue: false});

			} else if (view === "list") {
				$("#dispwrap").html(""); //drop any existing dispatches
				$("#dispgrid").html(""); //drop any existing dispatches
				var dhtml = "";

				for (var nid in alldispatches) {
					var dispatches = alldispatches[nid];

					$.each(dispatches, function(key, fulldispatch) {

						//does dispatch straddle midnight?
						var startdate = new XDate(fulldispatch.fields.time_on).getDate();
						var enddate = new XDate(fulldispatch.fields.time_off).getDate();
						var partdispatch = [];

						//dispatch straddles midnight
						if (enddate !== startdate) {
							//create a "double" dispatch
							var part1 = $.extend(true, {}, fulldispatch);
							var part2 = $.extend(true, {}, fulldispatch);

							//change part1 end time to a minute to midnight
							var p1end = new XDate(part1.fields.time_on).setHours(23).setMinutes(59).toISOString();
							part1.fields.time_off = p1end;
							partdispatch.push(part1);

							var p2start = new XDate(part2.fields.time_off).setHours(0).setMinutes(0).toISOString();
							part2.fields.time_on = p2start;
							partdispatch.push(part2);

						} else {
							partdispatch.push(fulldispatch);
						}

						$.each(partdispatch, function(index, dispatch) {
							var timeon = new XDate(dispatch.fields.time_on);
							var timeoff = new XDate(dispatch.fields.time_off);
							var timecanx;
							dispatch.fields.cancelled_time !== null ? timecanx = new XDate(dispatch.fields.cancelled_time) : timecanx = false;
							var timecreated = new XDate(dispatch.fields.created);
							var timemod;
							dispatch.fields.modified !== null ? timemod = new XDate(dispatch.fields.modified) : timemod = false;
							var timeack;
							dispatch.fields.controller_ack !== null ? timeack = new XDate(dispatch.fields.controller_ack) : timeack = false;
							var notes = dispatch.fields.notes;
							var conid = dispatch.fields.controller;
							var desc = nodeMap[nid].desc;

							//get starting hour and minutes
							//dispatches displayed in 30 minute blocks
							var starth = timeon.clone().getHours(); //starting hour & min
							var startm = timeon.clone().getMinutes();
							startm >= 30 ? startm = 1 : startm = 0;

							//check if dispatch has been cancelled
							if (timecanx) {
								var canxh = timecanx.clone().getHours();
								var canxm = timecanx.clone().getMinutes();
								canxm >= 30 ? canxm = 1 : canxm = 0;
							} else {
								var canxh = false;
								var canxm = false;
							}

							//only show dispatches that have not
							if (!dispCanxBefore(timeon, timecanx)) {

								var halves = dispHalves(timeon, timeoff);
								var hasStarted = false;

								dhtml += '<div class="dispcontain">';
								dhtml += '<div class="disphead">' + desc + '</div>';
								dhtml += '<div class="disptitle"><div class="dispmaindate">' + dispMonthYear(timeon) + '</div>';
								dhtml += '<div class="disptype">' + notes + '</div></div>';
								dhtml += '<div class="displeft"><div class="dispstart">Starting: ' + convertDispDate(timeon) + '</div>';
								dhtml += '<div class="dispend">Finishing: ' + convertDispDate(timeoff) + '</div>';
								dhtml += '<div class="dispcanx">Cancelled: ' + convertDispDate(timecanx) + '</div></div>';
								dhtml += '<div class="dispright"><div class="dispcreated">Created: ' + convertDispDate(timecreated) + '</div>';
								dhtml += '<div class="dispmod">Modified: ' + convertDispDate(timemod) + '</div>';
								dhtml += '<div class="dispack">Acknowledged: ' + convertDispDate(timeack) + '</div></div>';

								//only show analyse button if the dispatch has started
								var thetime = new XDate().getTime();
								if (timeon < thetime) {
									//build list of sources
									var nodedata = nodeMap[nid];
									var sources = [];
									var sourcelist = "";

									$.each(nodedata, function(key, val) {
										if (typeof(val) === "number") {
											if (sources.indexOf(key) === -1) {
												sources.push(key);
											}
										}
									});

									$.each(sources, function(key, val) {
										sourcelist += val + ",,";
									});

									sourcelist += nodeMap[nid].incomers;

									dhtml += '<button class="disp-button" onclick="analyseDispatch(\'' + timeon + '\',\'' + timeoff + '\',\'' + sourcelist + '\');">Analyse</button>';
								}

								dhtml += '<div class="disptimeline">';

								for (var i=0; i<24; i++) {
									dhtml += '<span class="disphour">';
									//check if we're at the starting hour
									if (i < starth) {
										for (var j=0; j<2; j++) {
											dhtml += '<span class="disphalfhour">&nbsp;</span>';
										}
									} else if (i === starth) {
										//create the half hour blocks
										for (var j=0; j<2; j++) {
											dhtml += '<span class="disphalfhour';

											//check if we're at the starting half hour
											if (j === startm || hasStarted) {
												dhtml += ' disphoursel';
												switch(notes.toLowerCase()) {
													case "client dispatch run":
														dhtml += ' cdr';
														break;
													case "eirgrid test run":
													case "eirgrid dispatch run":
														dhtml += ' edr';
														break;
													case "elex test run":
													case "elex test simulate":
														dhtml += ' elex';
														break;
												}

												dhtml += '">&nbsp;</span>';

												hasStarted = true;
												halves--;
											} else if(!hasStarted) {
												dhtml += '">&nbsp;</span>';
											}
										}
									} else if (i > starth) {
										for (var j=0; j<2; j++) {
											if (halves > 0) {
												dhtml += '<span class="disphalfhour disphoursel';

												switch(notes.toLowerCase()) {
													case "client dispatch run":
														dhtml += ' cdr';
														break;
													case "eirgrid test run":
													case "eirgrid dispatch run":
														dhtml += ' edr';
														break;
													case "elex test run":
													case "elex test simulate":
														dhtml += ' elex';
														break;
												}

												dhtml += '">&nbsp;</span>';
												halves--;
											} else {
												dhtml += '<span class="disphalfhour">&nbsp;</span>';
											}
										}
									} else if (i > starth && halves <= 0) {
										for (var j=0; j<2; j++) {
											dhtml += '<span class="disphalfhour">&nbsp;</span>';
										}
									}

									dhtml += '</span>';
								}

								dhtml += '</div><div class="disphourslist">';

								for (var i=0; i<24; i++) {
									dhtml += '<span class="disphourtext">' + i + '</span>';
								}

								dhtml += '</div></div>';
							}
						});
					});

					$("#dispwrap").html(dhtml);

					$("html body").animate({
						scrollTop: $("#dispgrid").offset().top
					}, {duration: 500, queue: false});

					$("#displegend").fadeIn({duration: 500, queue: false});
				}
			}
		} else {
			showMessage("err", "Please select a dispatchable unit to view.");
		}
	});

}

function analyseDispatch(timeon, timeoff, sources) {
	var start = parseInt(timeon) - 1800000; //half an hour
	var end = parseInt(timeoff) + 1800000;

	//build JSON object to hold url data
	var obj = {
		type: "dispatch",
		start: start,
		end: end,
		sources: sources
	};

	var reportData = encodeURI(JSON.stringify(obj));
	document.location.href = "/portal/history/analysis?" + reportData;
}

function getDispatches(usernodes, urlstart, urlend) {
	var alldispatches = [];

	//request each node seperately
	$.each(usernodes, function(key, node) {
		var nid = node.split(":")[0];

		//build the url
		url = "/api/node/dispatch?node=" + nid + "&start=" + urlstart + "&end=" + urlend;

		$.ajax({
			url: url,
			type: "GET",
			dataType: "JSON",
			async: false,
			success: function(result) {

				if (result.length > 0) {
					alldispatches[nid] = result;
				}
			},
			error: function() {
				alldispatches[nid] = null;
			}
		});
	});

	return alldispatches;
}

function dispHourMin(dispTime) {
	if (dispTime) {
		var ms = XDate.parse(dispTime);
		var d = new XDate(ms);
		var arr = [];

		arr.push(d.getHours());
		arr.push(d.getMinutes());
		return arr;
	} else {
		return false;
	}
}

function dispHalves(start, end) {
	if (start && end) {
		var sms = start.getTime();
		var ems = end.getTime();

		var diff = ems - sms;
		var halves = Math.ceil(diff / 1800000); //30 minutes
		return halves;
	} else {
		return false;
	}
}


function dispMonthYear(dispDate) {
	if (dispDate) {
		var cd = dispDate.toString("dd MMM yyyy");
		return cd;
	} else {
		return "&#8212";
	}
}

function convertDispDate(dispDate) {
	if (dispDate) {
		var cd = dispDate.toString("ddd, dd MMM yyyy HH:mm:ss");
		return cd;
	} else {
		return "&#8212";
	}
}

function histogramDocReady() {
	//get the list of sites the user has access to
	$.when(
		$.getJSON("/user/meter")
	).done(function() {

		userMeters = arguments[0];
		var optlist = "";

		$.each(userMeters, function(index, meter) {
			optlist += '<option value="' + meter.controller_id + ',' + meter.meter_id + '">' + meter.site_description + ': ' + meter.metered_circuit + '</option>';
		});

		$("#histmeter").html(optlist);

		$("#histmeter > option").tsort();

		$("#histmeter").chosen({
			max_selected_options: 1,
			placeholder_text_multiple: "Please select a metered circuit. Start typing to search.",
			width: "500px"
		});

	}).fail(function() {
		showMessage("err", defaultErrMsg);
	});

	$("#histparams").chosen({
		max_selected_options: 1,
		placeholder_text_multiple: "Please select a parameter. Start typing to search.",
		width: "500px"
	});

	$("#histresolution").chosen({
		max_selected_options: 1,
		placeholder_text_multiple: "Please select a resolution. Start typing to search.",
		width: "500px"
	});

	$("#histinc").on("click", function(e) {
		e.preventDefault();
		var curval = $("#histtolerance").val();
		curval = Math.round((parseFloat(curval) + 0.1) * 10) / 10;
		$("#histtolerance").val(curval);
	});

	$("#histdec").on("click", function(e) {
		e.preventDefault();
		var curval = $("#histtolerance").val();
		curval = Math.round((parseFloat(curval) - 0.1) * 10) / 10;
		$("#histtolerance").val(curval);
	});

	var fromtime = new XDate();
	fromtime = fromtime.addDays(-1);
	fromtime = fromtime.toString("ddd, dd MMM yyyy");

	var totime = new XDate();
	totime = totime.toString("ddd, dd MMM yyyy");

	var rightnow = new XDate();
	rightnow = rightnow.toString("ddd, dd MMM yyyy");

	var thisyear = new XDate();
	thisyear = thisyear.toString("yyyy");

	$("#histfrom").datetimepicker({
		value: fromtime,
		format: "D, d M Y",
		maxDate: rightnow,
		yearStart: 2013,
		yearEnd: thisyear,
		timepicker: false,
		closeOnDateSelect: true
	});

	$("#histto").datetimepicker({
		value: totime,
		format: "D, d M Y",
		maxDate: rightnow,
		yearStart: 2013,
		yearEnd: thisyear,
		timepicker: false,
		closeOnDateSelect: true,
		onSelectDate: function(dp, $input) {
			//check if we have a resolution
			var res = $("#histresolution").val();

			if (res) {
				res = res[0];
				var min = new XDate($input.val());
				var max = new XDate($input.val());
				var limit = -5;

				//set the min and date of the from datepicker
				switch(res) {
					case "day":
						min.addDays(limit);
						max.addDays(-2);
						break;
					case "week":
						min.addWeeks(limit);
						max.addWeeks(-2);
						break;
					case "month":
						min.addMonths(limit);
						max.addMonths(-2);
						break;
				}

				var val = min.toString("ddd, dd MMM yyyy");
				min = min.toString("yyyy/MM/dd");
				max = max.toString("yyyy/MM/dd");

				$("#histfrom").datetimepicker({
					format: "D, d M Y",
					value: val,
					minDate: min,
					maxDate: max
				});
			}
		}
	});

	$("#histresolution").on("change", function(e, p) {
		var limit = -5; //set a limit of max 5 requests

		//check if user has selected or deselected
		if (p.selected) {
			var res = p.selected;
			//get the to date
			var to = new XDate($("#histto").val());
			var min = new XDate(to);
			var max = new XDate(to);

			//set the min and date of the from datepicker
			switch(res) {
				case "day":
					min.addDays(limit);
					max.addDays(-2);
					break;
				case "week":
					min.addWeeks(limit);
					max.addWeeks(-2);
					break;
				case "month":
					min.addMonths(limit);
					max.addMonths(-2);
					break;
			}

			var val = min.toString("ddd, dd MMM yyyy");
			min = min.toString("yyyy/MM/dd");
			max = max.toString("yyyy/MM/dd");

			$("#histfrom").datetimepicker({
				format: "D, d M Y",
				value: val,
				minDate: min,
				maxDate: max
			});

		}

	});

	$("#optionstab").on("click", function() {

		if ($("#optionstabtitle").html() == "Hide Histogram Options") {

			$("#histoptswrap").animate({top: ($("#histoptswrap").height()) * -1}, {duration: 500, queue: false});
			$("#optionstabtitle").html("Show Histogram Options");
			$("#optionstabup").fadeOut({duration: 50, queue: false});
			$("#optionstabdown").fadeIn({duration: 50, queue: false});

		} else {

			$("#histoptswrap").animate({top: "20px"}, {duration: 500, queue: false});
			$("#optionstabtitle").html("Hide Histogram Options");
			$("#optionstabdown").fadeOut({duration: 50, queue: false});
			$("#optionstabup").fadeIn({duration: 50, queue: false});

		}
	});

	$("#histopts").submit(function(e) {
		e.preventDefault();

		//check that user has supplied all details
		var meterinfo = $("#histmeter").val();
		var param = $("#histparams").val();
		var res = $("#histresolution").val();
		var tolerance = $("#histtolerance").val();
		var from = $("#histfrom").val();
		var to = $("#histto").val();
		var errmsg = "We were unable to build your histogram. Please rectify the following items and try again.<ul>";
		var haserr = false;

		if (meterinfo) {
			meterinfo = meterinfo[0];
		} else {
			errmsg += '<li>Please select a metered circuit.</li>';
			haserr = true;
		}

		if (param) {
			param = param[0];
		} else {
			errmsg += '<li>Please select a parameter.</li>';
			haserr = true;
		}

		if (res) {
			res = res[0];
		} else {
			errmsg += '<li>Please select a resolution.</li>';
			haserr = true;
		}

		if (tolerance) {
			tolerance = parseFloat(tolerance);
		} else {
			errmsg += '<li>Please select a tolerance.</li>';
			haserr = true;
		}

		if (from) {
			from = new XDate(from);
			from = from.getTime() / 1000;
		} else {
			errmsg += '<li>Please select a from date.</li>';
			haserr = true;
		}

		if (to) {
			to = new XDate(to);
			to = to.getTime() / 1000;
		} else {
			errmsg += '<li>Please select a to date.</li>';
			haserr = true;
		}

		if (from > to) {
			errmsg += '<li>Please check your dates: "From" is after "To".</li>';
			haserr = true;
		}

		errmsg += "</ul>";

		if (!haserr) {
			Pace.start();
			var diff;
			var url = "";
			var requests = [];

			//get controller and meter
			var siteinfo = meterinfo.split(",");
			var conid = siteinfo[0];
			var meterid = siteinfo[1];

			//start building the url
			var baseurl = "/api/historical/histogram?sources=" + meterinfo + "&parameter=" + param;
			baseurl += "&resolution=" + res + "&tolerance=" + tolerance + "&time_start=" + from + "&time_end=" + to;
			requests.push($.getJSON(baseurl));

			$.when.apply($, requests).done(function() {

				//create the dataset object to hold data
				var opts = {
					fieldId: "id",
					convert: {
						"x": "Number",
						"y": "Number",
						"z": "Number"
					}
				};

				var dataset = new vis.DataSet(opts);

				var id = 0;
				var xmin = 10000000;
				var xmax = -10000000;
				var ymin = 10000000;
				var ymax = -10000000;
				var zmin = 10000000;
				var zmax = -10000000;
				var label = "";
				var tooltips = {};
				var tlabel;

				if (Object.keys(arguments[0]).length > 0 ) {
					var rawdata = arguments[0];
				} else {
                    Pace.stop();
					var errmsg = "There was a problem retrieving the data you requested.";
					errmsg += "<ul><li>No data exists for the parameter/ dates chosen.</li></ul>";
					errmsg += "Please choose a different parameter, different dates, or a higher resolution.";
					errmsg += " (The difference between the start and end date should be at least twice the resolution length.)";
					showMessage("err", errmsg);
					return false;
				}

				$.each(rawdata, function(index, data) {
					var date = new XDate(data.date_time);

					//set the y label based on the resolution
					switch(res) {
						case "day":
							tlabel = "Date: " + date.toString("ddd, dd MMM yyyy");
							date = date.getDate();
							label = "Dates";
							break;
						case "week":
							tlabel = "Week Starting: " + date.toString("ddd, dd MMM yyyy");
							date = date.getWeek();
							label = "Weeks";
							break;
						case "month":
							tlabel = "Month Starting: " + date.toString("ddd, dd MMM yyyy");
							date = date.getMonth() + 1;
							label = "Months";
							break;
					}

					dataset.add({
						id: id++,
						x: data.value_bin,
						y: date,
						z: data.count
					});

					var temp = {};
					temp[data.value_bin] = tlabel;
					tooltips[data.count] = temp;

					if (data.value_bin < xmin) xmin = data.value_bin;
					if (data.value_bin > xmax) xmax = data.value_bin;

					if (date < ymin) ymin = date;
					if (date > ymax) ymax = date;

					if (data.count < zmin) zmin = data.count;
					if (data.count > zmax) zmax = data.count;

				});

				var options = {
					backgroundColor: {
						strokeWidth: 0
					},
					width:  '100%',
					height: '100%',
					style: 'surface',
					showPerspective: false,
					showGrid: true,
					showShadow: false,
					verticalRatio: 0.5,
					keepAspectRatio: false,
					xMin: xmin,
					xMax: xmax,
					yMin: ymin,
					yMax: ymax,
					zMin: zmin,
					zMax: zmax,
					xLabel: "Value Bins",
					yLabel: label,
					zLabel: "Count",
					yStep: 1,
					tooltip: function(p) {
						return tooltips[p.z][p.x] + "<br>Value Bin: " + p.x + "<br>Count: " + p.z;
					},
					yCenter: "40%"
				};

				var container = document.getElementById("histcontain");
				var graph3d = new vis.Graph3d(container, dataset, options);

				var desc = convertConMeter(conid + "-" + meterid);

				$("#surface-title").html("<h1>Histogram for " + desc + "</h1>");
				$("#hd-param").html("Parameter: " + convertParams(param));
				$("#hd-from").html("From: " + $("#histfrom").val());
				$("#hd-to").html("To: " + $("#histto").val());
				$("#hd-resolution").html("Resolution: " + res);
				$("#optionstab").click();
				$("#surfacewrap").animate({opacity: 1}, {duration: 500, queue: false});
                Pace.stop();

			}).fail(function() {
                Pace.stop();
			});
		} else {
			showMessage("err", errmsg);
		}
	});
}

function weatherDocReady() {

	//set the page to refresh every 30 minutes
	setInterval(function() {location.reload(true);}, 1800000);

	//catch div resizes
	$(document).on("divresize", function() {

		var allcharts = Highcharts.charts;
		$.each(allcharts, function(key, val) {
			setTimeout(function() {
				Highcharts.charts[key].reflow();
				Highcharts.charts[key].redraw();

				var width = Highcharts.charts[key].plotWidth;
				width = width * 1.1;

				var el = $('div[data-highcharts-chart*="' + key + '"]');
				var id = $(el).attr("id");
				id = id.split("-");
				var pk = id[1];

				//set width of small chart wrapper
				$(".small-wrap-" + pk).css("width", width);
			}, 1000);
		});
	});

	var requests = {};
	var weather = [];
	var lat, lon;

	//get the users sites if not already populated
	if (Object.keys(userSites).length === 0) {
		$.ajax({
			url: "/user/sites",
			type: "GET",
			dataType: "JSON",

			success: function(result) {
				userSites = arguments[0];
			},
			error: function(o, s, e) {
				showMessage("err", defaultErrMsg);
			}
		});
	}

	//get the weather data for each site
	$.each(userSites, function(key, val) {
		var pk = val.pk;
		var url = "/api/site/weather?site=" + pk;

		requests[val.pk] = {
			url: url,
			desc: val.fields.description
		};
	});

	//get the weather data for each site
	$.each(requests, function(pk, data) {

		$.when(
			$.getJSON(data.url)
		).done(function() {

			//create divs to hold weather data for each site
			var cid = "wchart-" + pk;
			var wel = '<div class="wwrap">';
			wel += '<div class="weather-title">' + data.desc + '</div>';
			wel += '<div id="' + cid + '" class="wchart"></div>';
			wel += '<div class="small-wrap small-wrap-' + pk + '">';
			for (var i=0; i<10; i++) {
				wel += '<div class="small-chart small-' + i;

				if (i % 2 === 1) {
					wel += ' sc-alt';
				}

				wel += '"></div>';
			}

			wel += '<div class="small-chart small-titles">';
			wel += '<div class="sc-icon">&nbsp;</div>';
			wel += '<div class="sc-desc">&nbsp;</div>';
			wel += '<div class="sc-text">Pressure (hPa)</div>';
			wel += '<div class="sc-text">Humidity (%)</div>';
			wel += '<div class="sc-text">Wind Speed (m/s)</div></div>';
			wel += '</div>';
			wel += '<div id="wcurr-' + pk + '"></div>';
			wel += '</div>';

			$("#weathercharts").append(wel);

			//get the different data objects
			var curw = arguments[0].current;
			var forew = arguments[0].forecast;
			var wdata = arguments[0].hourly.list;

			//build chart to hold data
			var tempseries = [];
			var rainseries = [];
			var windseries = [];
			var smalldata = [];
			var temprange = [];
			var timecat = [];

			for (var i=0; i<10; i++) {
				var dt = wdata[i].dt * 1000;
				timecat.push([dt]);
				tempseries.push([dt, wdata[i].main.temp - 273.15]);
				temprange.push([dt, wdata[i].main.temp_min - 273.15, wdata[i].main.temp_max - 273.15]);

				if (typeof(wdata[i].rain) !== "undefined") {
					rainseries.push([dt, wdata[i].rain["3h"]]);
				} else {
					rainseries.push([dt, 0]);
				}

				windseries.push([dt, wdata[i].wind.speed]);

				//build the data required for the small weather charts
				smalldata[i] = {
					humidity: wdata[i].main.humidity,
					pressure: wdata[i].main.pressure,
					icon: wdata[i].weather[0].icon,
					desc: wdata[i].weather[0].main,
					wind: wdata[i].wind.speed,
					dt: wdata[i].dt
				};
			}

			tempseries.sort();
			temprange.sort();
			rainseries.sort();
			windseries.sort();
			timecat.sort();

			var opts = {
				chart: {
					animation: false,
					backgroundColor: "#FFFFFF",
					borderWidth: 0,
					plotShadow: false,
					renderTo: cid,
					shadow: false,
					showAxes: true,
					type: "spline",
					zoomType: null
				},
				credits: {
					enabled: false,
				},
				exporting: {
					enabled: false
				},
				global: {
					useUTC: true
				},
				lang: {
					noData: "No Data Exists for the Date Selected. Please Select a Valid Date."
				},
				legend: {
					align: "right",
					backgroundColor: '#FFFFFF',
					borderColor: "#909090",
					borderRadius: 5,
					borderWidth: 1,
					enabled: true,
					floating: true,
					itemDistance: 30,
					layout: 'vertical',
					shadow: true,
					verticalAlign: 'top',
					y: 0
				},
				noData: {
					style: {
						fontWeight: 'bold',
						fontSize: '15px',
						color: '#303030'
					}
				},
				navigator: {
					enabled: false
				},
				plotOptions: {
					column: {
						color: "#0088FF",
						minPointLength: 5,
						pointWidth: 30,
						shadow: true
					},
					spline: {
						marker: {
							enabled: true,
							fillColor: "#FFFFFF",
							lineWidth: 2,
							lineColor: null,
							radius: 4,
							symbol: "circle"
						},
						shadow: true,
						tooltip: {
							followPointer: true,
						}
					}
				},
				rangeSelector : {
					enabled: false
				},
				series: [{
					data: tempseries,
					color: "#FF8800",
					name: "Temperature",
					tooltip: {
						valueSuffix: ' °C',
						xDateFormat: '%A, %d %b %Y, %H:%M'
					},
					type: "spline",
					yAxis: "temp",
					zIndex: 40
				}, {
					data: temprange,
					color: "#ffd992",
					name: "Temperature Range",
					tooltip: {
						valueSuffix: ' °C',
					},
					type: "areasplinerange",
					linkedTo: ":previous",
					yAxis: "temp",
					zIndex: 10
				}, {
					data: rainseries,
					color: "#0088FF",
					name: "Precipitation",
					tooltip: {
						valueSuffix: ' mm'
					},
					type: "column",
					yAxis: "rain",
					zIndex: 20
				}, {
					data: windseries,
					color: "#880088",
					name: "Wind Speed",
					tooltip: {
						valueSuffix: ' metres per second'
					},
					type: "spline",
					yAxis: "wind",
					zIndex: 30
				}],
				scrollbar: {
					enabled: false
				},
				title: {
					text: "Weather Forecast (3 Hour Intervals)"
				},
				tooltip: {
					crosshairs: [{
						color: "#22CC22",
						width: 2,
						zIndex: 22
					}],
					dateTimeLabelFormats: {
						day: '%d %b %H:%M',
						hour: '%d %b %H:%M'
					},
					shared: true,
					useHTML: true,
					valueDecimals: 2
				},
				xAxis: {
					endOnTick: false,
					categories: timecat,
					labels: {
						align: "center",
						formatter: function() {
							var xlabel = Highcharts.dateFormat('%a', new Date(this.value)) + '<br>';
							xlabel += Highcharts.dateFormat('%d %b', new Date(this.value)) + '<br>';
							xlabel += Highcharts.dateFormat('%H:%M', new Date(this.value));
							return xlabel;
						}
					},
					lineColor: "#aaa",
					lineWidth: 3,
					minRange:  60 * 1000,
					tickColor: "#aaa",
					tickInterval: 3 * 3600 * 1000,
					tickPixelInterval: null,
					type: "datetime",
					startOnTick: false
				},
				yAxis: [{
					id: "temp",
					title: {
						text: NaN
					},
					labels: {
						align: "left",
						format: "{value} °C",
						overflow: "justify",
						style: {
							color: "#FF8800"
						}
					},
					minorGridLineWidth: 0,
					minorTickWidth: 0,
					opposite: true,
					lineWidth: 0,
					lineColor: "transparent",
					tickWidth: 0
				}, {
					id: "rain",
					title: {
						text: NaN
					},
					labels: {
						format: "{value} mm",
						style: {
							color: "#0088FF"
						}
					},
					minorGridLineWidth: 0,
					minorTickWidth: 0,
					gridLineWidth: 0,
					opposite: true,
					lineWidth: 0,
					lineColor: "transparent",
					tickWidth: 0
				}, {
					id: "wind",
					title: {
						text: NaN
					},
					labels: {
						format: "{value} m/s",
						style: {
							color: "#880088"
						}
					},
					minorGridLineWidth: 0,
					minorTickWidth: 0,
					gridLineWidth: 0,
					opposite: true,
					lineWidth: 0,
					lineColor: "transparent",
					tickWidth: 0
				}]
			};

			var chart = new Highcharts.StockChart(opts);
			var width = chart.plotWidth;
//			width = width * 1.1;

			//set width of small chart wrapper
			$(".small-wrap-" + pk).css("width", width);

			//build the small weather "charts"
			for (var i=0; i<10; i++) {
				//build the contents of the small chart
				var shtml = '<div class="sc-icon">' + wcodes[smalldata[i].icon] + '</div>';
				shtml += '<div class="sc-desc">' + smalldata[i].desc + '</div>';
				shtml += '<div class="sc-text">' + smalldata[i].pressure + '</div>';
				shtml += '<div class="sc-text">' + smalldata[i].humidity + '</div>';
				shtml += '<div class="sc-text">' + smalldata[i].wind + '</div>';

				$(".small-wrap-" + pk + " > .small-" + i).html(shtml);
			}

			//build the current weather
			var curdesc = curw.weather[0].main;
			var curicon = curw.weather[0].icon;
			var utime = curw.dt * 1000;
			utime = new XDate(utime);
			utime = utime.toString("ddd dd MMM yyyy, HH:mm");
			var curtemp = curw.main.temp;
			var curcc = curw.clouds.all;
			var curpressure = curw.main.pressure;
			var curhumidity = curw.main.humidity;
			var curmintemp = curw.main.temp_min;
			var curmaxtemp = curw.main.temp_max;
			var winddir = curw.wind.deg;
			var windspeed = curw.wind.speed;


			var whtml = '<div class="weather-forecast">';

			//show the 5 day forecast
			//forecast returns todays weather - so ignore list[0]
			for (var i=0; i<5; i++) {
				var w = forew.list[i];
				var futime = w.dt * 1000;
				futime = new XDate(futime);
				futime = futime.toString("dddd");
				var ficon = w.weather[0].icon;
				var fdesc = w.weather[0].main;
				var fdaytemp = w.temp.day;
				var fspeed = w.speed;

				whtml += '<div class="weather-forecast-day">';
				whtml += '<p><span class="f-time">' + futime + '</span></p>';
				whtml += '<p><span class="f-icon">' + wcodes[ficon] + '</span></p>';
				whtml += '<p class="f-text"><span class="f-desc">Outlook: ' + fdesc + '</span></p>';
				whtml += '<p class="f-text"><span class="f-temp">Temp: ' + (Math.round((fdaytemp - 273.15) * 10) / 10).toFixed(1) + ' &deg;C</span></p>';
				whtml += '<p class="f-text"><span class="f-speed">Windspeed: ' + fspeed + '</span></p>';
				whtml += '</div>';
			}

			whtml += '</div>';

			$("#wcurr-" + pk).html(whtml);

		});
	});


}

function homeDocReady() {
	//get the list of all the meters that the user has access to
	var $html = "";
	var sysw = 0;
	$.ajax({
		url: "/user/meter",
		type: "GET",
		dataType: "JSON",
		async: false,
		success: function(result) {
			$.each(result, function(key, meter) {
				//get the sysw for the site
				$.ajax({
					url: "/api/site/now?mprn=" + meter.mprn,
					type: "GET",
					dataType: "JSON",
					async: false,
					success: function(sitedata) {
						$.each(sitedata, function(key, data) {
							sysw += data.sys_w;
						});

						$html += '<p>' + meter.site_description + ' - ' + meter.metered_circuit;
						$html += ': <span id="' + meter.mprn + '">' + sysw + '</span></p>';
						sysw = 0;
					}
				});
			});

			$("#home-sitelist").html($html);
		},
		error: function() {
			showMessage("err", defaultErrMsg);
		}
	});
}

function buildNodeMap() {
	$.ajax({
		url: "/user/meter",
		type: "GET",
		dataType: "JSON",
	}).done(function(result) {
			userMeters = result;

			$.ajax({
				url: "/user/node",
				type: "GET",
				dataType: "JSON",
			}).done(function(result) {

					$.each(result, function(key, meter) {
						var node = meter.node_id;
						var conMeter = meter.controller_id + "," + meter.meter_id;
						var desc = meter.node_description;

						if (node) {
							if (!nodeMap[node]) {
								nodeMap[node] = {};
							}

							nodeMap[node][conMeter] = node;
							nodeMap[node].desc = desc;

							var conID = "";
							var list = "";

							$.each(userMeters, function(key, val) {
								if (node === val.limiting_node) {
									var mprn = val.mprn;

									$.each(userMeters, function(k, v) {
										if (mprn === v.limiting_site) {
											conID = v.controller_id + "," + v.meter_id;

											if (list.indexOf(conID) === -1) {
												list += conID  + ",,";
											}
										}
									});
								}
							});

							list = list.slice(0, -2);
							nodeMap[node].incomers = list;
						}

					});
			});
	}).fail(function(o, s, e) {
			showMessage("err", defaultErrMsg);
	});
}

//check if dispatch was cancelled before dispatch was due to start
function dispCanxBefore(timeon, timecanx) {
	if (!timecanx) { //not cancelled
		return false;
	} else {
		var s = timeon.getTime();
		var c = timecanx.getTime();

		if (c < s) { //canx before start
			return true;
		} else {
			return false;
		}
	}
}

function getNodeFromMPRN(mprn) {
	var conId, meterId;
	var nodedata = [];

	$.each(userMeters, function(id, meter) {
		if (mprn === meter.mprn) {
			conId = meter.controller_id;
			meterId = meter.meter_id;

			var cm = conId + "," + meterId;

			$.each(nodeMap, function(node, ndata) {
				if (ndata[cm]) {
					nodedata.push(ndata.desc);
				}
			});
		}
	});

	return nodedata;
}

function accountDocReady() {
	var siteinfo = "";

	if (verifyResult.toLowerCase() === "true") {
		localStorage.removeItem("userData");
		getUserData();

		var mess = "<p>Thank you for verifying your email.</p>";

		setTimeout(function() {
			showMessage("info", mess);
		}, 500);
	} else if (verifyResult.toLowerCase() === "false") {
		var mess = "<p>Unable to verify email address. Please check that you are logged in as the correct user and that you have clicked the correct link.</p>";

		setTimeout(function() {
			showMessage("err", mess);
		}, 500);
	}

    //bind to text boxes to prevent pasting
    $("#newemail").bind("paste",function(e) {
        e.preventDefault();
    });

    $("#confirmnewemail").bind("paste",function(e) {
        e.preventDefault();
    });

    $("#user-email-form-submit").on("click", function() {
        Pace.start();
    });

    $("#user-phone-form-submit").on("click", function() {
        Pace.start();
    });

	updateUserInfo();

	$.each(userSites, function(key, site) {
		siteinfo += '<div class="user-sites-wrap">';
		siteinfo += '<h2>' + site.fields.description + '</h2>';
		siteinfo += '<h3>Site Information</h3>';
        siteinfo += '<span class="user-mprn">MPRN: ' + site.pk + '</span>';
		siteinfo += '<span class="user-mic">Maximum Import Capacity: ' + site.fields.mic + ' kVA</span>';
        siteinfo += '<span class="user-mec">Maximum Export Capacity: ' + site.fields.mec + ' kVA</span>';
		siteinfo += '<span class="user-voltage">Voltage Level: ' + site.fields.voltage_level + '</span>';
		siteinfo += '<span class="user-duos">DUoS Group: ' + site.fields.duos_group + '</span>';
		siteinfo += '<h3>Dispatchable Units</h3>';

		var units = getNodeFromMPRN(site.pk);
		var uniqnode = $.unique(units);

		if (uniqnode) {
			$.each(uniqnode, function(id, node) {
				siteinfo += '<span class="user-node">' + node + '</span>';
			});
		} else {
			siteinfo += '<span class="user-node">&#8212;</span>';
		}

		siteinfo += '</div>';

	});

	$("#user-sites").append(siteinfo);

	$("#passform").on("submit", function(e) {
		e.preventDefault();

		var curpass = $("#curpass").val();
		var newpass1 = $("#newpass").val();
		var newpass2 = $("#newpassconf").val();

		if (curpass && (newpass1 === newpass2)) {
			Pace.start();

			var data = "password=" + newpass1 + "&old_password=" + curpass;

			$.ajax({
				url: '/user/change/password',
				type: 'POST',
				data: data,
				success: function(result) {
					var mess = '<p>Your password has been sucessfully changed.</p>';
					showMessage("info", mess);
				},
				error: function() {
					showMessage("err", 'Could not change your password. Please try again');
				}
			});

			Pace.stop();

		} else {
			console.log(curpass, newpass1, newpass2);
			var msg = "<p>To change your password, please check the following:</p><ul>";

			if (!curpass) {
				msg += "<li>Please check your current password</li>";
			}

			if (newpass1 !== newpass2) {
				msg += "<li>The new passwords you entered do not match. Please try again.</li>";
				$("#newpass").val("");
				$("#newpassconf").val("");
			}

			msg += "</ul>";

			showMessage("err", msg);
		}
	});

	$("#passform").on("reset", function(e) {
		$("#user-pass").slideUp(500);
	});

}

function updateUserInfo() {
	var username = userData[0].username;
	var fname = userData[0].first_name;
	var lname = userData[0].last_name;
	var email = userData[0].email;
	var phone = userData[0].phone_number;
	var login = XDate.parse(userData[0].last_login);
	login = new XDate(login).toString("ddd, dd MMM yyyy, HH:mm:ss");
	var emailverified = userData[0].email_verified;
	var phoneverified = userData[0].phone_verified;

	if (!emailverified && email) {
		$("#emailverified").show();
	} else {
		$("#emailverified").hide();
	}

	if (!phoneverified && phone) {
		$("#phoneverified").show();
	} else {
		$("#phoneverified").hide();
	}

	$("#username").text(username);
	$("#name").text(fname + " " + lname);

	if (email !== null) {
		$("#email").text(email);
	} else {
		$("#email").html('<a href="javascript: $(\'#modemail\').click();">Add an Email Address</a>');
		$("#modemail").html("Add Your Email Address");
	}

	if (phone !== null) {
		$("#phone").text(phone);
	} else {
		$("#phone").html('<a href="javascript: $(\'#modphone\').click();">Add a Mobile Phone Number</a>');
		$("#modephone").html("Add Your Phone Number");
	}

	$("#lastlogin").text(login);
}

function modifyUserEmail() {
	var curemail = $("#email").html();
	if (curemail.indexOf("href") !== -1) {
		curemail = "";
	}

	$("#emailverified").fadeOut(500);
	$("#user-email-form").slideDown(500, function() {
		$("#newemail").focus();
	});

	//bind to form submit and reset
	$("#emailform").on("submit", function(e) {
		e.preventDefault();
		Pace.start();
		var newemail = $("#newemail").val();
		var confirmemail = $("#confirmnewemail").val();

		if (newemail == confirmemail && newemail) {

			var data = "email=" + newemail;

			//save the new email
			$.ajax({
				url: "/user/change/email",
				type: "POST",
				data: data,

				success: function(result) {
					$("#email").html(newemail);

					$("#user-email-form").slideUp(500);
					$("#emailverified").fadeIn(1000);

					var mess = "<p>Your email address has been updated. A verification email has been sent.<br><br>";
					mess += "Please click the link in the email to verify, our you can manually enter the verification code by clicking the verify button beside your email address<br><br>";
					mess += "If you do not receive the email within 24 hours, please check your email and reverify.";

					showMessage("info", mess);
					localStorage.removeItem("userData");
					getUserData();
					updateUserInfo();
				},
				error: function(o, sts, err) {
                    Pace.stop();
					var mess ="<p>We were unable to update your email address. Please check your email address and try again.</p>";
					showMessage("err", mess);
				}
			});

		} else {
            Pace.stop();
			showMessage("err", "The supplied email addresses do not match or are empty. Please check, and try again.");
		}
		Pace.stop();
	});

	$("#emailform").on("reset", function(e) {
		setTimeout(function() {
			var oldemail = $("#curemail").val();
			if (oldemail === "") {
				oldemail = '<a href="javascript: $(\'#modemail\').click();">Add an Email Address</a>';
			}

			$("#user-form").html(oldemail);
			$("#user-email-form").slideUp(500);
//			$("#emailverified").fadeIn(500);
		}, 1);
	});
}

function modifyUserPhone() {
	var curphone = $("#phone").html();
	if (curphone.indexOf("href") !== -1) {
		curphone = "";
	}

	$("#curphone").val(curphone);
	$("#phoneverified").fadeOut(500);
	$("#user-phone-form").slideDown(500, function() {
		$("#newphone").focus();
	});

	//bind to form submit and reset
	$("#phoneform").on("submit", function(e) {
		e.preventDefault();
		Pace.start();
		var newphone = $("#newphone").val();
		newphone = cleanPhone(newphone);

		//check if the phone is valid
		var isvalid = isValidNumber(newphone, "IE");

		if (isvalid) {

			//save the new phone
			var eformat = formatE164("IE", newphone);

			var data = "phone=" + encodeURIComponent(eformat);

			$.ajax({
				url: "/user/change/phone",
				type: "POST",
				data: data,

				success: function(result) {
					$("#phone").html(eformat);

					$("#user-phone-form").slideUp(1000);

					$("#phoneverified").fadeIn(1000);

					var mess = "<p>Your mobile number has been updated and a verification code has been sent.<br><br>";
					mess += "On receipt of the verification code, please click the verify button beside your mobile number and enter the code.<br><br>";
					mess += "If you do not receive the text within 30 minutes, please check your mobile number and re-verify.</p>";

					showMessage("info", mess);

					localStorage.removeItem("userData");
					getUserData();
					updateUserInfo();
				},
				error: function(o, sts, err) {
                    Pace.stop();
					var mess ="<p>We were unable to update your mobile number. Please check your email address and try again.</p>";
					showMessage("err", mess);
				}
			});


		} else {
            Pace.stop();
			showMessage("err", "The supplied mobile number is not valid. Please check, and try again.");
		}

        Pace.stop();
	});

	$("#phoneform").on("reset", function(e) {
		setTimeout(function() {
			var oldphone = $("#curphone").val();
			if (oldphone === "") {
				oldphone = '<a href="javascript: $(\'#modphone\').click();">Add a Mobile Phone Number</a>';
			}
			$("#phone").html(oldphone);
			$("#user-phone-form").slideUp(500);

		}, 1);
	});
}

function modifyUserPass() {
	$("#user-pass").slideDown(500, function() {
		$("#curpass").focus();
	});
}

function verifyUserData(type) {
	//create a modal box to display form

	var $usermodal = $("#user-modal").length;

	if (!$usermodal) {
		var form = '<div id="user-modal">';
		form += '<span class="user-modal-wrap"><label for="verifycode">Verification Code:</label>';
		form += '<input type="text" id="verifycode" name="verifycode"></span>';
		form += '</div>';

		$("body").append(form);
	} else {
		$("#verifycode").val("");
	}

	$("#user-modal").dialog({
		buttons: [{
			text: "Verify",
			click: function() {
				var code = $("#verifycode").val();

				$.ajax({
					url: "/portal/account/verify/" + type + "/" + code,
					type: "POST",
					data: null,

					success: function(result) {
						localStorage.removeItem("userData");
						getUserData();
						updateUserInfo();

						var mess = "<p>Thank you. Your ";

						if (type === "email") {
							mess += "email address";
						} else {
							mess += "mobile number";
						}

						mess += " has been verified."

						showMessage("info", mess);
					},
					error: function(o, sts, err) {
						var mess ="<p>We were unable to verify your ";

						if (type == "email") {
							mess += "email address";
						} else {
							mess += "mobile number";
						}

						mess += ". Please check the verification code and ensure that you are logged into the correct account and try again.</p>";

						showMessage("err", mess);
					}
				});

				$(this).dialog("close");
			}
		}, {
			text: "Cancel",
			click: function() {
				$(this).dialog("close");
			}
		}, {
			text: "Request New Verification Code",
			click: function() {
				if (type === "phone") {
                    $(this).dialog("close");
					var eformat = formatE164("IE", $("#phone").text());

                    var data = "phone=" + encodeURIComponent(eformat);

                    $.ajax({
                        url: "/user/change/phone",
                        type: "POST",
                        data: data,

                        success: function(result) {
                            $("#phone").html(eformat);

                            $("#user-phone-form").slideUp(1000);

                            $("#phoneverified").fadeIn(1000);

                            var mess = "<p>A new verification code has been sent.<br><br>";
                            mess += "On receipt of the verification code, please click the verify button beside your mobile number and enter the code.<br><br>";
                            mess += "If you do not receive the text within 30 minutes, please check your mobile number and re-verify.</p>";

                            showMessage("info", mess);

                            localStorage.removeItem("userData");
                            getUserData();
                            updateUserInfo();
                        },
                        error: function(o, sts, err) {
                            Pace.stop();
                            var mess ="<p>We were unable to request a new verification code. Please try again.</p>";
                            showMessage("err", mess);
                        }
                    });

                    Pace.stop();

				} else if (type === "email") {
                    $(this).dialog("close");
                    Pace.start();
                    var data = "email=" + $("#email").text();

                    //save the new email
                    $.ajax({
                        url: "/user/change/email",
                        type: "POST",
                        data: data,

                        success: function(result) {

                            $("#emailverified").fadeIn(1000);

                            var mess = "<p>A new verification email has been sent.<br><br>";
                            mess += "Please click the link in the email to verify, our you can manually enter the verification code by clicking the verify button beside your email address<br><br>";
                            mess += "If you do not receive the email within 24 hours, please check your email and reverify.";

                            showMessage("info", mess);
                            localStorage.removeItem("userData");
                            getUserData();
                            updateUserInfo();
                        },
                        error: function(o, sts, err) {
                            Pace.stop();
                            var mess ="<p>We were unable to request a new verification code. Please try again.</p>";
                            showMessage("err", mess);
                        }
                    });

					Pace.stop();
				}
			},
			'class': 'user-reverify-button'
		}],
		closeOnEscape: true,
		dialogClass: "user-dialog",
		draggable: false,
		height: 200,
		hide: 500,
		modal: true,
		position: {
			my: "bottom",
			at: "center",
			of: window
		},
		resizable: false,
		show: 500,
		title: "Please Enter Your Verification Code",
		width: 400,
	});

	$("#verifycode").focus();
}

function notificationsDocReady() {
    //show the users currently existing notifications
	$.ajax({
		url: "/api/notification",
		type: "GET",
		dataType: "JSON",
		success: function(result) {
			//parse userNotifications to match structure used to set notifications
			userNotifications = result;
			displayNotifications(result);
		},
		error: function(o, s, e) {
			showMessage("err", defaultErrMsg);
		}
	});
}

function createNotificationsDocReady() {
    //prevent paste into text box
    $("#note-desc").bind("paste",function(e) {
        e.preventDefault();
    });

	getAllowedParams();
	var meterlist = "";
	var paramlist = "";
	var paramnum = 0;
	var alphanum = [32,43,44,45,46,40,41,60,61,62];

	for (var i=65; i<=90; i++) {
		alphanum.push(i);
	}

	for (var i=97; i<=122; i++) {
		alphanum.push(i);
	}

	for (var i=48; i<=57; i++) {
		alphanum.push(i);
	}

	$("#note-desc").on("click", function() {
		if ($(this).val() === "Maximum 100 Characters") {
			$(this).val("");
		}
	});

	$("#note-desc").on("blur", function() {
		if ($(this).val() === "") {
			$(this).val("Maximum 100 Characters");
		}
	});

	$("#note-desc").keypress(function(e) {
		var kk = e.which;
		if ($.inArray(kk, alphanum) === -1) {
			e.preventDefault();
		}
	});

	var min = new XDate(0).setHours(0).setMinutes(0).setSeconds(0);
	var max = min.clone().addDays(2);

	$("#note-slider").dateRangeSlider({
		arrows: false,
		bounds: {
			min: min,
			max: max
		},
		defaultValues: {
			min: min,
			max: max
		},
		formatter: function(val) {
			var date = new XDate(val);
			date = date.toString("HH:mm");
			return date;
		},
		range: {
			min: {minutes: 15},
			max: false
		},
		step: {minutes: 15}
	});


	$(".note-days-item").on("click", function() {
		$(this).toggleClass("note-days-item-selected");
	});

	$.each(userMeters, function(key, meter) {
		meterlist += '<option value="' + meter.controller_id + '_' + meter.meter_id + '">' + meter.site_description + ' - ' + meter.metered_circuit + '</option>';
	});

	$("#note-meters").append(meterlist);

	$("#note-meters > option").tsort();

	$("#note-meters").chosen({
		max_selected_options: 1,
		placeholder_text_multiple: "Please select a metered circuit.",
		width: "400px"
	});

	$("#note-meters").on("change", function(e, p) {

		if (p.selected) {
			var meter = p.selected;
			updateAllowedNoteParams(meter, "add");
		} else {
			updateAllowedNoteParams(0, "remove");
		}
	});

	$("#note-params").chosen({
		placeholder_text_multiple: "Please select a metered circuit first.",
		width: "400px"
	});

	$("#note-params").on("change", function(e, p) {
		if (p.selected) {
			var meter = $("#note-meters").val()[0];

			var item = '<div class="note-bounds-item" id="note-bounds-item-' + paramnum + '" data-param="' + p.selected + '">';
			item += '<span class="note-bound-param" id="note-bound-' + paramnum + '" data-param="';
			item += p.selected + '">' + convertConMeter(meter) + ": " + engParams[p.selected].name + '</span>';
			item += '<input type="text" class="note-bound-param-val" id="note-bound-val-' + paramnum + '" data-param="';
			item += p.selected + '"> ' + engParams[p.selected].units + '</div>';

			if (paramnum === 0) {
				$("#note-bounds").html(item);
			} else {
				$("#note-bounds").append(item);
			}

			$("#note-bounds-item-" + paramnum).slideDown();

			$("#note-bound-val-" + paramnum).keypress(function(e) {
				var kk = e.which;
				var allkeys = [43,45,48,49,50,51,52,53,54,55,56,57];
				var numkeys = [48,49,50,51,52,53,54,55,56,57];

				if ($(this).val().length === 0) {
					if ($.inArray(kk, allkeys) === -1) {
						e.preventDefault();
					}
				} else {
					if ($.inArray(kk, numkeys) === -1) {
						e.preventDefault();
					}
				}
			});

			paramnum++;

		} else if (p.deselected) {
			$('.note-bounds-item[data-param="' + p.deselected + '"]').slideUp(function() {
				$('.note-bounds-item[data-param="' + p.deselected + '"]').remove();

				if ($("#note-bounds").children().length === 0) {
					var out = '<span id="note-bounds-none">No Metered Circuit/ Parameters Selected Yet.</span>';

					$("#note-bounds").html(out);
					paramnum = 0;
				}
			});
		}

	});

	$("#note-bound-type").chosen({
		max_selected_options: 1,
		placeholder_text_multiple: "Please select if bounds are a lower or upper limit.",
		width: "400px"
	});

	$("#note-ignore-time").keypress(function(e) {
		var kk = e.which;
		var numkeys = [48,49,50,51,52,53,54,55,56,57];

		if ($.inArray(kk, numkeys) === -1) {
			e.preventDefault();
		}
	});

	$("#note-ignore-time").on("click", function() {
		if ($(this).val() === "0") {
			$(this).val("");
		}
	});

	$("#note-ignore-time").on("blur", function() {
		if ($(this).val() === "" || !$.isNumeric($(this).val())) {
			$(this).val("0");
		}
	});

	$("#note-notifyby").chosen({
		placeholder_text_multiple: "Please select a notification method.",
		width: "400px"
	});

	$("#note-resolved").chosen({
		max_selected_options: 1,
		placeholder_text_multiple: "Please select when to receive notifications.",
		width: "400px"
	});

	$("#note-form").on("reset", function() {
		clearNotificationForm();
	});

	$("#note-form").submit(function(e) {
		e.preventDefault();
		Pace.start();
		var hasErr = false;
		var errMsg = "There was a problem creating your notification. Please correct the following issues and try again.<ul>";
		var data = {
			upper_lower: null,
			seconds_to_latch: 0,
			time_from: "1970-01-01 00:00",
			time_to: "1970-01-01 00:00",
			weekdays: null,
			event_description: null,
			controller_id: null,
			meter_id: null,
			enabled: 1,
			notify_by_email: 0,
			notify_by_sms: 0,
			notify_when_resolved: 0,
			id: parseInt($("#note-id").val()),
			freq: null,
			phase_1_amps: null,
			phase_1_volts: null,
			phase_2_amps: null,
			phase_2_volts: null,
			phase_3_amps: null,
			phase_3_volts: null,
            neutral_amps: null,
			sys_va: null,
			sys_var: null,
			sys_w: null
		};

		var desc = $("#note-desc").val();
		if (desc === "Maximum 100 Characters") {
			errMsg += '<li>"Description" not provided.</li>';
			hasErr = true;
		}

		var meter = $("#note-meters").val();
		if (meter === null) {
			errMsg += '<li>"Metered Circuit" circuit not selected.</li>';
			hasErr = true;
		}

		var parameter = $("#note-params").val();
		if (parameter === null) {
			errMsg == '<li>"Parameter" not selected.</li>';
			hasErr = true;
		}

		var type = $("#note-bound-type").val();
		if (type === null) {
			errMsg += '<li>"Type" not selected.</li>';
			hasErr = true;
		}

		var bounds = $(".note-bound-param-val");
		if (bounds.length === 0) {
			errMsg += '<li>"Bounds" not selected.</li>';
			hasErr = true;
		} else {
			bounds.each(function(key, val) {
				if (!$.isNumeric($(val).val())) {
					errMsg += '<li>"' + $(val).siblings(".note-bound-param").text() + '" requires a numeric value.</li>';
					hasErr = true;
				}
			});
		}

		var seldays = $(".note-days-item-selected");
		if (seldays.length === 0) {
			errMsg += '<li>"Active Days" not selected.</li>';
			hasErr = true;
		}

		var latch = parseInt($("#note-ignore-time").val());
		if (latch === null || !$.isNumeric(latch)) {
			errMsg += '<li>"Don\'t Notify if Event Lasts Less Than" not selected.</li>';
			hasErr = true;
		} else if (!$.isNumeric(latch)) {
			errMsg += '<li>""Don\'t Notify if Event Lasts Less Than" is not a number.</li>';
			hasErr = true;
		}

		var via = $("#note-notifyby").val();
		if (via === null) {
			errMsg += '<li>"Notification Method" not selected.</li>';
			hasErr = true;
		}

		var resolved = $("#note-resolved").val();
		if (resolved === null) {
			errMsg += '<li>"Notification Trigger" not selected</li>';
			hasErr = true;
		}

		startend = $("#note-slider").dateRangeSlider("values");
		start = startend.min;
		end = startend.max;

		if (hasErr) {
            Pace.stop();
			errMsg += "</ul>";
			showMessage("err", errMsg);
		} else {
			//build json object to save notification
			bounds.each(function(key, val) {
                if ($.inArray($(val).attr("data-param"), power_list) !== -1) {
				    data[$(val).attr("data-param")] = parseInt($(val).val()) * 1000;
                } else {
                    data[$(val).attr("data-param")] = parseInt($(val).val());
                }
			});

			var weekdays = "";
			seldays.each(function(key, val) {
				weekdays += $(val).attr("data-value") + ",";
			});

			weekdays = weekdays.slice(0, -1);
			data.weekdays = weekdays;

			data.upper_lower = type[0];
			data.event_description = desc;

			var conmeter = meter[0].split("_");
			data.controller_id = parseInt(conmeter[0]);
			data.meter_id = parseInt(conmeter[1]);

			var time_from = new XDate(start).toString("HH:mm");
			time_from = "1970-01-01 " + time_from;

			var time_to = new XDate(end).toString("HH:mm");
			time_to = "1970-01-01 " + time_to;

			data.time_from = time_from;
			data.time_to = time_to;
			data.seconds_to_latch = latch;

			$.each(via, function(key, val) {
				if (val === "none") {
					data.notify_by_email = 0;
					data.notify_by_sms = 0;
					return false;
				} else if (val === "email") {
					data.notify_by_email = 1;
				} else if (val === "text") {
					data.notify_by_sms = 1;
				}
			});

			data.notify_when_resolved = parseInt(resolved[0]);

			setNotification(data, "new");
		}
	});
}

function clearNotificationForm() {
	$("#note-meters > option").each(function() {
		$(this).removeAttr("selected");
	});
	$("#note-meters").trigger("chosen:updated");

	$("#note-bound-type > option").each(function() {
		$(this).removeAttr("selected");
	});
	$("#note-bound-type").trigger("chosen:updated");

	$("#note-bounds").html('<span id="note-bounds-none">No Metered Circuit/ Parameters Selected Yet.</span>');

	$("#note-params option").each(function() {
		$(this).removeAttr("selected");
	});
	$("#note-params").trigger("chosen:updated");

	var min = new XDate(0).setHours(0).setMinutes(0).setSeconds(0);
	var max = min.clone().addDays(2);
	$("#note-slider").dateRangeSlider("values", min, max);

	$(".note-days-item").each(function() {
		$(this).removeClass("note-days-item-selected");
	});

	$("#note-ignore-time").val(0);

	$("#note-notifyby > option").each(function() {
		$(this).removeAttr("selected");
	});
	$("#note-notifyby").trigger("chosen:updated");

	$("#note-resolved > option").each(function() {
		$(this).removeAttr("selected");
	});
	$("#note-resolved").trigger("chosen:updated");

	$("#note-id").val(0);
}

function editNotification(id) {
	clearNotificationForm();

	$.each(userNotifications, function(key, note) {
		var paramnum = 0;

		if (note.id === id) {
			$("#note-desc").val(note.event_description);

			var conMeter = note.controller_id + "_" + note.meter_id;

			$("#note-meters > option").each(function() {
				if ($(this).val() === conMeter) {
					$(this).attr("selected", "selected");
					$("#note-meters").trigger("chosen:updated");
					updateAllowedNoteParams(conMeter, "add");
				}
			});

			var limit = note.upper_lower;

			$("#note-bound-type > option").each(function() {
				if ($(this).val() === limit) {
					$(this).attr("selected", "selected");
					$("#note-bound-type").trigger("chosen:updated");
				}
			});

			var param = [];
			if (note.freq !== null) param.push("freq");
			if (note.phase_1_amps !== null) param.push("phase_1_amps");
			if (note.phase_2_amps !== null) param.push("phase_2_amps");
			if (note.phase_3_amps !== null) param.push("phase_3_amps");
			if (note.phase_1_volts !== null) param.push("phase_1_volts");
			if (note.phase_2_volts !== null) param.push("phase_2_volts");
			if (note.phase_3_volts !== null) param.push("phase_3_volts");
			if (note.sys_va !== null) param.push("sys_va");
			if (note.sys_var !== null) param.push("sys_var");
			if (note.sys_w !== null) param.push("sys_w");

			$("#note-params option").each(function() {
				var val = $(this).val();
				if ($.inArray(val, param) !== -1) {
					$(this).attr("selected", "selected");
					$("#note-params").trigger("chosen:updated");

					var meter = $("#note-meters").val()[0];
					var item = '<div class="note-bounds-item" id="note-bounds-item-' + paramnum + '" data-param="' + val + '">';
					item += '<span class="note-bound-param" id="note-bound-' + paramnum + '" data-param="';
					item += val + '">' + convertConMeter(meter) + ": " + engParams[val].name + '</span>';
					item += '<input type="text" class="note-bound-param-val" id="note-bound-val-' + paramnum + '" data-param="';
					item += val + '" value="' + note[val] + '"> ' + engParams[val].units + '</div>';

					if (paramnum === 0) {
						$("#note-bounds").html(item);
					} else {
						$("#note-bounds").append(item);
					}

					$("#note-bounds-item-" + paramnum).slideDown();

					$("#note-bound-val-" + paramnum).keypress(function(e) {
						var kk = e.which;
						var allkeys = [43,45,48,49,50,51,52,53,54,55,56,57];
						var numkeys = [48,49,50,51,52,53,54,55,56,57];

						if ($(this).val().length === 0) {
							if ($.inArray(kk, allkeys) === -1) {
								e.preventDefault();
							}
						} else {
							if ($.inArray(kk, numkeys) === -1) {
								e.preventDefault();
							}
						}
					});

					paramnum++;
				}
			});

			var timef = new XDate(note.time_from);
			var timet = new XDate(note.time_to).addDays(1);
			$("#note-slider").dateRangeSlider("values", timef, timet);

			var weekdays = note.weekdays.split(",");
			$(".note-days-item").each(function() {
				if ($.inArray($(this).attr("data-value"), weekdays) !== -1) {
					$(this).addClass("note-days-item-selected");
				} else {
					$(this).removeClass("note-days-item-selected");
				}
			});

			var latch = note.seconds_to_latch;
			$("#note-ignore-time").val(latch);

			var byemail = note.notify_by_email;
			var bysms = note.notify_by_sms;

			$("#note-notifyby > option").each(function() {
				if ($(this).val() === "email") {
					if (byemail === 1) {
						$(this).attr("selected", "selected");
					} else{
						$(this).removeAttr("selected");
					}
				}

				if ($(this).val() === "text") {
					if (bysms === 1) {
						$(this).attr("selected", "selected");
					} else {
						$(this).removeAttr("selected");
					}
				}

				if ($(this).val() === "none") {
					if (byemail === 0 && bysms === 0) {
						$(this).attr("selected", "selected");
					} else {
						$(this).removeAttr("selected");
					}
				}
			});

			$("#note-notifyby").trigger("chosen:updated");

			var resolved = note.notify_when_resolved;

			$("#note-resolved > option").each(function() {

				if ($(this).val() == resolved) {
					$(this).attr("selected", "selected");
				} else {
					$(this).removeAttr("selected");
				}
			});

			$("#note-resolved").trigger("chosen:updated");

			$("#note-id").val(id);
		}
	});
	//show the notification form
	$("#note-options").slideDown(500);

	$("html body").animate({scrollTop: 0}, {duration: 500, queue: false}, function() {
		$("#note-show").trigger("click");
	});
}

function setNotification(data, type) {
	var stringdata = JSON.stringify(data);
	var urldata = "data=" + stringdata;

	//post the data
	$.ajax({
		url: "/api/notification",
		type: "POST",
		data: urldata,
		success: function(result) {

			if (type === "new") {
				var newnote = {};
				newnote[0] = data;

				var notelen = Object.keys(userNotifications).length;
				userNotifications[notelen] = newnote;

				displayNotifications(newnote);
			}
		},
		error: function(o, s, e) {
            Pace.stop();
			showMessage("err", "Unable to save your notification. Please check your Internet connection and try again.");
		}
	});

    Pace.stop();
}

function toggleNotification(id) {
	//disabled due to issues with structure of notifications object
	$.each(userNotifications, function(key, note) {
		if (note.id === id) {
			note.enabled = (1 - note.enabled); //toggles between 1 and 0
			setNotification(note, "update");
		}
	});

}

function displayNotifications(result) {
	var nhtml = "";
	var enabledsort = 1;
	var disabledsort = Object.keys(result).length;

	$.each(result, function(id, note) {
		var enabled = note.enabled
		nhtml += '<div class="note-existing-wrap';
		if (enabled === 0) {
			nhtml += ' note-disabled-wrap" data-sort="' + disabledsort + '">';
			disabledsort--;
		} else {
			nhtml += '" data-sort="' + enabledsort + '">';
			enabledsort++;
		}
		nhtml += '<div class="note-existing-titlebar';
		if (enabled === 0) nhtml += ' note-disabled-title';
		nhtml += '">';
		nhtml += '<div class="note-existing-site">' + note.event_description + '</div>';
		nhtml += '<div class="note-existing-edit"><span onclick="editNotification(' + note.id + ');">&#xf2bf;</span></div>';
		nhtml += '<div class="note-existing-enabled"><span onclick="toggleNotification(' + note.id + ');">&#xf2a9;</span></div></div>';
		nhtml += '<div class="note-existing-meta">';
		nhtml += '<span class="note-existing-circuit"><b>Circuit:</b> ' + convertConMeter(note.controller_id + '_' + note.meter_id) + '</span>';
		nhtml += '<span class="note-existing-action"><b>Action:</b> ';
		if (note.notify_by_email === 1) nhtml += 'Email, ';
		if (note.notify_by_sms === 1) nhtml += 'SMS, ';
		nhtml += ' Log.</span>';
		nhtml += '<span class="note-existing-trigger"><b>Trigger:</b> ';
		if (note.upper_lower === "lower") nhtml += 'Below ';
		if (note.upper_lower === "upper") nhtml += 'Above ';
		nhtml += 'threshold for more than ' + note.seconds_to_latch + ' seconds.</span>';
		nhtml += '<span class="note-existing-resolved"><b>Notify When Resolved:</b> ';
		if (note.notify_when_resolved === 1) nhtml += 'Yes.';
		if (note.notify_when_resolved === 0) nhtml += 'No.';
		nhtml += '</span></div>';
		nhtml += '<div class="note-existing-param-types">';
		nhtml += '<span class="note-existing-param-type">kW</span>';
		nhtml += '<span class="note-existing-param-type">kVAR</span>';
		nhtml += '<span class="note-existing-param-type">kVA</span>';
		nhtml += '<span class="note-existing-param-type">P1 V</span>';
		nhtml += '<span class="note-existing-param-type">P2 V</span>';
		nhtml += '<span class="note-existing-param-type">P3 V</span>';
		nhtml += '<span class="note-existing-param-type">P1 A</span>';
		nhtml += '<span class="note-existing-param-type">P2 A</span>';
		nhtml += '<span class="note-existing-param-type">P3 A</span>';
        nhtml += '<span class="note-existing-param-type">Neutral A</span>';
		nhtml += '<span class="note-existing-param-type">FREQ</span>';
		nhtml += '</div>';
		nhtml += '<div class="note-exisiting-param-vals">';
		nhtml += '<span class="note-existing-param-val">';
		note.sys_w !== null ? nhtml += (note.sys_w / 1000).toFixed(0) : nhtml += ' - ';
		nhtml += '</span><span class="note-existing-param-val">';
		note.sys_var !== null ? nhtml += (note.sys_var / 1000).toFixed(0) : nhtml += ' - ';
		nhtml += '</span><span class="note-existing-param-val">';
		note.sys_va !== null ? nhtml += (note.sys_va / 1000).toFixed(0) : nhtml += ' - ';
		nhtml += '</span><span class="note-existing-param-val">';
		note.phase_1_volts !== null ? nhtml += note.phase_1_volts : nhtml += ' - ';
		nhtml += '</span><span class="note-existing-param-val">';
		note.phase_2_volts !== null ? nhtml += note.phase_2_volts : nhtml += ' - ';
		nhtml += '</span><span class="note-existing-param-val">';
		note.phase_3_volts !== null ? nhtml += note.phase_3_volts : nhtml += ' - ';
		nhtml += '</span><span class="note-existing-param-val">';
		note.phase_1_amps !== null ? nhtml += note.phase_1_amps : nhtml += ' - ';
		nhtml += '</span><span class="note-existing-param-val">';
		note.phase_2_amps !== null ? nhtml += note.phase_2_amps : nhtml += ' - ';
		nhtml += '</span><span class="note-existing-param-val">';
		note.phase_3_amps !== null ? nhtml += note.phase_3_amps : nhtml += ' - ';
        nhtml += '</span><span class="note-existing-param-val">';
		note.neutral_amps !== null ? nhtml += note.neutral_amps : nhtml += ' - ';
		nhtml += '</span><span class="note-existing-param-val">';
		note.freq !== null ? nhtml += note.freq : nhtml += ' - ';
		nhtml += '</span></div>';
		nhtml += '<div class="note-existing-days">';
		nhtml += '<span class="note-existing-day';
		if (note.weekdays.indexOf(0) !== -1) nhtml += ' note-selected';
		nhtml += '">Mon</span>';
		nhtml += '<span class="note-existing-day';
		if (note.weekdays.indexOf(1) !== -1) nhtml += ' note-selected';
		nhtml += '">Tue</span>';
		nhtml += '<span class="note-existing-day';
		if (note.weekdays.indexOf(2) !== -1) nhtml += ' note-selected';
		nhtml += '">Wed</span>';
		nhtml += '<span class="note-existing-day';
		if (note.weekdays.indexOf(3) !== -1) nhtml += ' note-selected';
		nhtml += '">Thu</span>';
		nhtml += '<span class="note-existing-day';
		if (note.weekdays.indexOf(4) !== -1) nhtml += ' note-selected';
		nhtml += '">Fri</span>';
		nhtml += '<span class="note-existing-day';
		if (note.weekdays.indexOf(5) !== -1) nhtml += ' note-selected';
		nhtml += '">Sat</span>';
		nhtml += '<span class="note-existing-day';
		if (note.weekdays.indexOf(6) !== -1) nhtml += ' note-selected';
		nhtml += '">Sun</span></div>';
		nhtml += '<div class="note-existing-times">';
		nhtml += '<span class="note-existing-time-title">';
		nhtml += '<span>From</span>';
		nhtml += '<span>To</span></span>';
		nhtml += '<span class="note-existing-time-from">';

		var timef = new XDate(note.time_from);
		var timet = new XDate(note.time_to);

		nhtml += '<span>' + timef.toString("HH:mm") + '</span>';
		nhtml += '<span>' + timet.toString("HH:mm") + '</span></span></div></div>';
	});

	$("#note-current").append(nhtml);

	$(".note-existing-wrap").tsort({attr: 'data-sort'});
}

function updateAllowedNoteParams(meter, type) {
	var output = "";
	var outPower = new Array('<optgroup id="power" label="Power">');
	var outVoltage = new Array('<optgroup id="voltage" label="Voltage">');
	var outCurrent = new Array('<optgroup id="current" label="Current">');
	var outOther = new Array('<optgroup id="other" label="Other">');

	if (type == "add") {

		var allowed = allowedParams[meter];

		if (typeof(allowed) === "undefined") {
			allowed = allowedParams.default;
		}

		$.each(allowed, function(key, val) {
			if (typeof(engParams[key]) !== "undefined" && val === 1) {
				output = '<option value="' + key + '">' + engParams[key].name + '</option>';

				switch(engParams[key].type) {
					case "Power":
						outPower.push(output);
						break;
					case "Voltage":
						outVoltage.push(output);
						break;
					case "Current":
						outCurrent.push(output);
						break;
					case "Other":
						if (key === 'freq') {
							outOther.push(output);
						}
						break;
				}

			}
		});

		$("#note-params").attr("data-placeholder", "Please select a parameter");
		output = outPower.sort() + outVoltage.sort() + outCurrent.sort() + outOther.sort();

	} else if (type == "remove") {
		$("#note-params").attr("data-placeholder", "Please select a metered circuit first");
		output += '<option disabled="disabled">Please select a metered circuit first</option>';
	}

	$("#note-params").html(output);
	$("#note-params").trigger("chosen:updated");
}

function logsDocReady() {
	//get the users logs
	$.ajax({
		url: '/api/notification/log',
		type: "GET",
		dataType: "JSON",
		success: function(result) {

			var lhtml = '<div class="log-row-title"><span class="log-desc-title">Description</span>';
			lhtml += '<span class="log-start-title">Time Occurred</span>';
			lhtml += '<span class="log-end-title">Time Resolved</span>';
			lhtml += '<span class="log-notify-o-title">Notified<br>Occurred?</span>';
			lhtml += '<span class="log-notify-r-title">Notified<br>Resolved?</span></div>';

			var alt = false;

			$.each(result, function(key, log) {
				var timeo = new XDate(log.datetime_occurred);
				timeo = timeo.toString("dd MMM yyyy<br>HH:mm:ss");

				if (log.datetime_resolved !== null) {
					var timer = new XDate(log.datetime_resolved);
					timer = timer.toString("dd MMM yyyy<br>HH:mm:ss");
				} else {
					var timer = "-";
				}


				lhtml += '<div class="log-row';
				if (alt) lhtml += ' log-row-alt';
				alt = !alt;
				lhtml+= '"><span class="log-desc">' + log.event_description + '</span>';
				lhtml += '<span class="log-start">' + timeo + '</span>';
				lhtml += '<span class="log-end">' + timer + '</span>';
				lhtml += '<span class="log-notify-o">';

                if (log.notified_occurred_email && log.notified_occurred_sms) {
                    lhtml += 'Email, SMS';
                } else if (log.notified_occurred_email && !log.notified_occurred_sms) {
                    lhtml += 'Email';
                } else if (!log.notified_occurred_email && log.notified_occurred_sms) {
                    lhtml += 'SMS';
                } else if (!log.notified_occurred_email && !log.notified_occurred_sms) {
                    lhtml += 'No';
                }

				lhtml += '</span>';
				lhtml += '<span class="log-notify-r">';

                if (log.notified_resolved_email && log.notified_resolved_sms) {
                    lhtml += 'Email, SMS';
                } else if (log.notified_resolved_email && !log.notified_resolved_sms) {
                    lhtml += 'Email';
                } else if (!log.notified_resolved_email && log.notified_resolved_sms) {
                    lhtml += 'SMS';
                } else if (!log.notified_resolved_email && !log.notified_resolved_sms) {
                    lhtml += 'No';
                }

				lhtml += '</span></div>';
			});

			$("#logs-list").append(lhtml);
		},
		error: function(o,s,e) {
			showMessage("err", defaultErrMsg);
		}
	});
}

//user is requesting new password
function requestPassDocReady() {
	//custom error messages
	$("#messages").on("custmessage", function(e, mess_type, mess_content) {
		$("#messages").removeClass();

		switch(mess_type) {
			case "info":
			default:
				$("#messages").addClass("mess-info");
				$("#messages").html(mess_content);
				break;
			case "err":
				$("#messages").addClass("mess-error");
				$("#messages").html(mess_content);
				break;
		}

		$("#messagewrap").animate({top: 0}, {duration: 1000, queue: false});
		$("html body").animate({scrollTop: 0}, {duration: 500, queue: false});

		setTimeout(function() {
			$("#messagewrap").animate({
				top: ($("#messagewrap").height() + 20) * -1
			}, {duration: 1000, queue: false});
		}, 10000);

	});

	$("#message-close").on("click", function() {
		$('#messagewrap').animate({
			top: (($('#messagewrap').height() + 10) * -1)
		}, 250);
	});

	$("#request-password-form").on("submit", function(e) {
		e.preventDefault();

		var username = $("#request-username").val();

		if (username.length !== 0) {
			$.ajax({
				url: '/user/change/password?username=' + username,
				type: 'GET',
				success: function(result) {
					showMessage("info", "A new password has been requested. Please check your email for further instructions.");
				},
				error: function() {
					showMessage("err", "Unable to request a new password. Please check your username and try again.");
				}
			});
		} else {
			showMessage("err", "Please provide your username.");
		}
	});
}

function resetPassDocReady() {
	//custom error messages
	$("#messages").on("custmessage", function(e, mess_type, mess_content) {
		$("#messages").removeClass();

		switch(mess_type) {
			case "info":
			default:
				$("#messages").addClass("mess-info");
				$("#messages").html(mess_content);
				break;
			case "err":
				$("#messages").addClass("mess-error");
				$("#messages").html(mess_content);
				break;
		}

		$("#messagewrap").animate({top: 0}, {duration: 1000, queue: false});
		$("html body").animate({scrollTop: 0}, {duration: 500, queue: false});

		setTimeout(function() {
			$("#messagewrap").animate({
				top: ($("#messagewrap").height() + 20) * -1
			}, {duration: 1000, queue: false});
		}, 10000);

	});

	$("#message-close").on("click", function() {
		$('#messagewrap').animate({
			top: (($('#messagewrap').height() + 10) * -1)
		}, 250);
	});

	$("#reset-password-form").on("submit", function(e) {
		e.preventDefault();

		var username = $("#reset-username").val();
		var pass1 = $("#reset-new-pass").val();
		var pass2 = $("#reset-confirm-pass").val();
		var haserr = false;
		var msg = "<p>There was a problem resetting your password. Please fix the following issues:</p><ul>";

		if (typeof username === "null" || username.length === 0) {
			haserr = true;
			msg += "<li>Please Enter Your Username</li>";
		}

		if (typeof pass1 === "null" || pass1 !== pass2) {
			haserr = true;
			msg += "<li>Passwords do not match. Please check your new password.</li>";
		}

		msg += '</ul>';

		if (!haserr) {
			Pace.start();

			var data = "code=" + code + "&password=" + pass1 + "&username=" + username;

			$.ajax({
				url: '/user/change/password/' + code,
				type: 'POST',
				data: data,
				success: function(result) {
					var mess = '<p>Your password has been sucessfully reset. You will be redirected to the login page in <span id="timer">5</span> seconds.';
					mess += 'Or you can <a href="/user/login">click here to login</a></p>';
					showMessage("info", mess);
					var counter = 5;

					var timer = setInterval(function() {
						$("#timer").text(counter--);
						if (counter === 0) {
							clearInterval(timer);
							document.location.href = "/user/login";
						}
					}, 1000);
				},
				error: function() {
					showMessage("err", 'Could not reset your password. Please try again');
				}
			});

			Pace.stop();

		} else {
			showMessage("err", msg);
		}
	});
}

function getUnixWet() {
    $.ajax({
        url: 'http://api.timezonedb.com/?zone=Europe/Dublin&format=json&key=9WLFN77EAQ65',
        type: 'GET',
        success: function(result) {
            return result.timestamp;
        },
        error: function() {
            return false;
        }
    });
}
