//used to store the JustGage and Highchart objects
var allgauges = {};
var widgettypes = {};
var syswlow = 25000; //used in determining reliability of THD and PF.
var highchartsbase = { //used as the base setup options for highcharts gauges.
	chart: {
		backgroundColor: "rgba(255,255,255,0.1)",
		borderWidth: 0,
		height: 175,
		marginTop: 20,
		plotBorderWidth: 0,
		plotShadow: false,
		width: 200,
		type: 'solidgauge'
	},
	exporting: {
		enabled: false
	},
	title: null,
	pane: {
		center: ['50%', '70%'],
		size: '100%',
		startAngle: -90,
		endAngle: 90,
		background: {
			backgroundColor: '#FFF',
			innerRadius: '60%',
			outerRadius: '100%',
			shape: 'arc'
		}
	},
	tooltip: {
		enabled: false
	},
	// the value axis
	yAxis: {
		allowDecimals: true,
		offset: 0,
		stops: [
			[0.1, '#55BF3B'], // green
			[0.5, '#DDDF0D'], // yellow
			[0.9, '#DF5353'] // red
		],
		lineWidth: 0,
		minorTickInterval: null,
		tickWidth: 0,
		title: {
			y: -50
		},
		labels: {
			y: 16,
			formatter: function () {
				return Highcharts.numberFormat(this.value, 0,'.','');
			}
		}
	},
	plotBackgroundColor: "rgba(255,255,255,0.1)",
	plotOptions: {
		solidgauge: {
			dataLabels: {
				y: -15,
				borderWidth: 0,
				useHTML: true,
				formatter: function () {
					return Highcharts.numberFormat(this.value, 1,'.','');
				}
			},
			overshoot: 0,
			wrap: false
		}
	}
};

var linebase = {
	chart: {
		renderTo: '',
		type: "spline",
		animation: false,
		zoomType: 'y',
		events: {
			load: function(){
				chart = this;
			}
		}
	},
	credits: false,
	lang: {
		noData: "No Data Exists for the Date Selected. Please Select a Valid Date."
	},
   legend: {
	enabled: true
   },
	noData: {
		style: {
			fontWeight: 'bold',
			fontSize: '15px',
			color: '#303030'
		}
	},
	subtitle: {
		text: null
	},
	title: {
		text: null
	},
	xAxis: [{
		type: "datetime",
		dateTimeLabelFormats: {
			day: '%H:%M',
			hour: '%H:%M'
		},
		labels: {
			step: 1,
			enabled: true,
			rotation: 330
		},
		minRange: 10
	}, {
		type: "datetime",
		dateTimeLabelFormats: {
			day: '%H:%M',
			hour: '%H:%M'
		},
		labels: {
			step: 2,
			enabled: true,
			rotation: 330
		},
		minRange: 10
	}],
	plotOptions: {
		series: {
			cursor: 'ns-resize',
			stickyTracking: false,
			pointInterval: 1800 * 1000,
			marker: {
				enabled: false,
				states: {
					hover: {
						enabled: true
					}
				}
			}
		},
		line: {
			//shadow: true
		},
		lineWidth: 1
	}
};

//rounds a number to one decimal place
function roundOne(num) {
	num = Math.round(num * 10);
	num = num / 10;

	return num;
}

//removes a widget from the allgauges object
function removeWidget(id) {
	if (allgauges[id]) {
		delete allgauges[id];
	}
}

//removes widget from the grid and from allgauges
function deleteWidget(el) {
	var wid = $(el).closest('li').attr('data-widget');
	var index = $('li[data-widget="' + wid + '"]').index();
	if (allgauges[wid]) {
		delete allgauges[wid];
	}
	gridster.remove_widget($(".gridster > ul > li").eq(index));
	getWidgetOrder();
}

//used to create the object that stores the available widgets and
//chart types for the site
function chkForData(data) {


	$.each(data, function(key, val) {
		var conId = val.controller_id;
		var meterId = val.meter_id;
		var combo = conId + "-" + meterId;

		//power-gauge widget
		if (val.min_kw !== null &&
			val.max_kw !== null &&
			val.min_kva !== null &&
			val.max_kva !== null &&
			val.sys_va !== null &&
			val.sys_var !== null &&
			val.sys_w !== null) {

			widgettypes[combo] = {
				power: "gauge"
			};

		}

		//power-line widget
		if (val.sys_w !== null) {
    		if (typeof(widgettypes[combo]) !== "undefined") {
        	    widgettypes[combo]["power"] += ",line";
    		} else {
        		widgettypes[combo] = {
            		power: "line"
        		};
    		}
		}


		//voltage-gauge and line widget
		if (val.phase_1_volts !== null &&
			val.phase_2_volts !== null &&
			val.phase_3_volts !== null) {

			widgettypes[combo]["voltage"] = "gauge,line";
		}

		//current-gauge and line widget
		if (val.phase_1_amps !== null &&
			val.phase_2_amps !== null &&
			val.phase_3_amps !== null &&
            val.neutral_amps !== null) {

			widgettypes[combo]["current"] = "gauge,line";
		}

		//thd-gauge and line widget
		if (val.i1_thd !== null &&
			val.i2_thd !== null &&
			val.i3_thd !== null &&
			val.v1_thd !== null &&
			val.v2_thd !== null &&
			val.v3_thd !== null) {

			widgettypes[combo]["thd"] = "gauge,line";
		}

		//frequency-gauge and line widget
		if (val.freq !== null) {
			widgettypes[combo]["frequency"] = "gauge,line";
		}

	});
}

function chkSign(val){
	if (val < 0) {
		return val * -1;
	} else {
		return val;
	}
}

function parseData(conId, meterId, wtype) {
	var rawData;
	var parsedData = {};

	//get the data for the controller & meter id combo
	$.each(data_series, function(key, val) {
		if (val.controller_id == conId && val.meter_id == meterId) {
			rawData = val;
		}
	});

	switch(wtype) {
		case "power-gauge":
			if (rawData.sys_w < 0) {
				parsedData["kw_max"] = Math.abs(Math.max(rawData.min_kw));
				parsedData["kva_max"]= Math.abs(Math.max(rawData.min_kva));
				parsedData["kvar_max"] = parsedData["kw_max"]/3;
				parsedData["import"] = false;
			} else {
				parsedData["kw_max"] = Math.abs(Math.max(rawData.max_kw));
				parsedData["kva_max"]= Math.abs(Math.max(rawData.max_kva));
				parsedData["kvar_max"] = parsedData["kw_max"]/3;
				parsedData["import"] = true;
			}

			parsedData["kw_val"] = chkSign(rawData.sys_w/1000);
			parsedData["kva_val"] = chkSign(rawData.sys_va/1000);
			parsedData["kvar_val"] = chkSign(rawData.sys_var/1000);
			break;

		case "power-line":
			parsedData["min_kw"] = rawData.min_kw;
			parsedData["max_kw"] = rawData.max_kw;
			break;

		case "voltage-gauge":
			parsedData["p1_val"] = rawData.phase_1_volts;
			parsedData["p2_val"] = rawData.phase_2_volts;
			parsedData["p3_val"] = rawData.phase_3_volts;
			parsedData["avg_val"] = (parsedData["p1_val"] + parsedData["p2_val"] + parsedData["p3_val"]) / 3;
			break;

		case "voltage-line":
			case "voltage-gauge":
			parsedData["p1_val"] = rawData.phase_1_volts;
			parsedData["p2_val"] = rawData.phase_2_volts;
			parsedData["p3_val"] = rawData.phase_3_volts;
			break;

		case "current-gauge":
			if (rawData.sys_w < 0) {
				parsedData["sum_max"] = Math.abs(Math.max(rawData.min_kva)/.23);
			} else {
				parsedData["sum_max"] = Math.abs(Math.max(rawData.max_kva)/.23);
			}

			parsedData["p1_val"] = rawData.phase_1_amps;
			parsedData["p2_val"] = rawData.phase_2_amps;
			parsedData["p3_val"] = rawData.phase_3_amps;
            parsedData["na_val"] = rawData.neutral_amps;
			parsedData["sum_val"] = rawData.phase_1_amps + rawData.phase_2_amps + rawData.phase_3_amps;
			parsedData["p1_max"] = parsedData["sum_max"]/3;
			parsedData["p2_max"] = parsedData["sum_max"]/3;
			parsedData["p3_max"] = parsedData["sum_max"]/3;
			break;

		case "current-line":
			parsedData["p1_val"] = rawData.phase_1_amps;
			parsedData["p2_val"] = rawData.phase_2_amps;
			parsedData["p3_val"] = rawData.phase_3_amps;
            parsedData["na_val"] = rawData.neutral_amps;
			break;

		case "thd-gauge":
			parsedData["v1_val"] = rawData.v1_thd * 100;
			parsedData["v2_val"] = rawData.v2_thd * 100;
			parsedData["v3_val"] = rawData.v3_thd * 100;
			parsedData["i1_val"] = rawData.i1_thd * 100;
			parsedData["i2_val"] = rawData.i2_thd * 100;
			parsedData["i3_val"] = rawData.i3_thd * 100;
			parsedData["sum_max"] = rawData.sum_max;
			parsedData["sum_val"] = rawData.phase_1_amps + rawData.phase_2_amps + rawData.phase_3_amps;
			parsedData["v_avg"] = (rawData.phase_1_volts + rawData.phase_2_volts + rawData.phase_3_volts) / 3;
			break;

		case "thd-line":
			parsedData["v1_val"] = rawData.v1_thd * 100;
			parsedData["v2_val"] = rawData.v2_thd * 100;
			parsedData["v3_val"] = rawData.v3_thd * 100;
			parsedData["i1_val"] = rawData.i1_thd * 100;
			parsedData["i2_val"] = rawData.i2_thd * 100;
			parsedData["i3_val"] = rawData.i3_thd * 100;
			parsedData["sum_max"] = rawData.sum_max;
			parsedData["sum_val"] = rawData.phase_1_amps + rawData.phase_2_amps + rawData.phase_3_amps;
			parsedData["v_avg"] = (rawData.phase_1_volts + rawData.phase_2_volts + rawData.phase_3_volts) / 3;
			break;

		case "frequency-line":
			parsedData["freq"] = rawData.freq;
			break;
	}

	return parsedData;
}

function createWidgetObj(conId, meterId, desc, ctype, cchart, grid) {

	if (conId && meterId && desc && ctype && cchart) {
		var data;

		switch(ctype.toLowerCase() + "-" + cchart.toLowerCase()) {
			case "power-gauge":
				data = parseData(conId, meterId, "power-gauge");

				var setup = {
					chartoptions: highchartsbase,
					title: desc + ": Power",
					kw_min: 0,
					kw_max: data.kw_max,
					kw_val: data.kw_val,
					kva_min: 0,
					kva_max: data.kva_max,
					kva_val: data.kva_val,
					kvar_min: 0,
					kvar_max: data.kvar_max,
					kvar_val: data.kvar_val,
					conId: conId,
					meterId: meterId,
					import: data.import,
					gridster: grid
				};
				break;

			case "voltage-gauge":
				data = parseData(conId, meterId, "voltage-gauge");
				var setup = {
					chartoptions: highchartsbase,
					title: desc + ": Voltage",
					avg_min: 0,
					avg_max: 255,
					avg_val: data.avg_val,
					p1_min: 0,
					p1_max: 250,
					p1_val: data.p1_val,
					p2_min: 0,
					p2_max: 250,
					p2_val: data.p2_val,
					p3_min: 0,
					p3_max: 250,
					p3_val: data.p3_val,
					conId: conId,
					meterId: meterId,
					gridster: grid
				};

				break;

			case "voltage-line":
				data = parseData(conId, meterId, "voltage-line");
				var setup = {
					chartoptions: linebase,
					title: desc + ": Voltage",
					p1_val: data.p1_val,
					p2_val: data.p2_val,
					p3_val: data.p3_val,
					conId: conId,
					meterId: meterId,
					gridster: grid
				};

				break;

			case "current-gauge":
				data = parseData(conId, meterId, "current-gauge");

				var setup = {
					chartoptions: highchartsbase,
					title: desc + ": Current",
					sum_min: 0,
					sum_max: data.sum_max,
					sum_val: data.sum_val,
					p1_min: 0,
					p1_max: data.p1_max,
					p1_val: data.p1_val,
					p2_min: 0,
					p2_max: data.p2_max,
					p2_val: data.p2_val,
					p3_min: 0,
					p3_max: data.p3_max,
					p3_val: data.p3_val,
                    na_val: data.na_val,
					conId: conId,
					meterId: meterId,
					gridster: grid
				};

				break;

			case "current-line":
				data = parseData(conId, meterId, "current-line");

				var setup = {
					chartoptions: linebase,
					title: desc + ": Current",
					p1_val: data.p1_val,
					p2_val: data.p2_val,
					p3_val: data.p3_val,
					na_val: data.na_val,
					conId: conId,
					meterId: meterId,
					gridster: grid
				};

				break;

			case "thd-gauge":
				data = parseData(conId, meterId, "thd-gauge");
				var setup = {
					chartoptions: highchartsbase,
					title: desc + ": Total Harmonic Distortion",
					v1_min: 0,
					v1_max: 10,
					v1_val: data.v1_val,
					v2_min: 0,
					v2_max: 10,
					v2_val: data.v2_val,
					v3_min: 0,
					v3_max: 10,
					v3_val: data.v3_val,
					i1_min: 0,
					i1_max: 60,
					i1_val: data.i1_val,
					i2_min: 0,
					i2_max: 60,
					i2_val: data.i2_val,
					i3_min: 0,
					i3_max: 60,
					i3_val: data.i3_val,
					sum_val: data.sum_val,
					sum_max: data.sum_max,
					v_avg: data.v_avg,
					conId: conId,
					meterId: meterId,
					gridster: grid
				};

				break;

			case "thd-line":
				data = parseData(conId, meterId, "thd-line");
				var setup = {
					chartoptions: linebase,
					title: desc + ": THD",
					conId: conId,
					meterId: meterId,
					desc: desc,
					v1_val: data.v1_val,
					v2_val: data.v2_val,
					v3_val: data.v3_val,
					i1_val: data.i1_val,
					i2_val: data.i2_val,
					i3_val: data.i3_val,
					sum_val: data.sum_val,
					sum_max: data.sum_max,
					v_avg: data.v_avg,
					gridster: grid
				};
				break;

			case "power-line":
				data = parseData(conId, meterId, "power-line");
				var setup = {
					chart_options: linebase,
					title: desc + ": Power",
					conId: conId,
					meterId: meterId,
					desc: desc,
					min_kw: data.min_kw,
					max_kw: data.max_kw,
					gridster: grid
				}

				break;

			case "frequency-gauge":
				var setup = {
					chart_options: {
						chart: {
							type: 'gauge',
							renderTo: '',
							plotBackgroundColor: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#303030'],
									[1, '#EBEBEB']
								]
							},
							plotBackgroundImage: null,
							height: 200,
						},
						credits: {
							enabled: false
						},
						title: {
							text: 'Hz'
						},
						pane: [{
							startAngle: -45,
							endAngle: 45,
							background: null,
							center: ['50%', '150%'],
							size: 300,
						}],
						yAxis: [{
							min: 47.0,
							max: 53.01,
							minorTickInterval: 0.2,
							minorTickPosition: 'outside',
							minorTickLength: 10,
							minorTickColor: '#fff',
							tickPositions: [47.0, 48.0, 49, 50, 51.0, 52.0, 53.0],
							tickPosition: 'inside',
							tickLength: 15,
							tickColor: '#fff',
							labels: {
								rotation: 'auto',
								distance: 20,
									style: {
									color: '#fff',
									size: '120%',
									fontWeight: 'bold'
							},
							step: 1
						},
						plotBands: [{
							from: 47.0,
							to: 48.0,
							color: {
								linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
								stops: [
									[0, '#FFCC00'],
									[1, '#FF3300']
								]
							},
							thickness: '20%'
						}, {
							from: 48.0,
							to: 49.8,
							color: {
								linearGradient: { x1: 1, y1: 0, x2: 0, y2: 0 },
								stops: [
									[0, '#6CE843'],
									[1, '#FFCC00']
								]
							},
							thickness: '20%'
						}, {
							from: 49.8,
							to: 50.2,
							color: '#6CE843',
							thickness: '20%'
						}, {
							from: 50.2,
							to: 52.0,
							color: {
								linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
								stops: [
									[0, '#6CE843'], // green
									[1, '#FFCC00'] // amber
								]
							},
							thickness: '20%'
						},{
							pane: 0,
							from: 52.0,
							to: 53.0,
							color: {
								linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
								stops: [
									[0, '#FFCC00'], // amber
									[1, '#FF3300']  // red
								]
							},
							thickness: '20%'
						}],
						}, {
							min: 47.0,
							max: 50.0,
							tickPosition: 'outside',
							lineColor:  '#666',
							lineWidth: 2,
							minorTickPosition: 'outside',
							tickColor:  '#666',
							minorTickColor:  '#666',
							tickLength: 15,
							minorTickLength: 10,
							labels: {
								enabled: false
							},
							offset: -30,
							endOnTick: true
						}],
						plotOptions: {
							gauge: {
								dataLabels: {
									enabled: true
								},
								dial: {
									radius: '100%',
									backgroundColor: 'red'
								}
							}
						},
						series: [{
							data: [50],
							yAxis: 0
						}],
						lang: {
							noData: null
						},
						exporting: {
							enabled: false
						},
					},
					title: desc + ": Frequency",
					conId: conId,
					meterId: meterId,
					desc: desc,
					gridster: grid
				};

				break;

			case "frequency-line":
				data = parseData(conId, meterId, "frequency-line");

				var setup = {
					chartoptions: linebase,
					title: desc + ": Frequency",
					freq: data.freq,
					conId: conId,
					meterId: meterId,
					gridster: grid
				};
				break;

			default:
				console.log('Unrecognised chart type: The chart type you requested is not yet available. Please choose another.');
				break;
		}

		var wclass = ctype.toLowerCase() + "-" + cchart.toLowerCase();
		var aWidget = new eWidget(wclass, setup);

	} else {
		console.log('A Controller ID, Meter ID, Widget Type, and Chart Type are required to add a new widget');
		console.log('The following parameters were received:');
		console.log('Controller ID: ', conId);
		console.log('Meter ID: ', meterId);
		console.log('Widget Type: ', ctype);
		console.log('Chart Type: ', cchart);
	}

	return conId + "-" + meterId + "-" + ctype + "-" + cchart;

}

//refresh all widgets
function wrefreshall() {

	//loop through all gauges and update each gauge in turn
	$.each(allgauges, function(widgetid, wsettings) {
		var wdetails = widgetid.split("-");
		var wtype = wdetails[0];
		var wchart = wdetails[1];
		var conId = wdetails[2];
		var meterId = wdetails[3];
		var newval = 0;
		var wid = conId + "-" + meterId;

		switch (wtype + "-" + wchart) {
			case "power-gauge":
				var sysw = getData(conId, meterId, "sys_w");
				var kwmaxval, kvamaxval, kvarmaxval, kwval, kvaval, kvarval;
				var wid = conId + "-" + meterId;
				var syswSign = localStorage.getItem("psyswSign-" + wid);

				//get gauge max vals based on sign of sys_w
				if (sysw < 0) {
					kwmaxval = getData(conId, meterId, "min_kw");
					kwmaxval = Math.abs(Math.max(kwmaxval));

					kvamaxval = getData(conId, meterId, "min_kva");
					kvamaxval = Math.abs(Math.max(kvamaxval));

					kvarmaxval = kwmaxval / 3;

					//check if sysw has changed sign and update the extremes if reqd

					if (syswSign == "pos") {
						//update the powerimg depending on sign of sysw
						$("#power-gauge-" + wid + " .powerimg").css("backgroundPosition", "0");
					} else {
						$("#power-gauge-" + wid + " .powerimg").css("backgroundPosition", "200px");
					}

						//KW gauge
						allgauges[widgetid]["kw-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(kwmaxval);
						allgauges[widgetid]["kw-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(kwmaxval);
						allgauges[widgetid]["kw-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(kwmaxval));

						//KVA gauge
						allgauges[widgetid]["kva-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(kvamaxval);
						allgauges[widgetid]["kva-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(kvamaxval);
						allgauges[widgetid]["kva-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(kvamaxval));

						//KVAR gauge
						allgauges[widgetid]["kvar-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(kvarmaxval);
						allgauges[widgetid]["kvar-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(kvarmaxval);
						allgauges[widgetid]["kvar-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(kvarmaxval));
					// }

				} else {
					kwmaxval = getData(conId, meterId, "max_kw");
					kwmaxval = Math.abs(Math.max(kwmaxval));

					kvamaxval = getData(conId, meterId, "max_kva");
					kvamaxval = Math.abs(Math.max(kvamaxval));

					kvarmaxval = kwmaxval / 3;

					if (syswSign == "neg") {
						//update the powerimg depending on sign of sysw
						$("#power-gauge-" + conId + "-" + meterId + " .powerimg").css("backgroundPosition", "200px");
					} else {
						$("#power-gauge-" + conId + "-" + meterId + " .powerimg").css("backgroundPosition", "0");
					}

						//KW gauge
						allgauges[widgetid]["kw-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(kwmaxval);
						allgauges[widgetid]["kw-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(kwmaxval);
						allgauges[widgetid]["kw-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(kwmaxval));

						//KVA gauge
						allgauges[widgetid]["kva-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(kvamaxval);
						allgauges[widgetid]["kva-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(kvamaxval);
						allgauges[widgetid]["kva-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(kvamaxval));

						//KVAR gauge
						allgauges[widgetid]["kvar-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(kvarmaxval);
						allgauges[widgetid]["kvar-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(kvarmaxval);
						allgauges[widgetid]["kvar-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(kvarmaxval));
					// }

				}

				//get gauge vals
				kwval = getData(conId, meterId, "sys_w");
				kwval = chkSign(kwval/1000);

				kvaval = getData(conId, meterId, "sys_va");
				kvaval = chkSign(kvaval/1000);

				kvarval = getData(conId, meterId, "sys_var");
				kvarval = chkSign(kvarval/1000);

				//update the gauges
				allgauges[widgetid]["kw-gauge-" + wid].series[0].points[0].update(roundOne(kwval));
				allgauges[widgetid]["kva-gauge-" + wid].series[0].points[0].update(roundOne(kvaval));
				allgauges[widgetid]["kvar-gauge-" + wid].series[0].points[0].update(roundOne(kvarval));

				break;

			case "voltage-gauge":
				var p1val, p2val, p3val, avgval;

				//get gauge vals
				p1val = getData(conId, meterId, "phase_1_volts");

				p2val = getData(conId, meterId, "phase_2_volts");

				p3val = getData(conId, meterId, "phase_3_volts");

				avgval = (p1val + p2val + p3val) / 3;

				//update the gauges
				allgauges[widgetid]["p1-gauge-" + wid].series[0].points[0].update(roundOne(p1val));
				allgauges[widgetid]["p2-gauge-" + wid].series[0].points[0].update(roundOne(p2val));
				allgauges[widgetid]["p3-gauge-" + wid].series[0].points[0].update(roundOne(p3val));
				allgauges[widgetid]["avg-gauge-" + wid].series[0].points[0].update(roundOne(avgval));

				break;

			case "voltage-line":
				var y1 = parseFloat(getData(conId, meterId, "phase_1_volts").toFixed(1));
				var y2 = parseFloat(getData(conId, meterId, "phase_2_volts").toFixed(1));
				var y3 = parseFloat(getData(conId, meterId, "phase_3_volts").toFixed(1));
				var x = parseInt(getData(conId, meterId, "time_received_wet_js"));

				allgauges[widgetid]["voltage-highchart-" + wid].series[0].addPoint([x,y1], true, true);
				allgauges[widgetid]["voltage-highchart-" + wid].series[1].addPoint([x,y2], true, true);
				allgauges[widgetid]["voltage-highchart-" + wid].series[2].addPoint([x,y3], true, true);

				break;

			case "current-gauge":
				var p1val, p2val, p3val, naval, sumval, summax, pmax, sysw;
				var syswSign = localStorage.getItem("csyswSign-" + wid);

				sysw = getData(conId, meterId, "sys_w");

				//get gauge vals
				p1val = getData(conId, meterId, "phase_1_amps");
				p2val = getData(conId, meterId, "phase_2_amps");
				p3val = getData(conId, meterId, "phase_3_amps");
                naval = getData(conId, meterId, "neutral_amps");

				sumval = parseInt(p1val) + parseInt(p2val) + parseInt(p3val);

				//get gauge max vals
				pmax = summax / 3;
				if (pmax == 0) pmax = 100;

				if (sysw < 0) {
					summax = getData(conId, meterId, "min_kva");
					summax = Math.abs(Math.max(summax)/.23);

					if (syswSign == "pos") {

						//SUM gauge
						allgauges[widgetid]["sum-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(summax);
						allgauges[widgetid]["kw-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(summax);
						allgauges[widgetid]["kw-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(summax));

						//I1 gauge
						allgauges[widgetid]["i1-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(pmax);
						allgauges[widgetid]["i1-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(pmax);
						allgauges[widgetid]["i1-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(pmax));

						//I2 gauge
						allgauges[widgetid]["i2-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(pmax);
						allgauges[widgetid]["i2-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(pmax);
						allgauges[widgetid]["i2-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(pmax));

						//I2 gauge
						allgauges[widgetid]["i3-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(pmax);
						allgauges[widgetid]["i3-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(pmax);
						allgauges[widgetid]["i3-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(pmax));

                        //Neutral Amps gauge
                        allgauges[widgetid]["na-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(pmax);
						allgauges[widgetid]["na-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(pmax);
						allgauges[widgetid]["na-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(pmax));
					}

				} else {

					summax = getData(conId, meterId, "max_kva");
					summax = Math.abs(Math.max(summax)/.23);

					if (syswSign == "neg") {

						//SUM gauge
						allgauges[widgetid]["sum-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(summax);
						allgauges[widgetid]["kw-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(summax);
						allgauges[widgetid]["kw-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(summax));

						//I1 gauge
						allgauges[widgetid]["i1-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(pmax);
						allgauges[widgetid]["i1-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(pmax);
						allgauges[widgetid]["i1-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(pmax));

						//I2 gauge
						allgauges[widgetid]["i2-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(pmax);
						allgauges[widgetid]["i2-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(pmax);
						allgauges[widgetid]["i2-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(pmax));

						//I2 gauge
						allgauges[widgetid]["i3-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(pmax);
						allgauges[widgetid]["i3-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(pmax);
						allgauges[widgetid]["i3-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(pmax));

                        //Neutral Amps gauge
                        allgauges[widgetid]["na-gauge-" + wid].yAxis[0].options.tickInterval = parseInt(pmax);
						allgauges[widgetid]["na-gauge-" + wid].yAxis[0].options.tickPixelInterval = parseInt(pmax);
						allgauges[widgetid]["na-gauge-" + wid].yAxis[0].setExtremes(0, parseInt(pmax));
					}

				}

				//update gauge vals
				allgauges[widgetid]["i1-gauge-" + wid].series[0].points[0].update(roundOne(p1val));
				allgauges[widgetid]["i2-gauge-" + wid].series[0].points[0].update(roundOne(p2val));
				allgauges[widgetid]["i3-gauge-" + wid].series[0].points[0].update(roundOne(p3val));
                allgauges[widgetid]["na-gauge-" + wid].series[0].points[0].update(roundOne(naval));
				allgauges[widgetid]["sum-gauge-" + wid].series[0].points[0].update(roundOne(sumval));

				break;

			case "current-line":
				var y1 = parseFloat(getData(conId, meterId, "phase_1_amps").toFixed(1));
				var y2 = parseFloat(getData(conId, meterId, "phase_2_amps").toFixed(1));
				var y3 = parseFloat(getData(conId, meterId, "phase_3_amps").toFixed(1));
				var y4 = parseFloat(getData(conId, meterId, "neutral_amps").toFixed(1));
				var x = parseInt(getData(conId, meterId, "time_received_wet_js"));

				allgauges[widgetid]["current-highchart-" + wid].series[0].addPoint([x,y1], true, true);
				allgauges[widgetid]["current-highchart-" + wid].series[1].addPoint([x,y2], true, true);
				allgauges[widgetid]["current-highchart-" + wid].series[2].addPoint([x,y3], true, true);
				allgauges[widgetid]["current-highchart-" + wid].series[3].addPoint([x,y4], true, true);
				break;

			case "thd-gauge":
				var v1val, v2val, v3val, i1val, i2val, i3val, vmsg, amsg;

				//get gauge vals
				v1val = getData(conId, meterId, "v1_thd") * 100;
				v2val = getData(conId, meterId, "v2_thd") * 100;
				v3val = getData(conId, meterId, "v3_thd") * 100;
				i1val = getData(conId, meterId, "i1_thd") * 100;
				i2val = getData(conId, meterId, "i2_thd") * 100;
				i3val = getData(conId, meterId, "i3_thd") * 100;

				//create messages
				if (this.v1_val > 0 || this.v2_val > 0 || this.v3_val > 0) {
					vmsg = "Normal situation, no risk of malfunctions";
				}

				if (this.v1_val > 10 || this.v2_val > 10 || this.v3_val > 10) {
					vmsg = "Significant harmonic pollution, some malfunctions are possible";
				}

				if (this.v1_val > 50 || this.v2_val > 50 || this.v3_val > 50) {
					vmsg = "Major harmonic pollution, malfunctions are probable.";
					vmsg += " In-depth analysis and the installation of attenuation devices are required";
				}

				if (this.i1_val > 0 || this.i2_val > 0 || this.i3_val > 0) {
					amsg = "Normal situation, no risk of malfunctions";
				}

				if (this.i1_val > 10 || this.i2_val > 10 || this.i3_val > 10) {
					amsg = "Significant harmonic pollution, some malfunctions are possible";
				}
				if (this.i1_val > 50 || this.i2_val > 50 || this.i3_val > 50) {
					amsg = "Major harmonic pollution, malfunctions are probable.";
					amsg += " In-depth analysis and the installation of attenuation devices are required";
				}

				//check if we need to hide the gauges
				//voltage gauges are hidden if the avg voltage is < 25 V
				//current gauges are hidden if the current total A is < 5% of max.
				var voltsCSS = $("#thd-" + wid +  " .thd-volts-ignore").css("display").toLowerCase();
				var showVolts = checkVolts(conId, meterId);

				if (showVolts) {
					if (voltsCSS === "block") {
						$("#thd-" + wid +  " .thd-volts-ignore").css("display", "none");
						$("#thd-" + wid +  " .thd-volts-wrap").css("display", "block");
					}
				} else {
					if (voltsCSS === "none") {
						$("#thd-" + wid +  " .thd-volts-ignore").css("display", "block");
						$("#thd-" + wid +  " .thd-volts-wrap").css("display", "none");
					}
				}

				var ampsCSS = $("#thd-" + wid +  " .thd-amps-ignore").css("display").toLowerCase();
				var showAmps = checkAmps(conId, meterId);

				if (showAmps) {
					if (ampsCSS === "block") {
						$("#thd-" + wid +  " .thd-amps-ignore").css("display", "none");
						$("#thd-" + wid +  " .thd-amps-wrap").css("display", "block");
					}
				} else {
					if (ampsCSS === "none") {
						$("#thd-" + wid +  " .thd-amps-ignore").css("display", "block");
						$("#thd-" + wid +  " .thd-amps-wrap").css("display", "none");
					}
				}

				//update gauges
				allgauges[widgetid]["v1-gauge-" + wid].series[0].points[0].update(roundOne(v1val));
				allgauges[widgetid]["v2-gauge-" + wid].series[0].points[0].update(roundOne(v2val));
				allgauges[widgetid]["v3-gauge-" + wid].series[0].points[0].update(roundOne(v3val));
				allgauges[widgetid]["i1-gauge-" + wid].series[0].points[0].update(roundOne(i1val));
				allgauges[widgetid]["i2-gauge-" + wid].series[0].points[0].update(roundOne(i2val));
				allgauges[widgetid]["i3-gauge-" + wid].series[0].points[0].update(roundOne(i3val));

				//update messages
				$("#thd-" + wid + " .thd-volts-msg").html(vmsg);
				$("#thd-" + wid + " .thd-amps-msg").html(amsg);

				break;

			case "thd-line":
				var y1 = parseFloat(getData(conId, meterId, "v1_thd").toFixed(3) * 100);
				var y2 = parseFloat(getData(conId, meterId, "v2_thd").toFixed(3) * 100);
				var y3 = parseFloat(getData(conId, meterId, "v3_thd").toFixed(3) * 100);
				var y4 = parseFloat(getData(conId, meterId, "i1_thd").toFixed(3) * 100);
				var y5 = parseFloat(getData(conId, meterId, "i2_thd").toFixed(3) * 100);
				var y6 = parseFloat(getData(conId, meterId, "i3_thd").toFixed(3) * 100);
				var x = parseInt(getData(conId, meterId, "time_received_wet_js"));

				//check reliability and show message if required.
				var showVolts = checkVolts(conId, meterId);
				var showAmps = checkAmps(conId, meterId);
				var css = $("#thd-line-wrap-" + wid + " .thd-line-ignore").css("display").toLowerCase();

				if (!showVolts && !showAmps) var igType = "Voltage and Current";
				if (showVolts && !showAmps) var igType = "Current";
				if (!showVolts && showAmps) var igType = "Voltage";

				if (!showVolts || !showAmps) {
					if (css === "none") {
						$("#thd-type-" + wid).text(igType);
						$("#thd-line-wrap-" + wid + " thd-line-ignore").css("display", "block");
						$("#thd-l-" + wid).css("height", "300px");
// 						allgauges["thd-line-" + wid]["thd-highchart-" + wid].chart.reflow();
					}
				} else {
					if (css === "block") {
						$("#thd-line-wrap-" + wid + " thd-line-ignore").css("display", "none");
						$("#thd-l-" + wid).css("height", "auto");
// 						allgauges["thd-line-" + wid]["thd-highchart-" + wid].chart.reflow(); console.log(wid);
					}
				}

				allgauges[widgetid]["thd-highchart-" + wid].series[0].addPoint([x,y1], true, true);
				allgauges[widgetid]["thd-highchart-" + wid].series[1].addPoint([x,y2], true, true);
				allgauges[widgetid]["thd-highchart-" + wid].series[2].addPoint([x,y3], true, true);
				allgauges[widgetid]["thd-highchart-" + wid].series[3].addPoint([x,y4], true, true);
				allgauges[widgetid]["thd-highchart-" + wid].series[4].addPoint([x,y5], true, true);
				allgauges[widgetid]["thd-highchart-" + wid].series[5].addPoint([x,y6], true, true);

				break;

			case "power-line":

				var y = parseInt(getData(conId, meterId, "sys_w"));
				var x = parseInt(getData(conId, meterId, "time_received_wet_js"));

				allgauges[widgetid]["power-highchart-" + wid].series[0].addPoint([x,y], true, true);



				break;

			case "frequency-gauge":
				var freq = getData(conId, meterId, "freq");
				var pf = getData(conId, meterId, "pf");
				var sysw = getData(conId, meterId, "sys_w");

				allgauges['frequency-gauge-' + wid]['frequency-highchart-' + wid].series[0].points[0].update(freq, true);
				$("#frequency-" + wid + " .freq_value").html(freq + " Hz");


				//check if pf is reliable
				var showFreq = checkAmps(conId, meterId);

				if (!showFreq) {
					$("#frequency-" + wid + " .freq_pf").html("Not Reliable");
				} else {
					$("#frequency-" + wid + " .freq_pf").html(pf);
				}

				break;

			case "frequency-line":
				var y = getData(conId, meterId, "freq");
				var x = parseInt(getData(conId, meterId, "time_received_wet_js"));

				allgauges[widgetid]["frequency-highchart-" + wid].series[0].addPoint([x,y], true, true);

				break;
		}

	});
}

//gets the data from data_series
function getData(conId, meterId, param) {
	if (conId && meterId && param) {
		for (var i=0; i<data_series.length; i++) {
			if (data_series[i].controller_id == conId && data_series[i].meter_id == meterId) {
				var param_val = data_series[i][param];
			}
		}
		return param_val;
	} else {
		console.log("Need a Controller ID, Meter ID and a parameter to retrieve");
		return false;
	}
}

//refresh single widget
function wrefresh(id, val, max) {
	console.log("refresh called for " + id + " with value " + val + " and new max " + max);

	max ? allgauges[id].refresh(val, max) : allgauges[id].refresh(val);
}

function eWidget(type, args) {
	var ltype = type.toLowerCase();

	switch (ltype) {
		case "power-gauge":
			powerGauge(args);
			break;
		case "voltage-gauge":
			voltageGauge(args);
			break;
		case "voltage-line":
			voltageLine(args);
			break;
		case "current-gauge":
			currentGauge(args);
			break;
		case "current-line":
			currentLine(args);
			break;
		case "thd-gauge":
			thdGauge(args);
			break;
		case "thd-line":
			thdLine(args);
			break;
		case "power-line":
			powerLine(args);
			break;
		case "frequency-gauge":
			frequencyGauge(args);
			break;
		case "frequency-line":
			frequencyLine(args);
			break;
		default:
			console.log('Unrecognised widget type. Try one of "power-gauge", "power-chart", "voltage-gauge", "current-gauge", "thd-gauge", or "frequency-gauge".');
			break;
	}

	//power gauge widget
	function powerGauge(args) {
		//can do nothing if we do not have a controller id and meter id
		if (args.conId && args.meterId) {
			//parameters for power gauge
			this.meterId = args.meterId;
			this.conId = args.conId;
			//unique id for each site/meter
			var wid = this.conId + '-' + this.meterId;

			//check if we have a widget with the same id already setup
// 			var cur_widgets = $("#widgetlist").sDashboard("getDashboardData");
			var okaytocreate = true;

/*
			$.each(cur_widgets, function(key, val) {
				if (val.widgetId == "power-gauge-" + wid) {
					okaytocreate = false;
				}
			});
*/

			if (okaytocreate) {

				//setup defaults
				args.title ? this.title= args.title: this.title = "Power Widget: " + this.conId + "-" + this.meterId;
				args.kw_min ? this.kw_min = args.kw_min : this.kw_min = 0;
				args.kw_max ? this.kw_max = args.kw_max : this.kw_max = 100;
				args.kw_val ? this.kw_val = parseFloat(args.kw_val).toFixed(1) : this.kw_val = this.kw_max/2;
				args.kva_min ? this.kva_min = args.kva_min : this.kva_min = 0;
				args.kva_max ? this.kva_max = args.kva_max : this.kva_max = 100;
				args.kva_val ? this.kva_val = parseFloat(args.kva_val).toFixed(1) : this.kva_val = this.kva_max/2;
				args.kvar_min ? this.kvar_min = args.kvar_min : this.kvar_min = 0;
				args.kvar_max ? this.kvar_max = args.kvar_max : this.kva_max = 100;
				args.kvar_val ? this.kvar_val = parseFloat(args.kvar_val).toFixed(1) : this.kvar_val = this.kvar_max/2;
				this.import = args.import

				//structure
				var $pgHtml = '<div id="power-gauge-' + wid + '" class="ewidget wContain">';
				$pgHtml += '<div id="powerimg" class="powerimg"></div>';
				$pgHtml += '<div id="kw-' + wid + '" class="kwgauge"></div>';
				$pgHtml += '<div id="kva-' + wid + '" class="kvagauge"></div>';
				$pgHtml += '<div id="kvar-' + wid + '" class="kvargauge"></div>';
				$pgHtml += '</div>';

				//structure for gridster
				var $gpgHtml = '<li data-widget="power-gauge-' + wid + '"><div class="widget-title">' + this.title + '<span class="widget-close" onclick="deleteWidget(this);">&#xf2d7;</span></div>' + $pgHtml + '</li>';
				//add to gridster
				gridster = $('.gridster > ul').gridster().data('gridster');
				//gridster.options.widget_base_dimensions = [220,220];
				gridster.add_widget($gpgHtml,args.gridster.size_x,args.gridster.size_y,args.gridster.col, args.gridster.row);
/*

				$("body").append($pgHtml);

				//have sDashboard add the widget to the dashboard
				$("#widgetlist").sDashboard('addWidget', {
					widgetTitle: this.title,
					widgetId: "power-gauge-" + wid,
					widgetContent: $("#power-" + wid)
				});

*/

// 				$("#power-gauge-" + wid).fadeIn(500);

				allgauges["power-gauge-" + wid] = {};

				//KW gauge
				var chartopts = args.chartoptions;


				//color for KVAR gauge
				if (this.kvar_val < this.kw_max / 3) {
					var kvarcolor = "#3B91F2";
				} else {
					var kvarcolor = "#DF5353";
				}


				$("#kw-" + wid).highcharts();
				$("#kw-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 200,
						width: 200,
					},
					yAxis: {
						min: this.kw_min,
						max: this.kw_max,
						stops: [
							[0, "#8ACF00"],
							[0.99, "#8ACF00"],
							[0.991, "#DF5353"],
							[1, "#DF5353"]
						],
						title: {
							text: 'KW'
						},
						tickInterval: parseFloat((this.kw_max).toFixed(1)),
						tickPixelInterval: parseFloat((this.kw_max).toFixed(1)),
					},

					credits: {
						enabled: false
					},

					series: [{
						name: 'KW',
						data: [parseFloat(this.kw_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">KW</span></div>'
						},
						tooltip: {
							valueSuffix: ' KW'
						}
					}]

				}));

				allgauges["power-gauge-" + wid]["kw-gauge-" + wid] = $("#kw-" + wid).highcharts();

				//KVA Gauge
				var chartopts = args.chartoptions;

				$("#kva-" + wid).highcharts();
				$("#kva-" + wid).highcharts(Highcharts.merge(chartopts, {
					yAxis: {
						min: this.kva_min,
						max: this.kva_max,
						stops: [
							[0, "#DDDF0D"],
							[0.99, "#DDDF0D"],
							[0.991, "#DF5353"],
							[1, "#DF5353"]
						],
						title: {
							text: 'KVA'
						},
						tickInterval: parseFloat(this.kva_max),
						tickPixelInterval: parseFloat(this.kva_max),
					},

					credits: {
						enabled: false
					},

					series: [{
						name: 'KVA',
						data: [parseFloat(this.kva_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">KVA</span></div>'
						},
						tooltip: {
							valueSuffix: ' KW'
						}
					}]

				}));

				allgauges["power-gauge-" + wid]["kva-gauge-" + wid] = $("#kva-" + wid).highcharts();

				//KVAR Gauge
				var chartopts = args.chartoptions;

				$("#kvar-" + wid).highcharts();
				$("#kvar-" + wid).highcharts(Highcharts.merge(chartopts, {
					yAxis: {
						min: this.kvar_min,
						max: this.kvar_max,
						stops: [
							[0, "#3B91F2"],
							[this.kw_max / 3, "#3B91F2"],
							[(this.kw_max / 3) + 0.001, "#DF5353"],
							[1, "#DF5353"]
						],
						title: {
							text: 'KVAR'
						},
						tickInterval: parseInt(this.kvar_max),
						tickPixelInterval: parseInt(this.kvar_max),
					},

					credits: {
						enabled: false
					},

					series: [{
						name: 'KVA',
						data: [parseFloat(this.kvar_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">KVAR</span></div>'
						},
						tooltip: {
							valueSuffix: ' KW'
						}
					}]

				}));

				allgauges["power-gauge-" + wid]["kvar-gauge-" + wid] = $("#kvar-" + wid).highcharts();

				//set import/ export picture
				if (this.import === true) {
					$("#powerimg").css("backgroundPosition", "0");
				} else {
					$("#powerimg").css("backgroundPosition", "200px");
				}
			}
		} else {
			console.log("A Controller ID and Meter ID must be specified to create a widget");
		}
	}

	//voltage gauge
	function voltageGauge(args) {
		//parameters
		if (args.conId && args.meterId) {
			this.conId = args.conId;
			this.meterId = args.meterId;
			//unique id for each site/meter
			var wid = this.conId + '-' + this.meterId;

			//check if we have a widget with the same id already setup
// 			var cur_widgets = $("#widgetlist").sDashboard("getDashboardData");
			var okaytocreate = true;

/*
			$.each(cur_widgets, function(key, val) {
				if (val.widgetId == "voltage-gauge-" + wid) {
					okaytocreate = false;
				}
			});
*/

			if (okaytocreate) {
				args.title ? this.title = args.title : this.title = "Voltage: " + this.conId + "-" + this.meterId;
				args.avg_min ? this.avg_min = args.avg_min : this.avg_min = 0;
				args.avg_max ? this.avg_max = args.avg_max : this.avg_max = 255;
				args.avg_val ? this.avg_val = parseFloat(args.avg_val).toFixed(1) : this.avg_val = 125;
				args.p1_min ? this.p1_min = args.p1_min : this.p1_min = 0;
				args.p1_max ? this.p1_max = args.p1_max : this.p1_max = 255;
				args.p1_val ? this.p1_val = parseFloat(args.p1_val).toFixed(1) : this.p1_val = 230;
				args.p2_min ? this.p2_min = args.p2_min : this.p2_min = 0;
				args.p2_max ? this.p2_max = args.p2_max : this.p2_max = 255;
				args.p2_val ? this.p2_val = parseFloat(args.p2_val).toFixed(1) : this.p2_val = 230;
				args.p3_min ? this.p3_min = args.p3_min : this.p3_min = 0;
				args.p3_max ? this.p3_max = args.p3_max : this.p3_max = 255;
				args.p3_val ? this.p3_val = parseFloat(args.p3_val).toFixed(1) : this.p3_val = 230;

				//unique id
				var wid = this.conId + '-' + this.meterId;

				//structure
				var $vgHtml = '<div id="voltage-' + wid + '" class="ewidget wContain">';
				$vgHtml += '<div id="volts-avg-' + wid + '" class="avggauge"></div>';
				$vgHtml += '<div id="volts-p1-' + wid + '" class="p1gauge"></div>';
				$vgHtml += '<div id="volts-p2-' + wid + '" class="p2gauge"></div>';
				$vgHtml += '<div id="volts-p3-' + wid + '" class="p3gauge"></div>';
				$vgHtml += '</div>';

				//structure for gridster
				var $gvgHtml = '<li data-widget="voltage-gauge-' + wid + '"><div class="widget-title">' + this.title + '<span class="widget-close" onclick="deleteWidget(this);">&#xf2d7;</span></div>' + $vgHtml + '</li>';
/*

				$("body").append($vgHtml);

				//have sDashboard add the widget to the dashboard
				$("#widgetlist").sDashboard('addWidget', {
					widgetTitle: this.title,
					widgetId: "voltage-gauge-" + wid,
					widgetContent: $("#voltage-" + wid)
				});

*/
				//add to gridster
				gridster = $('.gridster > ul').gridster().data('gridster');
				//gridster.options.widget_base_dimensions = [220,220];
				gridster.add_widget($gvgHtml,args.gridster.size_x,args.gridster.size_y,args.gridster.col, args.gridster.row);

// 				$("#voltage-gauge-" + wid).fadeIn(500);

				allgauges["voltage-gauge-" + wid] = {};

				//AVG gauge
				var chartopts = args.chartoptions;

				$("#volts-avg-" + wid).highcharts();
				$("#volts-avg-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 250,
						width: 250
					},
					yAxis: {
						min: this.avg_min,
						max: this.avg_max,
						stops: [
							[220/this.avg_max, "#DF5353"], //red
							[225/this.avg_max, "#DDDF0D"], //yellow
							[240/this.avg_max, "#55BF3B"], //green
							[245/this.avg_max, "#DF5353"] //red
						],
						title: {
							text: 'Average',
							y: -70
						},
						tickInterval: parseInt(this.avg_max),
						tickPixelInterval: parseInt(this.avg_max),
					},
					credits: {
						enabled: false
					},
					series: [{
						name: 'Volts',
						data: [parseFloat(this.avg_val).toFixed(1)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">volts</span></div>'
						},
						tooltip: {
							valueSuffix: ' Volts'
						}
					}]

				}));

				allgauges["voltage-gauge-" + wid]["avg-gauge-" + wid] = $("#volts-avg-" + wid).highcharts();

				//Phase 1 volts gauge
				var chartopts = args.chartoptions;

				$("#volts-p1-" + wid).highcharts();
				$("#volts-p1-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 200,
						width: 150
					},
					yAxis: {
						min: this.p1_min,
						max: this.p1_max,
						stops: false,
						minColor: "#ab8358",
						maxColor: "#ab8358",
						title: {
							text: 'L1'
						},
						tickInterval: parseInt(this.p1_max),
						tickPixelInterval: parseInt(this.p1_max),
					},

					credits: {
						enabled: false
					},

					series: [{
						name: 'Volts',
						data: [parseFloat(this.p1_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">volts</span></div>'
						},
						tooltip: {
							valueSuffix: ' V'
						}
					}]

				}));

				allgauges["voltage-gauge-" + wid]["p1-gauge-" + wid] = $("#volts-p1-" + wid).highcharts();

				//Phase 2 volts gauge
				var chartopts = args.chartoptions;

				$("#volts-p2-" + wid).highcharts();
				$("#volts-p2-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 200,
						width: 150
					},
					yAxis: {
						min: this.p2_min,
						max: this.p2_max,
						stops: false,
						minColor: "#2a2a2a",
						maxColor: "#2a2a2a",
						title: {
							text: 'L2'
						},
						tickInterval: parseInt(this.p2_max),
						tickPixelInterval: parseInt(this.p2_max),
					},

					credits: {
						enabled: false
					},

					series: [{
						name: 'Volts',
						data: [parseFloat(this.p2_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">volts</span></div>'
						},
						tooltip: {
							valueSuffix: ' V'
						}
					}]

				}));

				allgauges["voltage-gauge-" + wid]["p2-gauge-" + wid] = $("#volts-p2-" + wid).highcharts();

				//Phase 3 volts gauge
				var chartopts = args.chartoptions;

				$("#volts-p3-" + wid).highcharts();
				$("#volts-p3-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 200,
						width: 150
					},
					yAxis: {
						min: this.p3_min,
						max: this.p3_max,
						stops: false,
						minColor: "#a3a3a3",
						maxColor: "#a3a3a3",
						title: {
							text: 'L3'
						},
						tickInterval: parseInt(this.p3_max),
						tickPixelInterval: parseInt(this.p3_max),
					},

					credits: {
						enabled: false
					},

					series: [{
						name: 'Volts',
						data: [parseFloat(this.p3_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">volts</span></div>'
						},
						tooltip: {
							valueSuffix: ' V'
						}
					}]

				}));

				allgauges["voltage-gauge-" + wid]["p3-gauge-" + wid] = $("#volts-p3-" + wid).highcharts();

			}
		} else {
			console.log("A Controller ID and Meter ID must be specified to create a widget");
		}

	}

	function voltageLine(args) {
		if (args.conId && args.meterId) {
			this.conId = args.conId;
			this.meterId = args.meterId;
			this.p1_val = args.p1_val;
			this.p2_val = args.p2_val;
			this.p3_val = args.p3_val;

			//unique id for each site/meter
			var wid = this.conId + '-' + this.meterId;

			//check if we have a widget with the same id already setup
// 			var cur_widgets = $("#widgetlist").sDashboard("getDashboardData");
			var okaytocreate = true;

/*
			$.each(cur_widgets, function(key, val) {
				if (val.widgetId == "voltage-line-" + wid) {
					okaytocreate = false;
				}
			});
*/

			if (okaytocreate) {
				args.title ? this.title = args.title : this.title = "Voltage: " + wid;
				args.desc ? this.desc = args.desc : this.desc = wid;

				//structure
				var $vlHtml = '<div id="voltage-l-' + wid + '" class="ewidget wContain line-graph"></div>';

				//structure for gridster
				var $gvlHtml = '<li data-widget="voltage-line-' + wid + '"><div class="widget-title">' + this.title + '<span class="widget-close" onclick="deleteWidget(this);">&#xf2d7;</span></div>' + $vlHtml + '</li>';

				//add to gridster
				gridster = $('.gridster > ul').gridster().data('gridster');
				//gridster.options.widget_base_dimensions = [220,220];
				gridster.add_widget($gvlHtml,args.gridster.size_x,args.gridster.size_y,args.gridster.col, args.gridster.row);
/*

				$("body").append($vlHtml);

				//have sDashboard add the widget to the dashboard
				$("#widgetlist").sDashboard('addWidget', {
					widgetTitle: this.title,
					widgetId: "voltage-line-" + wid,
					widgetContent: $("#voltage-l-" + wid)
				});

*/
// 				$("#voltage-line-" + wid).fadeIn(500);

				//highcharts options
				var chartopts = args.chartoptions;

				//set renderTo highcharts option
				chartopts.chart.renderTo = "voltage-l-" + wid;

				chartopts.yAxis = {
					min: 0,
					max: 255,
					title: {
						enabled: false
					},
					type: 'linear',
					labels: {
						enabled: true,
						formatter: function() {
								return Highcharts.numberFormat(this.value, 0, '', ',') + ' V';
						}
					}
				};

				chartopts.tooltip = {
					animation: true,
					pointFormat: '{series.name}: <b>{point.y}</b><br>',
					shared: true,
					valueDecimals: 0,
					valueSuffix: ' V',
					formatter: function() {
						var s = [];
						s.push('<b>Time: </b>' + Highcharts.dateFormat('%H:%M:%S', this.x));
						$.each(this.points, function(i, point) {
							s.push('<b>' + point.series.name + ': </b>' + point.y + ' V');
						});

						return s.join('<br>');
					}
				};

				//create chart
				allgauges["voltage-line-" + wid] = {};
				allgauges["voltage-line-" + wid]["voltage-highchart-" + wid] = new Highcharts.Chart(chartopts);

				//get mprn for site
				var mprn = getData(conId, meterId, "mprn");

				//set start and end times for data
				var end = Math.round((new Date()).getTime() / 1000);
				var start = end - 300;

				//create url for api request
				var url = "/api/site/historical?site=" + mprn + "&controller=" + this.conId + "&meter=" + this.meterId;
				url += "&params=phase_1_volts,phase_2_volts,phase_3_volts&start=" + start + "&end=" + end;

				//get the data
				$.ajax({
					url: url,
					type: "GET",
					dataType: "JSON",
					success: function(result) {
						var chart_p1 = [];
						var chart_p2 = [];
						var chart_p3 = [];

						$.each(result.raw, function(key, val) {

							chart_p1.push([parseFloat(val.time_sent), parseFloat(val.phase_1_volts)]);
							chart_p2.push([parseFloat(val.time_sent), parseFloat(val.phase_2_volts)]);
							chart_p3.push([parseFloat(val.time_sent), parseFloat(val.phase_3_volts)]);

						});

						chart_p1.sort();
						chart_p2.sort();
						chart_p3.sort();

						allgauges["voltage-line-" + wid]["voltage-highchart-" + wid].addSeries({
							name: "Phase 1 Volts",
							data: chart_p1,
							color: "#ab8358"
						});

						allgauges["voltage-line-" + wid]["voltage-highchart-" + wid].addSeries({
							name: "Phase 2 Volts",
							data: chart_p2,
							color: "#2a2a2a"
						});

						allgauges["voltage-line-" + wid]["voltage-highchart-" + wid].addSeries({
							name: "Phase 3 Volts",
							data: chart_p3,
							color: "#a3a3a3"
						});


					}
				});

			}
		} else {
			console.log("A Controller ID and Meter ID are required to create a widget");
		}
	}

	function currentGauge(args) {
		//parameters
		if (args.conId && args.meterId) {
			this.conId = args.conId;
			this.meterId = args.meterId;
			//unique id for each site/meter
			var wid = this.conId + '-' + this.meterId;

			//check if we have a widget with the same id already setup
// 			var cur_widgets = $("#widgetlist").sDashboard("getDashboardData");
			var okaytocreate = true;

/*
			$.each(cur_widgets, function(key, val) {
				if (val.widgetId == "current-gauge-" + wid) {
					okaytocreate = false;
				}
			});
*/

			if (okaytocreate) {
				args.title ? this.title = args.title : this.title = "Current: " + this.conId + "-" + this.meterId;
				args.sum_min ? this.sum_min = args.sum_min : this.sum_min = 0;
				args.sum_max > 0 ? this.sum_max = args.sum_max : this.sum_max = 100;
				args.sum_val ? this.sum_val = parseFloat(args.sum_val).toFixed(1) : this.sum_val = 50;
				args.p1_min ? this.p1_min = args.p1_min : this.p1_min = 0;
				args.p1_max > 0 ? this.p1_max = args.p1_max : this.p1_max = 100;
				args.p1_val ? this.p1_val = parseFloat(args.p1_val).toFixed(1) : this.p1_val = 50;
				args.p2_min ? this.p2_min = args.p2_min : this.p2_min = 0;
				args.p2_max > 0 ? this.p2_max = args.p2_max : this.p2_max = 100;
				args.p2_val ? this.p2_val = parseFloat(args.p2_val).toFixed(1) : this.p2_val = 50;
				args.p3_min ? this.p3_min = args.p3_min : this.p3_min = 0;
				args.p3_max > 0 ? this.p3_max = args.p3_max : this.p3_max = 100;
				args.p3_val ? this.p3_val = parseFloat(args.p3_val).toFixed(1) : this.p3_val = 50;
                args.na_min ? this.na_min = args.na_min : this.na_min = 0;
				args.na_max > 0 ? this.na_max = args.na_max : this.na_max = this.p1_max * .1;
                args.na_val ? this.na_val = parseFloat(args.na_val).toFixed(1) : this.na_val = 50;

				//structure
				var $agHtml = '<div id="current-' + wid + '" class="ewidget wContain">';
                $agHtml += '<div class="current-top-wrap">';
                $agHtml += '<div id="current-sum-' + wid + '" class="sumgauge"></div>';
                $agHtml += '<div id="current-na-' + wid + '" class="nagauge"></div></div>';
                $agHtml += '<div class="current-bottom-wrap">';
				$agHtml += '<div id="current-p1-' + wid + '" class="i1gauge"></div>';
				$agHtml += '<div id="current-p2-' + wid + '" class="i2gauge"></div>';
				$agHtml += '<div id="current-p3-' + wid + '" class="i3gauge"></div>';
                $agHtml += '</div>';
				$agHtml += '</div>';

				var $gagHtml = '<li data-widget="current-gauge-' + wid + '"><div class="widget-title">' + this.title + '<span class="widget-close" onclick="deleteWidget(this);">&#xf2d7;</span></div>' + $agHtml + '</li>';

				//add to gridster
				gridster = $('.gridster > ul').gridster().data('gridster');
				//gridster.options.widget_base_dimensions = [220,220];
				gridster.add_widget($gagHtml,args.gridster.size_x,args.gridster.size_y,args.gridster.col, args.gridster.row);
/*

				$("body").append($agHtml);

				//have sDashboard add the widget to the dashboard
				$("#widgetlist").sDashboard('addWidget', {
					widgetTitle: this.title,
					widgetId: "current-gauge-" + wid,
					widgetContent: $("#current-" + wid)
				});

				$("#current-gauge-" + wid).fadeIn(500);

*/
				allgauges["current-gauge-" + wid] = {};

				//SUM gauge
				var chartopts = args.chartoptions;

				$("#current-sum-" + wid).highcharts();
				$("#current-sum-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 250,
						width: 250
					},
					yAxis: {
						min: parseInt(this.sum_min),
						max: parseInt(this.sum_max),
						stops: [
							[0, "#55BF3B"], //green
							[0.899999, "#55BF3B"], //green
							[0.9, "#DDDF0D"], //yellow
							[0.949999, "#DDDF0D"], //yellow
							[0.95, "#DF5353"] //red
						],
						title: {
							text: 'Total',
							y: -70
						},
						tickInterval: parseInt(this.sum_max),
						tickPixelInterval: parseInt(this.sum_max),
					},
					credits: {
						enabled: false
					},
					series: [{
						name: 'Amps',
						data: [parseFloat(this.sum_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">amps</span></div>'
						},
						tooltip: {
							valueSuffix: ' A'
						}
					}]

				}));

				allgauges["current-gauge-" + wid]["sum-gauge-" + wid] = $("#current-sum-" + wid).highcharts();

				//Phase 1 amps gauge
				var chartopts = args.chartoptions;

				$("#current-p1-" + wid).highcharts();
				$("#current-p1-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 200,
						width: 150
					},
					yAxis: {
						min: 0,
						max: parseInt(this.p1_max),
						stops: false,
						minColor: "#ab8358",
						maxColor: "#ab8358",
						title: {
							text: 'I1'
						},
						tickInterval: parseInt(this.p1_max),
						tickPixelInterval: parseInt(this.p1_max),
					},
					credits: {
						enabled: false
					},
					series: [{
						name: 'Amps',
						data: [parseFloat(this.p1_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">amps</span></div>'
						},
						tooltip: {
							valueSuffix: ' A'
						}
					}]

				}));

				allgauges["current-gauge-" + wid]["i1-gauge-" + wid] = $("#current-p1-" + wid).highcharts();

				//Phase 2 amps gauge
				var chartopts = args.chartoptions;

				$("#current-p2-" + wid).highcharts();
				$("#current-p2-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 200,
						width: 150
					},
					yAxis: {
						min: 0,
						max: parseInt(this.p2_max),
						stops: false,
						minColor: "#2a2a2a",
						maxColor: "#2a2a2a",
						title: {
							text: 'I2'
						},
						tickInterval: parseInt(this.p2_max),
						tickPixelInterval: parseInt(this.p2_max),
					},
					credits: {
						enabled: false
					},
					series: [{
						name: 'Amps',
						data: [parseFloat(this.p2_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">amps</span></div>'
						},
						tooltip: {
							valueSuffix: ' A'
						}
					}]

				}));

				allgauges["current-gauge-" + wid]["i2-gauge-" + wid] = $("#current-p2-" + wid).highcharts();

				//Phase 3 amps gauge
				var chartopts = args.chartoptions;

				$("#current-p3-" + wid).highcharts();
				$("#current-p3-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 200,
						width: 150
					},
					yAxis: {
						min: 0,
						max: parseInt(this.p3_max),
						stops: false,
						minColor: "#a3a3a3",
						maxColor: "#a3a3a3",
						title: {
							text: 'I3'
						},
						tickInterval: parseInt(this.p3_max),
						tickPixelInterval: parseInt(this.p3_max),
					},
					credits: {
						enabled: false
					},
					series: [{
						name: 'Amps',
						data: [parseFloat(this.p3_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">amps</span></div>'
						},
						tooltip: {
							valueSuffix: ' A'
						}
					}]

				}));

				allgauges["current-gauge-" + wid]["i3-gauge-" + wid] = $("#current-p3-" + wid).highcharts();

                //Neutral amps gauge
				var chartopts = args.chartoptions;

				$("#current-na-" + wid).highcharts();
				$("#current-na-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 200,
						width: 150
					},
					yAxis: {
						min: 0,
						max: parseInt(this.na_max),
						stops: false,
						minColor: "#47afff",
						maxColor: "#47afff",
						title: {
							text: 'Neutral'
						},
						tickInterval: parseInt(this.na_max),
						tickPixelInterval: parseInt(this.na_max),
					},
					credits: {
						enabled: false
					},
					series: [{
						name: 'Amps',
						data: [parseFloat(this.na_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">amps</span></div>'
						},
						tooltip: {
							valueSuffix: ' A'
						}
					}]

				}));

				allgauges["current-gauge-" + wid]["na-gauge-" + wid] = $("#current-na-" + wid).highcharts();

			}
		} else {
			console.log("A Controller ID and Meter ID are required to create a widget");
		}
	}

	function currentLine(args) {
		if (args.conId && args.meterId) {
			this.conId = args.conId;
			this.meterId = args.meterId;
			this.p1_val = args.p1_val;
			this.p2_val = args.p2_val;
			this.p3_val = args.p3_val;

			//unique id for each site/meter
			var wid = this.conId + '-' + this.meterId;

			//check if we have a widget with the same id already setup
// 			var cur_widgets = $("#widgetlist").sDashboard("getDashboardData");
			var okaytocreate = true;

/*
			$.each(cur_widgets, function(key, val) {
				if (val.widgetId == "current-line-" + wid) {
					okaytocreate = false;
				}
			});
*/

			if (okaytocreate) {
				args.title ? this.title = args.title : this.title = "Current: " + wid;
				args.desc ? this.desc = args.desc : this.desc = wid;

				//structure
				var $clHtml = '<div id="current-l-' + wid + '" class="ewidget wContain line-graph"></div>';
				var $gclHtml = '<li data-widget="current-line-' + wid + '"><div class="widget-title">' + this.title + '<span class="widget-close" onclick="deleteWidget(this);">&#xf2d7;</span></div>' + $clHtml + '</li>';

				//add to gridster
				gridster = $('.gridster > ul').gridster().data('gridster');
				//gridster.options.widget_base_dimensions = [220,220];
				gridster.add_widget($gclHtml,args.gridster.size_x,args.gridster.size_y,args.gridster.col, args.gridster.row);
/*

				$("body").append($clHtml);

				//have sDashboard add the widget to the dashboard
				$("#widgetlist").sDashboard('addWidget', {
					widgetTitle: this.title,
					widgetId: "current-line-" + wid,
					widgetContent: $("#current-l-" + wid)
				});

				$("#current-line-" + wid).fadeIn(500);
*/

				//highcharts options
				var chartopts = args.chartoptions;

				//set renderTo highcharts option
				chartopts.chart.renderTo = "current-l-" + wid;

				chartopts.yAxis = {
					title: {
						enabled: false
					},
					type: 'linear',
					labels: {
						enabled: true,
						formatter: function() {
								return Highcharts.numberFormat(this.value, 0, '', ',') + ' A';
						}
					}
				};

				chartopts.tooltip = {
					animation: true,
					pointFormat: '{series.name}: <b>{point.y}</b><br>',
					shared: true,
					valueDecimals: 0,
					valueSuffix: ' V',
					formatter: function() {
						var s = [];
						s.push('<b>Time: </b>' + Highcharts.dateFormat('%H:%M:%S', this.x));
						$.each(this.points, function(i, point) {
							s.push('<b>' + point.series.name + ': </b>' + point.y + ' A');
						});

						return s.join('<br>');
					}
				};

				//create chart
				allgauges["current-line-" + wid] = {};
				allgauges["current-line-" + wid]["current-highchart-" + wid] = new Highcharts.Chart(chartopts);

				//get mprn for site
				var mprn = getData(conId, meterId, "mprn");

				//set start and end times for data
				var end = Math.round((new Date()).getTime() / 1000);
				var start = end - 300;

				//create url for api request
				var url = "/api/site/historical?site=" + mprn + "&controller=" + this.conId + "&meter=" + this.meterId;
				url += "&params=phase_1_amps,phase_2_amps,phase_3_amps,neutral_amps&start=" + start + "&end=" + end;

				//get the data
				$.ajax({
					url: url,
					type: "GET",
					dataType: "JSON",
					success: function(result) {
						var chart_p1 = [];
						var chart_p2 = [];
						var chart_p3 = [];
						var chart_p4 = [];

						$.each(result.raw, function(key, val) {

							chart_p1.push([parseFloat(val.time_sent), parseFloat(val.phase_1_amps)]);
							chart_p2.push([parseFloat(val.time_sent), parseFloat(val.phase_2_amps)]);
							chart_p3.push([parseFloat(val.time_sent), parseFloat(val.phase_3_amps)]);
							chart_p4.push([parseFloat(val.time_sent), parseFloat(val.neutral_amps)]);

						});

						chart_p1.sort();
						chart_p2.sort();
						chart_p3.sort();
						chart_p4.sort();

						allgauges["current-line-" + wid]["current-highchart-" + wid].addSeries({
							name: "Phase 1 Amps",
							data: chart_p1,
							color: "#ab8358"
						});

						allgauges["current-line-" + wid]["current-highchart-" + wid].addSeries({
							name: "Phase 2 Amps",
							data: chart_p2,
							color: "#2a2a2a"
						});

						allgauges["current-line-" + wid]["current-highchart-" + wid].addSeries({
							name: "Phase 3 Amps",
							data: chart_p3,
							color: "#a3a3a3"
						});

						allgauges["current-line-" + wid]["current-highchart-" + wid].addSeries({
							name: "Neutral Amps",
							data: chart_p4,
							color: "#47afff"
						});
					}
				});

			}
		} else {
			console.log("A Controller ID and Meter ID are required to create a widget");
		}
	}

	function thdGauge(args) {

		if (args.conId && args.meterId) {
			this.conId = args.conId;
			this.meterId = args.meterId;
			//unique id for each site/meter
			var wid = this.conId + '-' + this.meterId;

			//check if we have a widget with the same id already setup
// 			var cur_widgets = $("#widgetlist").sDashboard("getDashboardData");
			var okaytocreate = true;

/*
			$.each(cur_widgets, function(key, val) {
				if (val.widgetId == "thd-gauge-" + wid) {
					okaytocreate = false;
				}
			});
*/

			if (okaytocreate) {
				args.v1_min ? this.v1_min = args.v1_min : this.v1_min = 0;
				args.v1_max ? this.v1_max = args.v1_max : this.v1_max = 10;
				args.v1_val ? this.v1_val = parseFloat(args.v1_val).toFixed(1) : this.v1_val = 5;
				args.v2_min ? this.v2_min = args.v2_min : this.v2_min = 0;
				args.v2_max ? this.v2_max = args.v2_max : this.v2_max = 10;
				args.v2_val ? this.v2_val = parseFloat(args.v2_val).toFixed(1) : this.v2_val = 5;
				args.v3_min ? this.v3_min = args.v3_min : this.v3_min = 0;
				args.v3_max ? this.v3_max = args.v3_max : this.v3_max = 10;
				args.v3_val ? this.v3_val = parseFloat(args.v3_val).toFixed(1) : this.v3_val = 5;
				args.i1_min ? this.i1_min = args.i1_min : this.i1_min = 0;
				args.i1_max ? this.i1_max = args.i1_max : this.i1_max = 60;
				args.i1_val ? this.i1_val = parseFloat(args.i1_val).toFixed(1) : this.i1_val = 30;
				args.i2_min ? this.i2_min = args.i2_min : this.i2_min = 0;
				args.i2_max ? this.i2_max = args.i2_max : this.i2_max = 60;
				args.i2_val ? this.i2_val = parseFloat(args.i2_val).toFixed(1) : this.i2_val = 30;
				args.i3_min ? this.i3_min = args.i3_min : this.i3_min = 0;
				args.i3_max ? this.i3_max = args.i3_max : this.i3_max = 60;
				args.i3_val ? this.i3_val = parseFloat(args.i3_val).toFixed(1) : this.i3_val = 30;
				args.thd_text_green ? this.thd_text_green = args.thd_text_green : this.thd_text_green = "Normal situation, no risk of malfunctions";
				args.thd_text_amber ? this.thd_text_amber = args.thd_text_green : this.thd_text_amber = "Significant harmonic pollution, some malfunctions are possible";
				args.thd_text_red ? this.thd_text_red = args.thd_text_red : this.thd_text_red = "Major harmonic pollution, malfunctions are probable. In-depth analysis and the installation of attenuation devices are required";
				args.title ? this.title = args.title : this.title = "THD: " + conId + "-" + meterId;

				var showAmps = checkAmps(this.conId, this.meterId);
				var showVolts = checkVolts(this.conId, this.meterId);

				//structure
				var $tgHtml = '<div id="thd-' + wid + '" class="ewidget wContain">';
				$tgHtml += '<div class="thd-volts-ignore"';

				if (!showVolts) {
					$tgHtml += ' style="display: block"';
				}

				$tgHtml += '><p>Harmonic Distortion - Voltage values are presently unreliable due to low voltage measurements.</p></div>';
				$tgHtml += '<div id="thd-volts-wrap"';

				if (!showVolts) {
					$tgHtml += ' style="display: none"';
				}

				$tgHtml += '><div class="thd-volts">';
				$tgHtml += '<h3>Total Harmonic Distortion - Voltage</h3>';
				$tgHtml += '<div id="thd-v1-gauge-' + wid + '" class="thdgauge"></div>';
				$tgHtml += '<div id="thd-v2-gauge-' + wid + '" class="thdgauge"></div>';
				$tgHtml += '<div id="thd-v3-gauge-' + wid + '" class="thdgauge"></div>';
				$tgHtml += '</div>';
				$tgHtml += '<div class="thd-volts-msg"></div>';
				$tgHtml += '</div>';
				$tgHtml += '<div class="thd-amps-ignore"';

				if (!showAmps) {
					$tgHtml += ' style="display: block"';
				}

				$tgHtml += '><p>Harmonic Distortion - Current values are presently unreliable due to low current measurements.</p></div>';
				$tgHtml += '<div id="thd-amps-wrap"';

				if (!showAmps) {
					$tgHtml += ' style="display: none"';
				}

				$tgHtml += '><div class="thd-amps">';
				$tgHtml += '<h3>Total Harmonic Distortion - Current</h3>';
				$tgHtml += '<div id="thd-i1-gauge-' + wid + '" class="thdgauge"></div>';
				$tgHtml += '<div id="thd-i2-gauge-' + wid + '" class="thdgauge"></div>';
				$tgHtml += '<div id="thd-i3-gauge-' + wid + '" class="thdgauge"></div>';
				$tgHtml += '</div>';
				$tgHtml += '<div class="thd-amps-msg"></div>';
				$tgHtml += '</div>';
				$tgHtml += '</div>';

				var $gtgHtml = '<li data-widget="thd-gauge-' + wid + '"><div class="widget-title">' + this.title + '<span class="widget-close" onclick="deleteWidget(this);">&#xf2d7;</span></div>' + $tgHtml + '</li>';
				//add to gridster
				gridster = $('.gridster > ul').gridster().data('gridster');
				//gridster.options.widget_base_dimensions = [220,220];
				gridster.add_widget($gtgHtml,args.gridster.size_x,args.gridster.size_y,args.gridster.col, args.gridster.row);
/*

				$("body").append($tgHtml);

				//have sDashboard add the widget to the dashboard
				$("#widgetlist").sDashboard('addWidget', {
					widgetTitle: this.title,
					widgetId: "thd-gauge-" + wid,
					widgetContent: $("#thd-" + wid)
				});

				$("#thd-gauge-" + wid).fadeIn(500);

*/
				allgauges["thd-gauge-" + wid] = {};

				//Phase 1 volts thd gauge
				var chartopts = args.chartoptions;

				$("#thd-v1-gauge-" + wid).highcharts();
				$("#thd-v1-gauge-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 150,
						width: 125
					},
					yAxis: {
						min: 0,
						max: parseInt(this.v1_max),
						stops: [
							[0, "#55BF3B"],
							[0.499999, "#55BF3B"],
							[.5, "#DDDF0D"],
							[0.799999, "#DDDF0D"],
							[0.8, "#DF5353"]
						],
						title: {
							text: 'P1'
						},
						tickInterval: parseInt(this.v1_max),
						tickPixelInterval: parseInt(this.v1_max),
					},
					credits: {
						enabled: false
					},
					series: [{
						name: 'P1',
						data: [parseFloat(this.v1_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">%</span></div>'
						},
						tooltip: {
							valueSuffix: ' %'
						}
					}]

				}));

				allgauges["thd-gauge-" + wid]["v1-gauge-" + wid] = $("#thd-v1-gauge-" + wid).highcharts();

				//Phase 2 volts thd gauge
				var chartopts = args.chartoptions;

				$("#thd-v2-gauge-" + wid).highcharts();
				$("#thd-v2-gauge-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 150,
						width: 125
					},
					yAxis: {
						min: 0,
						max: parseInt(this.v2_max),
						stops: [
							[0, "#55BF3B"],
							[0.499999, "#55BF3B"],
							[.5, "#DDDF0D"],
							[0.799999, "#DDDF0D"],
							[0.8, "#DF5353"]
						],
						title: {
							text: 'P2'
						},
						tickInterval: parseInt(this.v2_max),
						tickPixelInterval: parseInt(this.v2_max),
					},
					credits: {
						enabled: false
					},
					series: [{
						name: 'P2',
						data: [parseFloat(this.v2_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">%</span></div>'
						},
						tooltip: {
							valueSuffix: ' %'
						}
					}]

				}));

				allgauges["thd-gauge-" + wid]["v2-gauge-" + wid] = $("#thd-v2-gauge-" + wid).highcharts();

				//Phase 3 volts thd gauge
				var chartopts = args.chartoptions;

				$("#thd-v3-gauge-" + wid).highcharts();
				$("#thd-v3-gauge-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 150,
						width: 125
					},
					yAxis: {
						min: 0,
						max: parseInt(this.v3_max),
						stops: [
							[0, "#55BF3B"],
							[0.499999, "#55BF3B"],
							[.5, "#DDDF0D"],
							[0.799999, "#DDDF0D"],
							[0.8, "#DF5353"]
						],
						title: {
							text: 'P3'
						},
						tickInterval: parseInt(this.v3_max),
						tickPixelInterval: parseInt(this.v3_max),
					},
					credits: {
						enabled: false
					},
					series: [{
						name: 'P3',
						data: [parseFloat(this.v3_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">%</span></div>'
						},
						tooltip: {
							valueSuffix: ' %'
						}
					}]

				}));

				allgauges["thd-gauge-" + wid]["v3-gauge-" + wid] = $("#thd-v3-gauge-" + wid).highcharts();

				//Phase 1 amps thd gauge
				var chartopts = args.chartoptions;

				$("#thd-i1-gauge-" + wid).highcharts();
				$("#thd-i1-gauge-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 150,
						width: 125
					},
					yAxis: {
						min: 0,
						max: parseInt(this.i1_max),
						stops: [
							[0, "#55BF3B"],
							[10/60, "#55BF3B"],
							[10/60 + 0.00001, "#DDDF0D"],
							[50/60, "#DDDF0D"],
							[50/60 + 0.00001, "#DF5353"]
						],
						title: {
							text: 'I1'
						},
						tickInterval: parseInt(this.i1_max),
						tickPixelInterval: parseInt(this.i1_max),
					},
					credits: {
						enabled: false
					},
					series: [{
						name: 'I1',
						data: [parseFloat(this.i1_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">%</span></div>'
						},
						tooltip: {
							valueSuffix: ' %'
						}
					}]

				}));

				allgauges["thd-gauge-" + wid]["i1-gauge-" + wid] = $("#thd-i1-gauge-" + wid).highcharts();

				//Phase 2 amps thd gauge
				var chartopts = args.chartoptions;

				$("#thd-i2-gauge-" + wid).highcharts();
				$("#thd-i2-gauge-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 150,
						width: 125
					},
					yAxis: {
						min: 0,
						max: parseInt(this.i2_max),
						stops: [
							[0, "#55BF3B"],
							[10/60, "#55BF3B"],
							[10/60 + 0.00001, "#DDDF0D"],
							[50/60, "#DDDF0D"],
							[50/60 + 0.00001, "#DF5353"]
						],
						title: {
							text: 'I2'
						},
						tickInterval: parseInt(this.i2_max),
						tickPixelInterval: parseInt(this.i2_max),
					},
					credits: {
						enabled: false
					},
					series: [{
						name: 'I2',
						data: [parseFloat(this.i2_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">%</span></div>'
						},
						tooltip: {
							valueSuffix: ' %'
						}
					}]

				}));

				allgauges["thd-gauge-" + wid]["i2-gauge-" + wid] = $("#thd-i2-gauge-" + wid).highcharts();

				//Phase 3 amps thd gauge
				var chartopts = args.chartoptions;

				$("#thd-i3-gauge-" + wid).highcharts();
				$("#thd-i3-gauge-" + wid).highcharts(Highcharts.merge(chartopts, {
					chart: {
						height: 150,
						width: 125
					},
					yAxis: {
						min: 0,
						max: parseInt(this.i3_max),
						stops: [
							[0, "#55BF3B"],
							[10/60, "#55BF3B"],
							[10/60 + 0.00001, "#DDDF0D"],
							[50/60, "#DDDF0D"],
							[50/60 + 0.00001, "#DF5353"]
						],
						title: {
							text: 'I3'
						},
						tickInterval: parseInt(this.i3_max),
						tickPixelInterval: parseInt(this.i3_max),
					},
					credits: {
						enabled: false
					},
					series: [{
						name: 'I3',
						data: [parseFloat(this.i3_val)],
						dataLabels: {
							format: '<div style="text-align:center"><span style="font-size:15px;color:' +
								((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
								'<span style="font-size:12px;color:silver">%</span></div>'
						},
						tooltip: {
							valueSuffix: ' %'
						}
					}]

				}));

				allgauges["thd-gauge-" + wid]["i3-gauge-" + wid] = $("#thd-i3-gauge-" + wid).highcharts();

				//create messages
				if (this.v1_val > 0 || this.v2_val > 0 || this.v3_val > 0) {
					var vmsg = this.thd_text_green;
				}

				if (this.v1_val > 10 || this.v2_val > 10 || this.v3_val > 10) {
					var vmsg = this.thd_text_amber;
				}

				if (this.v1_val > 50 || this.v2_val > 50 || this.v3_val > 50) {
					var vmsg = this.thd_text_red;
				}

				if (this.i1_val > 0 || this.i2_val > 0 || this.i3_val > 0) {
					var amsg = this.thd_text_green;
				}

				if (this.i1_val > 10 || this.i2_val > 10 || this.i3_val > 10) {
					var amsg = this.thd_text_amber;
				}
				if (this.i1_val > 50 || this.i2_val > 50 || this.i3_val > 50) {
					var amsg = this.thd_text_red;
				}

				$("#thd-" + wid + " .thd-volts-msg").html(vmsg);
				$("#thd-" + wid + " .thd-amps-msg").html(amsg);

			}
		} else {
			console.log("A Controller ID and Meter ID are required to create a widget");
		}
	}

	function thdLine(args) {
		if (args.conId && args.meterId) {
			this.conId = args.conId;
			this.meterId = args.meterId;
			this.v1_val = args.v1_val;
			this.v2_val = args.v2_val;
			this.v3_val = args.v3_val;
			this.i1_val = args.i1_val;
			this.i2_val = args.i2_val;
			this.i3_val = args.i3_val;

			//unique id for each site/meter
			var wid = this.conId + '-' + this.meterId;

			//check if we have a widget with the same id already setup
// 			var cur_widgets = $("#widgetlist").sDashboard("getDashboardData");
			var okaytocreate = true;

/*
			$.each(cur_widgets, function(key, val) {
				if (val.widgetId == "thd-line-" + wid) {
					okaytocreate = false;
				}
			});
*/

			if (okaytocreate) {
				args.title ? this.title = args.title : this.title = "THD: " + wid;
				args.desc ? this.desc = args.desc : this.desc = wid;

				var showVolts = checkVolts(this.conId, this.meterId);
				var showAmps = checkAmps(this.conId, this.meterId);

				//structure
				var $tlHtml = '<div id="thd-line-wrap-' + wid + '" class="ewidget wContain line-graph">';
				$tlHtml += '<div class="thd-line-ignore"';

				if (!showVolts || !showAmps) {
					$tlHtml += ' style="display: block"';
				} else {
					$tlHtml += ' style="display: none"';
				}

				$tlHtml += '><p>Due to low measurement values <span id="thd-type-' + wid + '">';

				if (!showVolts && !showAmps) $tlHtml += "Voltage and Current";
				if (showVolts && !showAmps) $tlHtml += "Current";
				if (!showVolts && showAmps) $tlHtml += "Voltage";

				$tlHtml += '</span> Harmonic Distortion values are not reliable.</p></div>';

				$tlHtml += '<div id="thd-l-' + wid + '"';

				if (!showVolts || !showAmps) {
					$tlHtml += ' style="height: 300px;" ';
				}

				$tlHtml += '></div></div>';
				//structure for gridster
				var $gtlHtml = '<li data-widget="thd-line-' + wid + '"><div class="widget-title">' + this.title + '<span class="widget-close" onclick="deleteWidget(this);">&#xf2d7;</span></div>' + $tlHtml + '</li>';
				//add to gridster
				gridster = $('.gridster > ul').gridster().data('gridster');
				//gridster.options.widget_base_dimensions = [220,220];
				gridster.add_widget($gtlHtml,args.gridster.size_x,args.gridster.size_y,args.gridster.col, args.gridster.row);
/*

				$("body").append($tlHtml);

				//have sDashboard add the widget to the dashboard
				$("#widgetlist").sDashboard('addWidget', {
					widgetTitle: this.title,
					widgetId: "thd-line-" + wid,
					widgetContent: $("#thd-line-wrap-" + wid)
				});

*/
// 				$("#thd-line-" + wid).fadeIn(500);

				//highcharts options
				var chartopts = args.chartoptions;

				//set renderTo highcharts option
				chartopts.chart.renderTo = "thd-l-" + wid;

				chartopts.yAxis = {
					title: {
						enabled: false
					},
					type: 'linear',
					labels: {
						enabled: true,
						formatter: function() {
								return Highcharts.numberFormat(this.value, 0, '', ',') + ' %';
						}
					}
				};

				chartopts.tooltip = {
					animation: true,
					pointFormat: '{series.name}: <b>{point.y}</b><br>',
					shared: true,
					valueDecimals: 0,
					valueSuffix: ' V',
					formatter: function() {
						var s = [];
						s.push('<b>Time: </b>' + Highcharts.dateFormat('%H:%M:%S', this.x));
						$.each(this.points, function(i, point) {
							s.push('<b>' + point.series.name + ': </b>' + Highcharts.numberFormat(point.y, 1,'.', ',') + ' %');
						});

						return s.join('<br>');
					}
				};

				//create chart
				allgauges["thd-line-" + wid] = {};
				allgauges["thd-line-" + wid]["thd-highchart-" + wid] = new Highcharts.Chart(chartopts);

				//get mprn for site
				var mprn = getData(conId, meterId, "mprn");

				//set start and end times for data
				var end = Math.round((new Date()).getTime() / 1000);
				var start = end - 300;

				//create url for api request
				var url = "/api/site/historical?site=" + mprn + "&controller=" + this.conId + "&meter=" + this.meterId;
				url += "&params=v1_thd,v2_thd,v3_thd,i1_thd,i2_thd,i3_thd&start=" + start + "&end=" + end;

				//get the data
				$.ajax({
					url: url,
					type: "GET",
					dataType: "JSON",
					success: function(result) {
						var chart_p1 = [];
						var chart_p2 = [];
						var chart_p3 = [];
						var chart_p4 = [];
						var chart_p5 = [];
						var chart_p6 = [];

						$.each(result.raw, function(key, val) {
							chart_p1.push([parseFloat(val.time_sent), parseFloat(val.v1_thd) * 100]);
							chart_p2.push([parseFloat(val.time_sent), parseFloat(val.v2_thd) * 100]);
							chart_p3.push([parseFloat(val.time_sent), parseFloat(val.v3_thd) * 100]);
							chart_p4.push([parseFloat(val.time_sent), parseFloat(val.i1_thd) * 100]);
							chart_p5.push([parseFloat(val.time_sent), parseFloat(val.i2_thd) * 100]);
							chart_p6.push([parseFloat(val.time_sent), parseFloat(val.i3_thd) * 100]);

						});

						chart_p1.sort();
						chart_p2.sort();
						chart_p3.sort();
						chart_p4.sort();
						chart_p5.sort();
						chart_p6.sort();

						allgauges["thd-line-" + wid]["thd-highchart-" + wid].addSeries({
							name: "P1 THD",
							data: chart_p1
						});

						allgauges["thd-line-" + wid]["thd-highchart-" + wid].addSeries({
							name: "P2 THD",
							data: chart_p2
						});

						allgauges["thd-line-" + wid]["thd-highchart-" + wid].addSeries({
							name: "P3 THD",
							data: chart_p3
						});

						allgauges["thd-line-" + wid]["thd-highchart-" + wid].addSeries({
							name: "I1 THD",
							data: chart_p4
						});

						allgauges["thd-line-" + wid]["thd-highchart-" + wid].addSeries({
							name: "I2 THD",
							data: chart_p5
						});

						allgauges["thd-line-" + wid]["thd-highchart-" + wid].addSeries({
							name: "I3 THD",
							data: chart_p6
						});
					}
				});

			}
		} else {
			console.log("A Controller ID and Meter ID are required to create a widget");
		}
	}

	function powerLine(args) {
		if (args.conId && args.meterId) {
			this.conId = args.conId;
			this.meterId = args.meterId;
			this.min_kw = args.min_kw;
			this.max_kw = args.max_kw;

			//unique id for each site/meter
			var wid = this.conId + '-' + this.meterId;

			//check if we have a widget with the same id already setup
// 			var cur_widgets = $("#widgetlist").sDashboard("getDashboardData");
			var okaytocreate = true;

/*
			$.each(cur_widgets, function(key, val) {
				if (val.widgetId == "power-line-" + wid) {
					okaytocreate = false;
				}
			});
*/

			if (okaytocreate) {
				args.title ? this.title = args.title : this.title = "Power: " + wid;
				args.desc ? this.desc = args.desc : this.desc = wid;

				//structure
				var $plHtml = '<div id="power-l-' + wid + '" class="ewidget wContain line-graph"></div>';
				var $gplHtml = '<li data-widget="power-line-' + wid + '"><div class="widget-title">' + this.title + '<span class="widget-close" onclick="deleteWidget(this);">&#xf2d7;</span></div>' + $plHtml + '</li>';
				//add to gridster
				gridster = $('.gridster > ul').gridster().data('gridster');
				//gridster.options.widget_base_dimensions = [220,220];
				gridster.add_widget($gplHtml,args.gridster.size_x,args.gridster.size_y,args.gridster.col, args.gridster.row);
/*

				$("body").append($plHtml);

				//have sDashboard add the widget to the dashboard
				$("#widgetlist").sDashboard('addWidget', {
					widgetTitle: this.title,
					widgetId: "power-line-" + wid,
					widgetContent: $("#power-l-" + wid)
				});

*/
// 				$("#power-line-" + wid).fadeIn(500);

				//highcharts options
				var chartopts = args.chart_options;

				//set renderTo highcharts option
				chartopts.chart.renderTo = "power-l-" + wid;

				//calculate min and max for chart
				var ymin = this.min_kw;
				var ymax = this.max_kw;

				//get the difference between the max and min, scale it up to allow space
				var diff = ymax - ymin;
				var scale = Math.abs(diff) * 0.05;

				//add or subtract depending on sign
				ymin <= 0 ? ymin = (ymin - scale - 100) * 1000 : ymin = (ymin + scale + 100) * 1000;
				ymax <= 0 ? ymax = (ymax - scale - 100) * 1000 : ymax = (ymax + scale + 100) * 1000;

				if (ymax < 0 && ymin < 0) {
					ymax = ymax * -1;
				}

				//set the highcharts options.
				chartopts.yAxis = {
					min: ymin,
					max: ymax,
					title: {
						enabled: false
					},
					type: 'linear',
					labels: {
						enabled: true,
						formatter: function() {
							if (this.value > 1000) {
								return Highcharts.numberFormat(this.value/1000, 0, '', ',') + ' kW';
							} else {
								return parseFloat(this.value/1000).toFixed(1) + ' kW';
							}
						}
					}
				};

				chartopts.tooltip = {
					animation: true,
					pointFormat: '{series.name}: <b>{point.y}</b><br>',
					shared: false,
					valueDecimals: 0,
					valueSuffix: ' kW',
					formatter: function() {
						this.series.name = desc;
						return '<b>Time: </b>' + Highcharts.dateFormat('%H:%M:%S', this.x) + '<br><br><b>' + this.series.name + ': </b>' + this.y/1000 + ' kW';
					}
				};

				//create chart
				allgauges["power-line-" + wid] = {};
				allgauges["power-line-" + wid]["power-highchart-" + wid] = new Highcharts.Chart(chartopts);

				//get mprn for site
				var mprn = getData(conId, meterId, "mprn");

				//set start and end times for data
				var end = Math.round((new Date()).getTime() / 1000);
				var start = end - 300;

				//create url for api request
				var url = "/api/site/historical?site=" + mprn + "&controller=" + this.conId + "&meter=" + this.meterId + "&params=sys_w&start=" + start + "&end=" + end;

				//get the data
				$.ajax({
					url: url,
					type: "GET",
					dataType: "JSON",
					success: function(result) {
						var chart_series = [];

						$.each(result.raw, function(key, val) {

							chart_series.push([parseFloat(val.time_sent), parseFloat(val.sys_w)]);

						});

						chart_series.sort();

						allgauges["power-line-" + wid]["power-highchart-" + wid].addSeries({
							name: args.desc,
							data: chart_series
						});


					}
				});

			}
		} else {
			console.log("A Controller ID and Meter ID are required to create a widget");
		}
	}

	function frequencyGauge(args) {

		if (args.conId && args.meterId) {
			this.conId = args.conId;
			this.meterId = args.meterId;
			//unique id for each site/meter
			var wid = this.conId + '-' + this.meterId;

			//check if we have a widget with the same id already setup
// 			var cur_widgets = $("#widgetlist").sDashboard("getDashboardData");
			var okaytocreate = true;

/*
			$.each(cur_widgets, function(key, val) {
				if (val.widgetId == "frequency-gauge-" + wid) {
					okaytocreate = false;
				}
			});
*/

			if (okaytocreate) {
				args.title ? this.title = args.title : this.title = "Frequency: " + wid;
				args.desc ? this.desc = args.desc : this.desc = wid;

				//structure
				var $fgHtml = '<div id="frequency-' + wid + '" class="ewdiget wContain">';
				$fgHtml += '<div id="freq-container-' + wid + '" class="freqcon"></div>';
				$fgHtml += '<div class="freq_text"></div>';
				$fgHtml += '<div class="freq_value"></div>';
				$fgHtml += '<div class="freq_pf_text"></div>';
				$fgHtml += '<div class="freq_pf"></div>';
				$fgHtml += '</div>';

				//structure for gridster
				var $gfgHtml = '<li data-widget="frequency-gauge-' + wid + '"><div class="widget-title">' + this.title + '<span class="widget-close" onclick="deleteWidget(this);">&#xf2d7;</span></div>' + $fgHtml + '</li>';
				//add to gridster
				gridster = $('.gridster > ul').gridster().data('gridster');
				//gridster.options.widget_base_dimensions = [220,220];
				gridster.add_widget($gfgHtml,args.gridster.size_x,args.gridster.size_y,args.gridster.col, args.gridster.row);
/*

				$("body").append($fgHtml);

				//have sDashboard add the widget to the dashboard
				$("#widgetlist").sDashboard('addWidget', {
					widgetTitle: this.title,
					widgetId: "frequency-gauge-" + wid,
					widgetContent: $("#frequency-" + wid)
				});

*/
// 				$("#frequency-gauge-" + wid).fadeIn(500);

				//highcharts options
				var chartopts = args.chart_options;

				//set renderTo highcharts option
				chartopts.chart.renderTo = "freq-container-" + wid;

				//set values
				$("#frequency-" + wid + " .freq_text").text("Normal Operating range: 49.8 to 50.2 Hz");
				$("#frequency-" + wid + " .freq_value").text(getData(conId, meterId, "freq") + " Hz");
            	$("#frequency-" + wid + " .freq_pf_text").text("Power Factor ");

				//check if we need to hide power factor
				var showFreq = checkAmps(this.conId, this.meterId);

				if (showFreq) {
            		$("#frequency-" + wid + " .freq_pf").text(getData(conId, meterId, "pf"));
				} else {
					$("#frequency-" + wid + " .freq_pf").text("Not Reliable");
				}

				//create gauge
				allgauges["frequency-gauge-" + wid] = {};
				allgauges["frequency-gauge-" + wid]["frequency-highchart-" + wid] = new Highcharts.Chart(chartopts);


			}

		} else {
			console.log("A Controller ID and Meter ID are required to create a widget");
		}
	}

	function frequencyLine(args) {
		if (args.conId && args.meterId) {
			this.conId = args.conId;
			this.meterId = args.meterId;
			this.freq = args.freq;

			//unique id for each site/meter
			var wid = this.conId + '-' + this.meterId;

			//check if we have a widget with the same id already setup
// 			var cur_widgets = $("#widgetlist").sDashboard("getDashboardData");
			var okaytocreate = true;

/*
			$.each(cur_widgets, function(key, val) {
				if (val.widgetId == "frequency-line-" + wid) {
					okaytocreate = false;
				}
			});
*/

			if (okaytocreate) {
				args.title ? this.title = args.title : this.title = "Frequency: " + wid;
				args.desc ? this.desc = args.desc : this.desc = wid;

				//structure
				var $flHtml = '<div id="frequency-l-' + wid + '" class="ewidget wContain line-graph"></div>';
				//structure for gridster
				var $gflHtml = '<li data-widget="frequency-line-' + wid + '"><div class="widget-title">' + this.title + '<span class="widget-close" onclick="deleteWidget(this);">&#xf2d7;</span></div>' + $flHtml + '</li>';
				//add to gridster
				gridster = $('.gridster > ul').gridster().data('gridster');
				//gridster.options.widget_base_dimensions = [220,220];
				gridster.add_widget($gflHtml,args.gridster.size_x,args.gridster.size_y,args.gridster.col, args.gridster.row);
/*

				$("body").append($flHtml);

				//have sDashboard add the widget to the dashboard
				$("#widgetlist").sDashboard('addWidget', {
					widgetTitle: this.title,
					widgetId: "frequency-line-" + wid,
					widgetContent: $("#frequency-l-" + wid)
				});

*/
// 				$("#frequency-line-" + wid).fadeIn(500);

				//highcharts options
				var chartopts = args.chartoptions;

				//set renderTo highcharts option
				chartopts.chart.renderTo = "frequency-l-" + wid;

				chartopts.yAxis = {
					min: 47,
					max: 52,
					title: {
						enabled: false
					},
					type: 'linear',
					labels: {
						enabled: true,
						formatter: function() {
								return Highcharts.numberFormat(this.value, 2, '.', ',') + ' Hz';
						}
					}
				};

				chartopts.tooltip = {
					animation: true,
					pointFormat: '{series.name}: <b>{point.y}</b><br>',
					shared: true,
					valueDecimals: 0,
					valueSuffix: ' V',
					formatter: function() {
						var s = [];
						s.push('<b>Time: </b>' + Highcharts.dateFormat('%H:%M:%S', this.x));
						$.each(this.points, function(i, point) {
							s.push('<b>' + point.series.name + ': </b>' + point.y + ' Hz');
						});

						return s.join('<br>');
					}
				};

				//create chart
				allgauges["frequency-line-" + wid] = {};
				allgauges["frequency-line-" + wid]["frequency-highchart-" + wid] = new Highcharts.Chart(chartopts);

				//get mprn for site
				var mprn = getData(conId, meterId, "mprn");

				//set start and end times for data
				var end = Math.round((new Date()).getTime() / 1000);
				var start = end - 300;

				//create url for api request
				var url = "/api/site/historical?site=" + mprn + "&controller=" + this.conId + "&meter=" + this.meterId;
				url += "&params=freq&start=" + start + "&end=" + end;

				//get the data
				$.ajax({
					url: url,
					type: "GET",
					dataType: "JSON",
					success: function(result) {
						var chart_p1 = [];

						$.each(result.raw, function(key, val) {

							chart_p1.push([parseFloat(val.time_sent), parseFloat(val.freq)]);

						});

						chart_p1.sort();

						allgauges["frequency-line-" + wid]["frequency-highchart-" + wid].addSeries({
							name: "Frequency",
							data: chart_p1
						});
					}
				});

			}
		} else {
			console.log("A Controller ID and Meter ID are required to create a widget");
		}
	}
}

function checkAmps(conId, meterId) {
	var i1, i2, i3, smax, sval, minkva, maxkva, sysw;

	sysw = getData(conId, meterId, "sys_w");
	minkva = getData(conId, meterId, "min_kva");
	maxkva = getData(conId, meterId, "max_kva");
	i1 = getData(conId, meterId, "phase_1_amps");
	i2 = getData(conId, meterId, "phase_2_amps");
	i3 = getData(conId, meterId, "phase_3_amps");
	sval = i1 + i2 + i3;

	if (sysw < 0) {
		smax = Math.abs(Math.max(minkva)/.23);
	} else {
		smax = Math.abs(Math.max(maxkva)/.23);
	}

	smax = smax * 0.05;

	if (smax === 0) {
		return false;
	} else if (sval < smax) {
		return false;
	} else if (smax !== 0 && sval > smax) {
		return true;
	}

}

function checkVolts(conId, meterId) {
	var p1val, p2val, p3val, vavg;

	p1val = getData(conId, meterId, "phase_1_volts");
	p2val = getData(conId, meterId, "phase_2_volts");
	p3val = getData(conId, meterId, "phase_3_volts");
	vavg = (p1val + p2val + p3val) / 3;

	if (vavg < 25) {
		return false;
	} else {
		return true;
	}
}
