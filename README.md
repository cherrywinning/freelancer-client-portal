# Freelancer Project Description

## Overview
The person who is successful in their tender for this job will be asked to review the files contained within the repo and to fix the following issues:

1. All Synchronous Ajax requests must be made into Asynchronous Ajax Requests,
1. All functions used within Ajax requests are updated to handle the new asynchronous nature of the data request.
1. That where Synchronous Ajax Requests were used to pass data to the front end, that interface updates are made that display meaningful default information. (See example below.)

## Detailed Project Description
The files contained within this repo were created before Synchronous Ajax Requests on the main thread were deprecated. The project requirements are to update all the ajax requests to use Promises/ Web Workers to retrieve data from remote API endpoints and to correctly process the data when the Ajax request completes.

The processing functions/ logic should not be changed except where the requirements to use Promises forces a change in the logic, but the end result must remain the same.

It is acceptable to use a loading icon if data takes a long time to be returned, or if the interface would display incorrect data to the end user.

For example, user data is requested from the back end and stored locally using the LocalStorage API. The data is then read from the LocalStorage API and displayed on screen. Because the code was initially written to treat the user data API call as a synchronous request, the user data would not be shown on screen until the Ajax request had completed. Now, the Ajax request is asynchronous, so the user data shows up as null, because non-existent user data is read from LocalStorage, before the user data API call has completed. In this case, it is acceptable to use a loading icon to hide the interface until the user data API call has completed.

There a number of Ajax requests that have been rewritten to use Web Workers. (See the Workers directory.) The basic premise is that the web worker received a `postMessage` from it's parent scripts, performs an `XMLHTTPRequest` and uses `postMessage` to return the data. This pattern can be extended to the broken Ajax requests as long as the the data processing occurs within the parent scripts `onmessage` handler.

Changes to function and variable names should not be made, as other parts of the code base may depend upon them.

## Library Versions
The following library are the maximum version allowed:
1. JQuery 1.9.1
