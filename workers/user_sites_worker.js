onmessage = function(e) {
	var userSitesRequest = new XMLHttpRequest();

	userSitesRequest.onreadystatechange = function() {
		if (userSitesRequest.readyState == XMLHttpRequest.DONE) {
			postMessage(userSitesRequest.responseText);
		}
	};

	userSitesRequest.open('GET', '/user/sites', true);
	userSitesRequest.send();

};
