onmessage = function(e) {
	var userMeterRequest = new XMLHttpRequest();

	userMeterRequest.onreadystatechange = function() {
		if (userMeterRequest.readyState == XMLHttpRequest.DONE) {
			postMessage(userMeterRequest.responseText);
		}
	};

	userMeterRequest.open('GET', '/user/meter', true);
	userMeterRequest.send();

};
