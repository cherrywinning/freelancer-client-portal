onmessage = function(e) {
	var m = e.data.mprn;
	var storedWidgetsRequest = new XMLHttpRequest();

	storedWidgetsRequest.onreadystatechange = function() {
		if (storedWidgetsRequest.readyState == XMLHttpRequest.DONE) {
			postMessage(storedWidgetsRequest.responseText);
		}
	};

	storedWidgetsRequest.open('GET', '/api/site/user/config?site=' + m, true);
	storedWidgetsRequest.send();
};
